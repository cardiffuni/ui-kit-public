import fs from 'fs';
import sass from 'sass';
import fsExtra from 'fs-extra';
import { createHash } from 'crypto';

/**
 *
 * @param {string} path The file path of the sass file
 * @param map
 */
export function getComputed(path) {
  try {
    /* try load file from cache */
    const json = getCache(path)
    let variables = {}
    
    if (json !== false && json !== undefined) {
      return json;
    } else {
      const sassFile = fs.readFileSync(path).toString();
      const sassFileLines = sassFile.split('\n');

      sassFileLines.forEach(line => {
        const lineVar = line.match(/^(\$[a-z-A-Z]+):( |)+([a-zA-Z0-9#()\$,% -.]+)$/);

        if (lineVar !== null) {
          let dependencies = [];
          let computedValue = '';

          if (lineVar[3].indexOf('$') !== -1) {
            let dependenciesStr = '';

            /* Get all variables on the current line except the variable name */
            dependencies = lineVar[3].match(/\$([a-zA-Z0-9]+)/g);

            dependencies.forEach(item => {
              if (variables[item] !== undefined && variables[item].computed_value !== undefined) {
                dependenciesStr += `${variables[item].name}: ${variables[item].computed_value}\n`;
              } else {
                throw new Error(`Unknown variable: ${item}`);
              }
            });

            /* Mock the variable color */
            dependenciesStr += `${lineVar[0]}\n.computed-value\n  background: ${lineVar[1]}`;

            const computedSass = sass.renderSync({
              data: dependenciesStr,
              indentedSyntax: true
            });

            if (computedSass.error === undefined) {
              /* Get the compiled value and set it into the variable */
              computedValue = computedSass.css.toString().match(/background: ([a-zA-Z0-9#-. ,]+)\;/)[1];
            }
          } else {
            computedValue = lineVar[3];
          }

          /* Add variable to object */
          variables[lineVar[1]] = {
            name: lineVar[1],
            raw_value: lineVar[3],
            computed_value: computedValue,
            file: path,
            dependencies: dependencies || [],
            full_decoration: lineVar[0]
          };
        }
      });
      /* create cache file */
      const hash = getFileHash(path)

      const pathWJson = path.replace(/.sass$/, `.${hash}.json`).replace('./src/', './.cache/');

      fsExtra.outputFileSync(pathWJson, JSON.stringify(variables));
      return variables;
    }
  } catch (e) {
    return e;
  }
}

/**
 * Get cache file as JSON if exists
 * @param {string} path Path for the sass file
 */
export function getCache(path) {
  const hash = getFileHash(path)
  const pathWJson = path.replace(/.sass$/, `.${hash}.json`).replace('./src/', './.cache/');

  if (fs.existsSync(`${pathWJson}`)) {
    const file = fs.readFileSync(pathWJson);
    return JSON.parse(file.toString());
  } else {
    return false;
  }
}

/**
 * Get the file hash based on the paths contents
 * @param {string} path file path
 */
export function getFileHash(path) {
  let hash = createHash('sha256');

  const input = fs.readFileSync(path);
  hash.update(input);
  return hash.digest('hex');
}

/**
 * Create a new file in ./.cache by the file path.
 * @param {string} path Current file to add a cache file to
 * @param {JSON} contents JSOn object to write to file
 */
export function createCache(path, contents) {
  const hash = getFileHash(path)

  console.log(hash)
  const pathWJson = path.replace(/.sass$/, `.${hash}.json`).replace('./src/', './.cache/');

  fsExtra.outputFileSync(pathWJson, JSON.stringify(contents));
  return;
}
