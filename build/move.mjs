import fs from "fs-extra";

export function Move(from, to) {
  return fs.move(from, to);
}
