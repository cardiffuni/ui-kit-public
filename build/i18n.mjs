import fs from 'fs';
import path from 'path';
import fsExtra from 'fs-extra';

const scriptStartTime = process.hrtime();

/**
 * Generate all component HTML
 */
const updateI18nImports = () => {
  try {
    fs.readdirSync(`./tests/unit/js/components/`).forEach((file) => {
      const valid = !fs.statSync(`./tests/unit/js/components/${file}`).isDirectory();

      if (valid && path.extname(`./tests/unit/js/components/${file}`) === '.js') {
        let contents = fs.readFileSync(`./tests/unit/js/components/${file}`).toString();
        const matches = contents.match(/(from) ('|")i18n!([a-zA-Z-0-9/]+)(.min|)/);
        if (matches !== null) {
          console.log(`Converting: ${file}`);

          const quote = matches[2];
          const name = matches[3];
          const hasExt = matches[4] !== undefined;

          if (hasExt) {
            contents = contents.replace(`from ${quote}i18n!${name}${matches[4]}`, `from ${quote}../../js/${name}`);
          } else {
            contents = contents.replace(`from ${quote}i18n!${name}`, `from ${quote}../../js/${name}`);
          }
          fsExtra.outputFileSync(`./tests/unit/js/components/${file}`, contents);
        }
      }
    });
  } catch (e) {
    console.error(e);
  }
};

try {
  updateI18nImports();

  const scriptEndTime = process.hrtime(scriptStartTime);
  console.info('\x1b[32mI18n map fixed in %dms\x1b[0m', scriptEndTime[1] / 1000000);
} catch (e) {}
