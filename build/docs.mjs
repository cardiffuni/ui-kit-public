import fs from 'fs';
import path from 'path';
import rimraf from 'rimraf';
import fsExtra from 'fs-extra';
import { getComputed } from './sass.mjs';

const scriptStartTime = process.hrtime();

const paths = {
  components: './src/html/components/',
  layouts: './src/html/layouts/',
  pages: './src/html/pages/',
  tests: './src/html/tests/',
  globals: './src/html/globals/',
  svg: './src/svg/',
  sass: './src/sass/',
  docs: './docs/',
  output: {
    pages: './docs/pages/',
    components: './docs/components/',
    layouts: './docs/layouts/',
    globals: './docs/',
  },
  jest: './tests/unit/html/',
};
const templates = {
  component: {
    variation: fs.readFileSync('./src/html/templates/component/variation.html').toString(),
    globalItem: fs.readFileSync('./src/html/templates/component/page.html').toString(),
  },
};

const blacklist = ['head', 'footer-global', 'header-global', '.gitkeep'];

const regexs = {
  includes: /\$\{require\(((\'|\")([a-zA-Z-0-9\/.]+)(\'|\"))\)\}/g,
  svgs: /\$\{require-svg\(((\'|\")([a-zA-Z-0-9\/.]+)(\'|\"))\)\}/g,
  sassVars: /\$\{require-variable\((\'|\")([a-zA-Z-0-9\/.]+)(\'|\"),( |)(\'|\")(\$[a-zA-Z-0-9\/. ]+)(\'|\"),( |)(\'|\")([a-zA-Z-0-9\/.;:,\'\" /<>=\n]+)(\'|\")\)\}/g,
  filePath: /(\'|\")([a-zA-Z-0-9\/.]+)(\'|\")/,
  sassFunction: /(\'|\")([a-zA-Z-0-9\/.]+)(\'|\"),( |)(\'|\")(\$[a-zA-Z-0-9\/.]+)(\'|\"),( |)(\'|\")([a-zA-Z-0-9\/.;:,\'\" /<>=\n]+)(\'|\")/,
  quotes: /(\'|\")/g,
};

let sassMaps = {};

/**
 * generates a list of all components in the directory
 * @param {string} dir Directory path
 */
const generateDirectoryList = (dir) => {
  let list = [];

  fs.readdirSync(dir).forEach((file) => {
    const valid = fs.statSync(`${dir}${file}`).isDirectory();

    if (blacklist.indexOf(file) === -1) {
      if (valid) {
        const pathObject = path.parse(`${dir}${file}`);

        if (fs.existsSync(`${dir}${file}/${pathObject.name}.json`)) {
          let component = JSON.parse(fs.readFileSync(`${dir}${file}/${pathObject.name}.json`).toString());

          list.push({
            name: component.name,
            url: file,
            items: component.items,
          });
        } else {
          list.push({
            name: file,
            url: file,
          });
        }
      } else {
        const pathObject = path.parse(`${dir}${file}`);

        if (fs.existsSync(`${dir}/${pathObject.name}.json`)) {
          let component = JSON.parse(fs.readFileSync(`${dir}/${pathObject.name}.json`).toString());

          list.push({
            name: component.name,
            url: file.replace('.json', ''),
          });
        } else {
          list.push({
            name: file,
            url: file.replace('.json', ''),
          });
        }
      }
    }
  });
  return list;
};

const componentsList = generateDirectoryList('./src/html/components/');
const globalsList = generateDirectoryList('./src/html/globals/');

/**
 * Resets test and docs html
 */
const reset = () => {
  rimraf.sync(`${paths.docs}`);
  rimraf.sync('./.cache');
  rimraf.sync(paths.jest);
};

const getPageNavHTML = (item, currentPage, outDir) => {
  let html = `<li class="nav-item">
    <a
      href="${outDir}${item.url}" class="nav-link level-2  ${currentPage === item.name ? 'active' : ''}"
    >${item.name}</a>`;
  return html;
};

/**
 * Generates HTML for the globals and components pages for the doc navigation
 * @param {string} currentPage Name of the current page to show the active state
 */
const docsNavigation = (currentPage) => {
  const template = {
    components: '',
    globals: '',
  };
  componentsList.forEach((item) => {
    template.components += getPageNavHTML(item, currentPage, 'http://localhost:8080/docs/components/');
  });
  globalsList.forEach((item) => {
    template.globals += getPageNavHTML(item, currentPage, 'http://localhost:8080/docs/');
  });

  return template;
};

/**
 * Recursively builds up a HTML page by including all SVG and html partials
 * @param {string} path path to the file
 * @param {string} component component name
 */
const htmlBuilder = (path, component) => {
  let html = fs.readFileSync(path).toString();

  /* Check for any imports */
  const svgsResults = html.match(regexs.svgs);
  const includesResults = html.match(regexs.includes);
  const sassVarsResults = html.match(regexs.sassVars);

  if (includesResults) {
    const splits = path.split('/');
    component = splits[splits.length - 2];
    component = component === 'pages' || component === 'tests' || component === 'layouts' ? '' : component;

    includesResults.forEach((value, index) => {
      component = component.includes('.html') ? '' : `${component.replace(/\//, '')}/`;

      const path = `${paths.components}${component}${value.match(regexs.filePath)[0].toString().replace(regexs.quotes, '')}`;
      const ext = path.includes('.html') ? '' : '.html';
      const partial = fs.readFileSync(`${path}${ext}`).toString();

      if (partial.match(regexs.includes) || partial.match(regexs.svgs)) {
        html = html.replace(value, htmlBuilder(path, component));
      } else {
        html = html.replace(value, partial);
      }
    });
  }

  if (svgsResults) {
    svgsResults.forEach((value) => {
      const path = `${paths.svg}${value.match(regexs.filePath)[0].toString().replace(regexs.quotes, '')}`;
      const ext = path.includes('.svg') ? '' : '.svg';
      const partial = fs.readFileSync(`${path}${ext}`).toString();

      html = html.replace(value, partial);
    });
  }

  if (sassVarsResults) {
    sassVarsResults.forEach((value) => {
      const sassFunc = value.match(regexs.sassFunction);
      const path = `${paths.sass}${sassFunc[2]}`;
      const ext = path.includes('.sass') ? '' : '.sass';

      if (sassMaps[`${path}${ext}`] === undefined) {
        const vars = getComputed(`${path}${ext}`);

        sassMaps[`${path}${ext}`] = vars;
        let renderText = sassFunc[10];

        renderText = renderText.replace(/computed/gm, sassMaps[`${path}${ext}`][sassFunc[6]].computed_value);
        renderText = renderText.replace(/name/gm, sassMaps[`${path}${ext}`][sassFunc[6]].name);
        renderText = renderText.replace(/raw/gm, sassMaps[`${path}${ext}`][sassFunc[6]].raw);
        html = html.replace(value, renderText);
      } else {
        let renderText = sassFunc[10];
        renderText = renderText.replace(/computed/gm, sassMaps[`${path}${ext}`][sassFunc[6]].computed_value);
        renderText = renderText.replace(/name/gm, sassMaps[`${path}${ext}`][sassFunc[6]].name);
        renderText = renderText.replace(/raw/gm, sassMaps[`${path}${ext}`][sassFunc[6]].raw);

        html = html.replace(value, renderText);
      }
    });
  } else {
    return html;
  }

  return html;
};

/**
 * Builds up the final HTML page
 * @param {string} componentPath Component path to build recursively
 * @param {string} component component name
 * @param {boolean} wrapper Whether to include a wrapper around HTML
 */
const template = (componentPath, component) => {
  let document = htmlBuilder(`${paths.components}head/head.html`, 'head');
  document = document.replace('{{{documentTitle}}}', component);
  document += htmlBuilder(`${paths.components}header-global/header-global.html`, 'header-global');
  document += htmlBuilder(`${componentPath}`, component);
  document += htmlBuilder(`${paths.components}footer-global/footer-global.html`, 'footer-global');

  return document;
};

/**
 * Stage all files to build HTML
 * @param {string} path Root path
 * @param {string} outPath Output file to generate HTML
 * @param {boolean} wrapper Whether to include a wrapper
 * @author Christopher Atwood
 */
const checkDirectory = (path, outPath = paths.docs) => {
  fs.readdirSync(path).forEach((component) => {
    const valid = fs.statSync(`${path}${component}`).isDirectory();

    if (blacklist.indexOf(component) === -1) {
      if (valid) {
        fs.readdirSync(`${path}${component}`).forEach((file) => {
          const isFile = fs.statSync(`${path}${component}/${file}`).isFile();

          if (isFile) {
            fsExtra.outputFileSync(`${outPath}${file}`, template(`${path}${component}/${file}`, component));
          }
        });
      } else {
        if (outPath === paths.jest) {
          const document = htmlBuilder(`${path}${component}`, component);

          fsExtra.outputFileSync(`${outPath}${component}`, document);
        } else {
          fsExtra.outputFileSync(`${outPath}${component}`, template(`${path}${component}`, component));
        }
      }
    }
  });
};

/**
 * Generates a component HTML page
 * @param {string} pathStr  Path the src doc file
 */
const generateHTML = (pathStr, out) => {
  try {
    let component = JSON.parse(fs.readFileSync(pathStr).toString());

    const pathObj = path.parse(pathStr);
    if (blacklist.indexOf(pathObj.name) === -1) {
      let pageHTML = '';
      let sectionNavHTML = '';

      if (component.html !== undefined) {
        pageHTML += htmlBuilder(`${pathObj.dir}/${component.html}`, pathObj.name);
      }

      if (component.items !== undefined) {
        component.items.forEach((value) => {
          let alertPartial = '';
          sectionNavHTML += `<li class="nav-item"><a href="#${value.name.replace(/ /g, '-')}" class="nav-link">${value.name}</a>`;

          const html = htmlBuilder(`${pathObj.dir}/${value.path}`, pathObj.name);

          value.alerts.forEach((value) => {
            alertPartial += `<p class="alert">${value}</p>`;
          });
          component.items;
          const hideCode = value.hideCode !== undefined && value.hideCode;

          pageHTML += templates.component.variation
            .replace('{{{title}}}', value.name)
            .replace('{{{description}}}', value.description)
            .replace('{{{whenToUse}}}', value.whenToUse)
            .replace('{{{id}}}', value.name.replace(/ /g, '-'))
            .replace('{{{html}}}', hideCode ? html : `<div class="component-example">${html}</div>`)
            .replace('{{{alert}}}', alertPartial)
            .replace('{{{contentNotes}}}', value.contentNotes)
            .replace('{{{escapedHTML}}}', hideCode ? '' : `<pre class="prettyprint">${escapeHTML(html)}</pre>`);
        });
      }

      if (sectionNavHTML.length > 0) {
        sectionNavHTML = `<div class="nav-section sticky-top">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <nav class="navbar">
                  <ul class="nav">
                    ${sectionNavHTML}
                    <li class="dropdown dropdown-more nav-item">
                      <a href="#" class="nav-link dropdown-toggle">More</a>
                      <ul class="dropdown-menu dropdown-menu-more dropdown-menu-right"></ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>`;
      }
      /* Get page navigation */
      const navigation = docsNavigation(component.name);

      /* Generate html */
      const componentHTML = templates.component.globalItem
        .replace('{{{title}}}', component.name)
        .replace('{{{intro}}}', component.intro)
        .replace('{{{description}}}', component.description)
        .replace('{{{html}}}', pageHTML)
        .replace('{{{componentsList}}}', navigation.components)
        .replace('{{{globalsList}}}', navigation.globals)
        .replace('{{{sectionNav}}}', sectionNavHTML)
        .replace('{{{componentsOpen}}}', out === './docs/' ? '' : 'show')
        .replace('{{{globalsOpen}}}', out === './docs/components/' ? '' : 'show');

      const content = `
        ${htmlBuilder(`${paths.components}head/head.html`, 'head').replace('{{{documentTitle}}}', component.name)}      
        ${htmlBuilder(`${paths.components}skip-content/skip-content.html`, 'skip-content')}
        ${htmlBuilder(`${paths.components}nav-secondary/nav-secondary.html`, 'nav-secondary')}
        <div class="content" id="content">${componentHTML}</div>
        <script data-main="../../dist/js/main" src="../../dist/lib/require.js"></script></body>`;

      fsExtra.outputFileSync(`${out}${pathObj.name}.html`, content);
      fsExtra.outputFileSync(`${out}standalone/${pathObj.name}.html`, componentHTML);
    }
  } catch (error) {
    console.log(error);
    // console.trace();
  }
};

/**
 * Escapes html chracters for prettier
 * @param {string} str HTML string
 * @return {string} ASCII escaped string
 */
const escapeHTML = (str) => str.replace(/[\u00A0-\u9999<>\&]/gim, (i) => `&#${i.charCodeAt(0)};`);

/**
 * Generate all component HTML
 */
const generateComponents = () => {
  fs.readdirSync(paths.components).forEach((file) => {
    const valid = fs.statSync(`${paths.components}${file}`).isDirectory();

    if (blacklist.indexOf(file) === -1) {
      if (valid && fs.existsSync(`${paths.components}${file}/${file}.json`)) {
        generateHTML(`${paths.components}/${file}/${file}.json`, './docs/components/');
      }
    }
  });
};

const generateGlobals = () => {
  fs.readdirSync(paths.globals).forEach((file) => {
    if (blacklist.indexOf(file) === -1) {
      if (fs.existsSync(`${paths.globals}${file}`)) {
        generateHTML(`${paths.globals}${file}/${file}.json`, './docs/');
      }
    }
  });
};
try {
  reset();
  fsExtra.copy('./src/html/raw', './docs/raw', function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log('success!');
    }
  });
  generateComponents();
  generateGlobals();
  checkDirectory(paths.pages, paths.output.pages);
  checkDirectory(paths.layouts, paths.output.layouts);
  checkDirectory(paths.tests, paths.jest);

  const scriptEndTime = process.hrtime(scriptStartTime);
  console.info('\x1b[32mDocs generated in %dms\x1b[0m', scriptEndTime[1] / 1000000);
} catch (e) {}
