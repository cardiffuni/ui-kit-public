# Table of contents

- [Table of contents](#table-of-contents)
  - [Install](#installation)
  - [Common issues](#common-issues)
  - [Docs generation](#docs-generation)
    - [HTML Partials](#html-partials)
    - [SVG partials](#svg-partials)
    - [Component](#component)
  - [Tests](#tests)

## Installation

1. Install python - Visit python site for the latest version
2. Install dependencies for the project ``npm install``
3. Run ``npm run dev`` to start a dev system

## Common issues

### Experimental JSON flag

On Windows machines, often there are issues with js-version script. If this is enocourned, add the update the follow script

#### from

``"js-version": "node --experimental-modules ./build/js-version.mjs",``

#### to

``"js-version": "node --experimental-modules --experimental-json-modules ./build/js-version.mjs",``

### Sass has not install correctly

Normall this occurs when python is not installed. Please verify python is installed. If install run ``npm rebuild node-sass``


## Contribution

All branches should branch of development.

## Commands

### Development 

You can run a dev server by using the ``npm run dev`` command. This will watch for file changes and create a development server on port 8080.

## Docs generation

All pages within the ``./src/html/components/**/*.html`` will automatically generate a HTML page with correct head and footer  elements. These pages will generate a basic HTML page with ``.content>.container>.row>.col-md-12``.  If you require a different layout for the component, then you can create a page within pages.

### HTML Partials

``` Javascript
  ${require('variations/accordion-squiz.html')}

  // OR
  ${require('variations/accordion-squiz')}
```

### SVG partials

``` Javascript
  ${require-svg('chevron-up.svg')}

  // OR
  ${require-svg('chevron-up')}
```

### Component

All UI Kit components can be found at [http://localhost:8080/docs/components](http://localhost:8080/docs/components). There are additional global/ utility styles which can be found under the globals section within the left hand navigation.

## Tests

Test pages do not generate with head or footer components.You must include all partials you which to test.
