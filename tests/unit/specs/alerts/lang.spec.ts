import $ from 'jquery';
import Alert from '../../js/components/alert';

jest.dontMock('jquery');

describe('Squiz language extract', (): void => {
  it('English Squiz language', (): void => {
    $('html').attr('lang', 'en');
    expect(Alert.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('16:30 ceremony');
  });

  it('Welsh Squiz language', (): void => {
    $('html').attr('lang', 'cy');
    expect(Alert.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('Seremoni 16:30');
  });

  it('Default Squiz language', (): void => {
    $('html').removeAttr('lang');
    expect(Alert.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('16:30 ceremony');
  });
  it('Test when multilingual is not present', (): void => {
    $('html').removeAttr('lang');
    expect(Alert.extractLanguageSubstring('16:30 ceremony')).toBe('16:30 ceremony');
  });
});
