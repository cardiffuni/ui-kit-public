import Alert from '../../js/components/alert';
import fs from 'fs';
import $ from 'jquery';

jest.dontMock('fs').dontMock('jquery');

beforeAll((): void => {
  document.documentElement.innerHTML = fs.readFileSync(fs.realpathSync('./tests/unit/html/alert-squiz.html')).toString();
});

it('Init works correctly', (): void => {
  Alert.init();
});

describe('Squiz generates correct HTML in English', (): void => {
  document.querySelector('html').setAttribute('lang', 'en');

  Alert.init();

  it('Generates open all button', (): void => {
    expect($('.alert').length).toBe(1);
  });

  it('Alert structure is generated correctly', (): void => {
    expect($('.alert-title').length).toBe(1);
    expect($('.alert-body').length).toBe(1);
  });
});

describe('Squiz generates correct HTML in Welsh', (): void => {
  document.querySelector('html').setAttribute('lang', 'cy');

  Alert.init();

  it('Generates open all button', (): void => {
    expect($('.alert').length).toBe(1);
  });

  it('Alert structure is generated correctly', (): void => {
    expect($('.alert-title').length).toBe(1);
    expect($('.alert-body').length).toBe(1);
  });
});
describe('Check that no errors when no alert is present', (): void => {
  document.documentElement.innerHTML = '';

  Alert.create();
});

it('Alert structure is generated without heading', (): void => {
  document.documentElement.innerHTML = fs.readFileSync(fs.realpathSync('./tests/unit/html/alert-squiz-no-desc.html')).toString();

  Alert.create();

  expect($('.alert-title').length).toBe(0);
  expect($('.alert-body').length).toBe(1);
});
