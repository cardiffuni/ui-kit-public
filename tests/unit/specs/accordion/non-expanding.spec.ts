import $ from 'jquery';
import Accordion from '../../js/components/accordion';
import fs from 'fs';

jest.dontMock('fs').dontMock('jquery');

beforeAll((): void => {
  document.documentElement.innerHTML = fs.readFileSync(fs.realpathSync('./tests/unit/html/accordion-mobile.html')).toString();
});

it('Init works correctly', (): void => {
  Accordion.init();
});

describe('Squiz generates correct HTML in English', (): void => {
  document.querySelector('html').setAttribute('lang', 'en');

  Accordion.create();

  it('Does not generate open all button', (): void => {
    expect($('.accordion .text-right .btn').length).toBe(0);
  });
});
