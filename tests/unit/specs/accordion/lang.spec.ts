import $ from 'jquery';
import Accordion from '../../js/components/accordion';

jest.dontMock('jquery');

describe('Squiz language extract', (): void => {
  it('English Squiz language', (): void => {
    $('html').attr('lang', 'en');
    expect(Accordion.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('16:30 ceremony');
  });

  it('Welsh Squiz language', (): void => {
    $('html').attr('lang', 'cy');
    expect(Accordion.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('Seremoni 16:30');
  });

  it('Default Squiz language', (): void => {
    $('html').removeAttr('lang');
    expect(Accordion.extractLanguageSubstring('en=16:30 ceremony|cy=Seremoni 16:30')).toBe('16:30 ceremony');
  });
  it('Test when multilingual is not present', (): void => {
    $('html').removeAttr('lang');
    expect(Accordion.extractLanguageSubstring('16:30 ceremony')).toBe('16:30 ceremony');
  });
});
