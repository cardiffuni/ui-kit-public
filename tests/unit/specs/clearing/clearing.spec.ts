import $ from 'jquery';
import Clearing from '../../js/components/clearing';
import i18n from '../../js/locale/globals';
import i18nCY from '../../js/locale/cy/globals';

describe('Clearing template is generated correctly', (): void => {
  it('English returns correct context', (): void => {
    $('document').attr('lang', 'cy');

    const tmpl: string = Clearing.template(i18n.root.clearing.title, i18n.root.clearing.bodyText, new URL('http://www.cardiff.ac.uk/cy/'), i18n.root.clearing.linkText);

    expect(tmpl).toEqual(`<div class="alert">
      <h4 class="alert-title">${i18n.root.clearing.title}</h4>
      <div class="alert-body">${i18n.root.clearing.bodyText} <a href="${new URL('http://www.cardiff.ac.uk/cy/').toString()}">${i18n.root.clearing.linkText}</a></div> 
    </div>`);
  });

  it('Welsh returns correct context', (): void => {
    $('document').attr('lang', 'en');

    const tmpl: string = Clearing.template(i18nCY.clearing.title, i18nCY.clearing.bodyText, new URL('http://www.cardiff.ac.uk/cy/'), i18nCY.clearing.linkText);

    expect(tmpl).toEqual(`<div class="alert">
      <h4 class="alert-title">${i18nCY.clearing.title}</h4>
      <div class="alert-body">${i18nCY.clearing.bodyText} <a href="${new URL('http://www.cardiff.ac.uk/cy/').toString()}">${i18nCY.clearing.linkText}</a></div> 
    </div>`);
  });
});

it('Only appears when is from clearing and is not a clearing page', (): void => {
  Object.defineProperty(document, 'referrer', { writable: true, value: 'http://localhost:8080/docs/html/pages/UG-page/index.html' });
  Object.defineProperty(window, 'location', {
    writable: false,
    value: {
      href: 'http://localhost:8080/docs/html/pages/clearing/index.html',
    },
  });

  Clearing.init();
});
