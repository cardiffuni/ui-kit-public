import $ from 'jquery'
import Pill from '../../js/components/pill';
import fs from 'fs'

jest
  .dontMock('fs')
  .dontMock('jquery')

beforeAll(() => {
  document.documentElement.innerHTML = fs.readFileSync(fs.realpathSync('./tests/unit/html/pills.html')).toString()
})

it('Init works correctly', ()  => {
  Pill.init()
})


describe('Clicking pill item ', (): void => {
  Pill.init()

  it ('And choose to open all', (): void => {
    $('.nav-link[data-target="all"]').trigger('click')
    
    expect($('.filter-pane.d-block').length).toBe(6)
    expect($('.filter-pane.d-none').length).toBe(0)
    expect($('.filter-content-title.d-block').length).toBe(1)
  })

  it ('and you click an item', (): void => {
    $('#home-tab').trigger('click')
    expect($('.filter-pane.d-block').length).toBe(2)
    expect($('.filter-pane.d-none').length).toBe(4)
    expect($('.filter-content-title.d-block').length).toBe(1)
  })

  it ('Click a item with no data-taret', (): void => {
    $('#no-data-target-pill').trigger('click')
    expect($('.filter-pane.d-block').length).toBe(2)
    expect($('.filter-pane.d-none').length).toBe(4)
    expect($('.filter-content-title.d-block').length).toBe(1)
  })
  
  it ('When no panes are open, all should open', (): void => {
    $('.filter-pane').removeClass('d-block')
    Pill.open('#null')
    expect($('.filter-pane.d-none').length).toBe(6)
    expect($('.filter-content-title.d-none').length).toBe(1)
    
  })
})