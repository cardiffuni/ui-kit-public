import i18n from '../../js/locale/globals';

describe('i18n returns correct text', (): void => {
  it('Check that link text is correct', (): void => {
    expect(i18n.root.clearing.linkText).toEqual('Go back to our Clearing pages');
  });
  it('Check that title is correct', (): void => {
    expect(i18n.root.clearing.title).toEqual('Clearing and Adjustment');
  });
  it('Check that body is correct', (): void => {
    expect(i18n.root.clearing.bodyText).toEqual(`Call us on <span class='InfinityNumber'>+44 (0)33 3241 2800</span> to discuss potential vacancies.`);
  });
});
