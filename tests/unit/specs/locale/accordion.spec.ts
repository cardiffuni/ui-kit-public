import i18n from '../../js/locale/globals';
import i18nCY from '../../js/locale/cy/globals';

describe('i18n returns correct text', (): void => {
  it('Correct translations in English', (): void => {
    document.querySelector('html').setAttribute('lang', 'en');
    expect(i18n.root.accordions.openAll).toEqual('Open all');
    expect(i18n.root.accordions.closeAll).toEqual('Close all');
  });

  it('Correct translations in Welsh', (): void => {
    document.querySelector('html').setAttribute('lang', 'cy');
    expect(i18nCY.accordions.openAll).toEqual('Agor pob un');
    expect(i18nCY.accordions.closeAll).toEqual('Cau pob un');
  });
});
