define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.BackToTop => {
  const icon =
    '<svg class="icon" viewBox="0 0 44 49" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>Arrow up</title><g id="Custom-icon-/-Back-to-top" transform="translate(-28.000000, -26.000000)"> <g id="Group-3" transform="translate(50.000000, 50.500000) rotate(-270.000000) translate(-50.000000, -50.500000) translate(26.000000, 29.000000)"><g id="Clip-2"></g><path d="M26.9203826,1.62519024 C29.0777739,3.79406829 29.0772522,7.32164146 26.9209043,9.48894634 L20.3788174,16.0642756 L42.6930783,16.0642756 C45.611687,16.0642756 47.9861217,18.4507756 47.9861217,21.384739 C47.9861217,24.3187024 45.611687,26.7052024 42.6930783,26.7052024 L20.3788174,26.7052024 L26.9209043,33.2800073 C29.0777739,35.448361 29.0772522,38.9759341 26.9209043,41.1437634 C25.8758609,42.1941171 24.4864696,42.7719951 23.0089043,42.7719951 C21.5308174,42.7719951 20.1419478,42.1941171 19.0969043,41.143239 L0.396730435,22.3490927 C0.142643478,22.0931902 0.000208695652,21.7460439 0.000208695652,21.384739 C0.000208695652,21.0234341 0.142643478,20.6762878 0.396730435,20.4203854 L19.0969043,1.62519024 C21.2542957,-0.541590244 24.763513,-0.542114634 26.9203826,1.62519024 Z" fill=""></path></g></g></svg>';

  /** 400px for the header and 40px for the spacing */
  const scrollThreshold = window.innerHeight + 440;
  /** margin-top: 90, bottom: 60 */
  const linkMargin = 150;
  const containerLGWidth = 980;
  const minWindowWidth = containerLGWidth;

  /** how far the screen has been scrolled whilst being throttled */

  let localNavHeight = ($('#document-toc-content').offset()?.top || 0) + ($('#document-toc-content').outerHeight() || 0) + linkMargin;

  /** how far the screen has been scrolled whilst being throttled */
  let lastKnownScrollPosition = $(window).scrollTop() || 0;

  /** Whether the event is being throttled */
  let ticking = false;

  const init = (): void => {
    checkOffset();

    $(window).on('resize', (): void => {
      lastKnownScrollPosition = $(window).scrollTop() || 0;
      if (ticking) return;

      localNavHeight = ($('#document-toc-content').offset()?.top || 0) + ($('#document-toc-content').outerHeight() || 0) + linkMargin;

      window.requestAnimationFrame((): void => {
        checkOffset();
        ticking = false;
      });

      ticking = true;
    });

    $(window).on('scroll', (): void => {
      lastKnownScrollPosition = $(window).scrollTop() || 0;
      if (ticking) return;

      window.requestAnimationFrame((): void => {
        checkOffset();
        ticking = false;
      });

      ticking = true;
    });
  };

  /**
   * Display or hide the back to top link based on the scroll distance.
   * > Desktop: When the scroll offset has passed the threshold, fade the item bottom in.
   * <= Desktop: Also show the link at the bottom of the page.
   */
  const checkOffset = (): void => {
    const backToTop = $('.back-to-top');
    if (backToTop.length === 0) return;
    const link = backToTop.find('a');
    if (link.length === 0) return;
    /**
     * If the link will fit on the left hand side without overlaying content.
     * Link width must be doubled in order to calcuate the correct container width
     */

    if (window.innerWidth >= minWindowWidth) {
      backToTop.addClass('position-sticky');

      /* Remove classes to prevent back to top overlapping */
      backToTop.removeClass('my-20');

      if (window.innerHeight + lastKnownScrollPosition >= Math.max(scrollThreshold, localNavHeight)) {
        backToTop.addClass('active');
      } else {
        backToTop.removeClass('active');
      }
    } else {
      backToTop.removeClass('position-sticky');
      backToTop.addClass('active');

      /* if the document has no control table, add additional margin Y */
      if ($('#control-table').length === 0) {
        backToTop.addClass('my-20');
      }
    }

    setTimeout((): void => {
      backToTop.addClass('back-to-top--loaded');
    }, 10);
  };

  return {
    init,
  };
});
