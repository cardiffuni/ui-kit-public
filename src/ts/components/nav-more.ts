define(['jquery'], ($: JQueryStatic): UIKit.Module.NavMore => {
  const setup = (): void => {
    $('.nav-section').each((index, element): void => {
      let emulated = false;

      if (window.innerWidth < 980) {
        element.style.height = '0px';
        element.style.display = 'block';
        emulated = true;
      }

      const childItems = $(element).find(`.nav > .nav-item`);
      element.style.width = '1170px';

      childItems.each((childIndex, navItem): void => {
        if (childIndex !== childItems.length - 1) {
          const html = $(navItem.outerHTML);
          html.addClass('d-none');
          html.attr('id', `nav-section-dd-${index}-${childIndex}`);

          hideElement(html);
          showElement(html);

          const offset = ($(navItem).outerWidth(true) || 0) + navItem.offsetLeft;

          /* Add item to the dropdown */
          $(element).find('.dropdown-menu').append(html);

          $(navItem).attr('data-show-at', offset + ($(element).find('.dropdown').outerWidth(true) || 0));
        }

        /* Add class to last item that is not the dropdown */
        if (childIndex === childItems.length - 2) {
          $(navItem).addClass('last-item');
        }
      });

      if (emulated) {
        element.style.height = '';
        element.style.display = '';
      }

      element.style.width = '';
      $(element).addClass('loaded');
    });
  };

  const reset = (): void => {
    showElement($('.nav-section .nav-item'));
    hideElement($('.nav-section .dropdown-menu .nav-item'));
    hideElement($('.nav-section .dropdown'));
  };

  const update = (): void => {
    if (window.innerWidth >= 980) {
      $('.nav-section .navbar').each((index, element): void => {
        let showMore = false;
        const width = $(element).outerWidth() || 0;
        const childItems = $(element).find(`.nav > .nav-item`);

        childItems.each((childIndex, navItem): void => {
          if (childIndex !== childItems.length) {
            if (parseInt($(navItem).attr('data-show-at') || '0') <= width) {
              showElement($(navItem));
              hideElement($(`#nav-section-dd-${index}-${childIndex}`));
            } else {
              hideElement($(navItem));
              showElement($(`#nav-section-dd-${index}-${childIndex}`));

              showMore = true;
            }
          }
        });

        if (showMore) {
          showElement($(element).find('.dropdown'));
        } else {
          hideElement($(element).find('.dropdown'));
        }
      });
    } else {
      reset();
    }
  };

  const showElement = (element: JQuery<HTMLElement>): void => {
    element.removeClass('d-none');
    element.removeClass('invisible');
    element.attr({ 'aria-hidden': false });
  };

  const hideElement = (element: JQuery<HTMLElement>): void => {
    element.addClass('d-none');
    element.addClass('invisible');
    element.attr({ 'aria-hidden': true });
  };
  const listen = (): void => {
    $(window).on('resize', (): void => {
      update();
    });
  };
  const init = (): Promise<boolean> => {
    return new Promise((resolve): void => {
      setup();
      update();
      listen();
      resolve(true);
    });
  };

  return {
    init,
  };
});
