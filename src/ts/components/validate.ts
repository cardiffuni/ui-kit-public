define(['jquery'], ($: JQueryStatic): UIKit.Module.Validate => {
  var validate: any;
  const init = (): void => {
    const form = $('.validate form') as UIKit.JQueryValidate;
    form.validate({
      errorClass: 'help-inline error',
      errorElement: 'span',
    });
  };
  // adds the error class to the surrounding control-group as per Bootstrap forms
  const highlight = (element: HTMLElement): void => {
    $(element).closest('.control-group').addClass('error');
  };

  // removes the error class from the control-group
  const unhighlight = (element: HTMLElement): void => {
    $(element).closest('.control-group').removeClass('error');
  };

  return {
    init,
    highlight,
    unhighlight,
  };
});
