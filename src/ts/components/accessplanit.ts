define(['jquery', 'accessplanit'], ($: JQueryStatic): UIKit.Module.Accessplanit => {
  const init = (): void => {
    const token = 'digi-comms-logs';
    const sessionKey = [...Array(32)].map((_: any): string => ((Math.random() * 36) | 0).toString(36)).join(``);

    $('#request-interest').on('submit', (evt: Event): void => {
      evt.preventDefault();

      $('#first-name-error-message').addClass('d-none');
      $('#last-name-error-message').addClass('d-none');
      $('#email-error-message').addClass('d-none');

      let count = 0;

      if ($('#first-name-input').val() == '') {
        $('#first-name-error-message').removeClass('d-none');
        count++;
      }

      if ($('#last-name-input').val() == '') {
        $('#last-name-error-message').removeClass('d-none');
        count++;
      }

      if ($('#email-input').val() == '') {
        $('#email-error-message').removeClass('d-none');
        count++;
      }

      const marketingAnswer = $('input[name="marketing-option"]:checked').val();
      let marketing = false;
      let isWelsh = false;
      const attendees = $('#number-attendees').val();

      if (marketingAnswer !== '' && typeof marketingAnswer === 'string') {
        marketing = marketingAnswer !== 'No';
        isWelsh = marketingAnswer === 'Welsh';
      }

      const additionalInformation = `Number of attendees: ${attendees === '' ? 'Not specified' : attendees}. Marketing language preference: ${
        marketing ? (isWelsh ? 'Welsh' : 'English') : 'Opted out'
      }.`;

      if (count != 0) {
        $('#collapse-register')[0].scrollIntoView();
        return;
      } else {
        $('#collapse-register')[0].scrollIntoView();
        $('form').addClass('d-none');
        $('#loading-form').removeClass('d-none');

        const enquiryData: Accessplanit.Enquiry = {
          OwnerID: 'CPDUNIRSNR',
          CourseTemplateID: $('#course-code').val() as string,
          MarketingOptIn: marketing,
          Title: 'Online enquiry',
          Forename: $('#first-name-input').val() as string,
          Surname: $('#last-name-input').val() as string,
          CompanyName: $('#company-input').val() as string,
          Email: $('#email-input').val() as string,
          AdditionalInformation: additionalInformation,
        };

        EnquiryManager.createOpportunity(
          enquiryData,
          // Success api request
          (data): any => {
            $('#loading-form').addClass('d-none');
            ($('#request-interest') as JQuery<HTMLFormElement>)[0].reset();
            $('#successfully-submitted').removeClass('d-none');
          },
          // Failed api request
          (data): any => {
            $('#collapse-register')[0].scrollIntoView();
            $('#loading-form').addClass('d-none');
            $('#error-submission-alert').removeClass('d-none');
            $('form').removeClass('d-none');

            /* log it in our logging software */

            const xhr = new XMLHttpRequest();
            xhr.open('POST', 'https://api.data.cardiff.ac.uk/logging/v1/log');
            xhr.setRequestHeader('Authorization', 'Bearer ce5ca5f9-6302-3c2c-8f5e-1a6031e08605');
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onreadystatechange = function (): void {
              if (xhr.readyState === 4 && xhr.status === 200) {
                var json = JSON.parse(xhr.responseText);
              }
            };

            const error = {
              log: token,
              token: sessionKey,
              step: 'createOpportunity',
              form: 'enquiry_cpd',
              asset: $('#asset_assetid').val(),
              value: JSON.stringify(data),
              client: 'Could create enquiry log.',
              server: '',
              // eslint-disable-next-line @typescript-eslint/camelcase
              error_code: data.status,
            };
            xhr.send(JSON.stringify(error));
          }
        );
      }
    });

    $('#back-button').on('click', (evt: Event): void => {
      evt.preventDefault();
      $('#successfully-submitted').addClass('d-none');
      $('form').removeClass('d-none');
    });

  };
  return {
    init,
  };
});
