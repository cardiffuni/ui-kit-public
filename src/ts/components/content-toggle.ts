define(['jquery', 'i18n!locale/globals.min', 'helpers/accessibility.min'], (
  $: JQueryStatic,
  i18n: UIKit.I18n,
  accessibility: UIKit.Helpers.Accessibility
): UIKit.Module.ContentToggle => {
  const chevronSVG = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 19.75"><title>Chevron down</title><path d="M28.28,6.13L16,18.41,3.72,6.13,0,9.84l16,16,16-16Z" transform="translate(0 -6.13)"></path></svg>`;

  /**
   * Call bootstrap function ad
   */
  const init = (): void => {
    contentCheck();
    addEventHandlers();
  };

  /**
   * Add content-toggle button to every class
   */
  const contentCheck = (): void => {
    const contentToggle = $('.content-toggle');

    contentToggle.each(function (index): void {
      let elementHeight = this.scrollHeight;
      if (elementHeight < 400) return;

      $(this).attr('data-index', index);
      const leftAligned = $(this).hasClass('content-toggle--left');

      /* Read more btn */
      const button = `<button class="btn btn-link btn-lg btn-content-toggle ${leftAligned ? 'btn-content-toggle--left' : ''} icon-right">
        <span class="btn-content-toggle-text">
          ${getText({ isClosed: true, content: $(this) })}
        </span> ${chevronSVG}
      </button>`;
      $(this).parent().append(button);

      /* Disable all tab'able item in the content area */
      toggleTabbing({
        contentToggle: $(this),
        value: '-1',
      });

      /* Add hidden tabbale item */
      $(this).parent().prepend(`<a href="#" class="sr-only" id="content-toggle-${index}-tab" aria-hidden="true" tabindex="-1">Tab to content</a>`);

      $(this).addClass('gradient');

      if ($(this).closest($("[class*='bg-']")).length) {
        let classes = $(this).closest($("[class*='bg-']")).attr('class');

        if (classes !== undefined) {
          let colour = classes.match(/bg-(\w*\S+)/);
          if (colour !== null) $(this).addClass('gradient-' + colour[1]);
        }
      }
    });
  };

  /**
   * Open or closes content-toggle area.
   */
  const addEventHandlers = (): void => {
    const closedSize = 400;

    $('.content-toggle').css('height', closedSize);
    $('.content-toggle').css('overflow', 'hidden');

    $(document).on('click', '.btn-content-toggle', function (e): void {
      const button = $(this);
      const content = button.prev('.content-toggle');
      const contentGroup = button.closest('.content-group');
      const currentHeight = content.get(0).scrollHeight;
      const currentStyleHeight = parseInt(content.css('height').replace('px', ''));

      if (
        ((currentStyleHeight >= closedSize - 10 && currentStyleHeight <= closedSize + 10) ||
          content.attr('expanded') === undefined ||
          content.attr('expanded') === 'false') &&
        content.hasClass('gradient')
      ) {
        e.stopPropagation();

        const offset: JQueryCoordinates | undefined = content.offset();

        /* Enable all tab'able item in the content area */
        toggleTabbing({
          contentToggle: content,
          value: '',
        });

        button.addClass('active');
        content.removeClass('gradient');

        /* Update text */
        button.find('.btn-content-toggle-text').text(getText({ isClosed: false, content }));

        /* Focus in on the hidden anchor for tabbing usage */
        $(`#content-toggle-${content.attr('data-index')}-tab`)
          .removeAttr('tabindex')
          .focus();

        /* remove height restriction */
        if (accessibility.preferences.reduceMotion) {
          content.css({ height: '' });

          /* Adjust window to be focus in on the element */
          focusSection(offset, contentGroup);
        } else {
          content.animate({ height: currentHeight }, 'slow', function (): void {
            $(this).css('height', '');

            /* Adjust window to be focus in on the element */
            focusSection(offset, contentGroup);
          });
        }
      } else {
        button.removeClass('active');

        /* Update text */
        button.find('.btn-content-toggle-text').text(getText({ isClosed: true, content }));

        /* Disable tabbing */
        $(`#content-toggle-${content.attr('data-index')}-tab`).attr('tabindex', '-1');

        /* Disable all tab'able item in the content area */
        toggleTabbing({
          contentToggle: content,
          value: '-1',
        });

        /* Adjust window to be focus in on the element */
        const offset: JQueryCoordinates | undefined = content.offset();
        hideContent(content, button, closedSize);
        focusSection(offset, contentGroup);
      }
    });
  };

  /**
   * Close a content toggle area.
   *
   * @param content Content toggle div
   * @param button Button to change the text
   * @param height The height to animate to
   */
  const hideContent = (content: JQuery<HTMLElement>, button: JQuery<HTMLElement>, height: number): void => {
    if (accessibility.preferences.reduceMotion) {
      content.css({ height: `${height}px` });
    } else {
      content.animate({ height: `${height}px` }, 'slow');
    }
    content.css('overflow', 'hidden');

    button.find('.btn-content-toggle-text').text(getText({ isClosed: true, content }));
    content.addClass('gradient');
  };

  /**
   * Return the button label
   */
  const getText = ({ isClosed, content }: { isClosed: boolean; content: JQuery<HTMLElement> }): string => {
    const customLabel = content.attr(`data-read-${isClosed ? 'more' : 'less'}-label`);

    if (customLabel !== undefined) return customLabel;
    return i18n.continueReading[isClosed ? 'more' : 'less'];
  };

  /**
   * Scroll to the given offset
   */
  const focusSection = (offset: JQueryCoordinates | undefined, contentGroup: JQuery<HTMLDivElement>): void => {
    if (offset === undefined) return;

    const subtitle = document.querySelector('.nav-section-subtitle');
    const navSection = document.querySelector('.nav-section.sticky-top');
    let scrollOffset = offset.top;

    if (contentGroup !== null) {
      const paddingOffset = parseInt(contentGroup.css('padding-top').replace('px', ''));

      if (paddingOffset !== NaN) scrollOffset -= paddingOffset;
    }

    if (navSection !== null) {
      if (subtitle !== null && subtitle.clientHeight > 0) {
        scrollOffset -= 81;

        if ($('.nav-section-title') !== null) {
          scrollOffset -= 40;
        }
      } else {
        scrollOffset -= 52;

        if ($('.nav-section-title').hasClass('d-block')) {
          scrollOffset -= 40;
        }
      }
    }

    if (accessibility.preferences.reduceMotion) {
      $('html, body').scrollTop(scrollOffset);
    } else {
      $('html, body').animate({ scrollTop: scrollOffset }, 'slow');
    }
  };

  /**
   * Hide all anchor, button, select and inputs when the offset to the content-toggle is 400 or more px of.
   *
   * It will compute the offset of all it's parent until it finds the correct parent(.content-toggle).
   * It will repeating the for 10 parents or until the computed offset is > 400px
   * @param
   */
  const toggleTabbing = ({ contentToggle, value }: { contentToggle: JQuery<HTMLElement>; value: string }): void => {
    contentToggle.find('a, button, select, input').each((index, element): void => {
      let parentFound = false;
      let offsetTotal = 0;
      let iterations = 0;
      let searchElm = $(element);

      while ((!parentFound || iterations < 10) && offsetTotal < 400) {
        if (searchElm.parent().hasClass('content-toggle')) {
          parentFound = true;
          break;
        }

        offsetTotal += searchElm[0].offsetTop;

        iterations++;
        searchElm = $(searchElm).parent();
      }

      if (offsetTotal > 400) {
        $(element).attr('data-offset-top', offsetTotal);
        $(element).attr('tabindex', value);
      }
    });
  };

  return {
    init,
  };
});
