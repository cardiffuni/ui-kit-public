define(['jquery', 'highcharts', 'highcharts/modules/data', 'highcharts/modules/accessibility'], (
  $: JQueryStatic,
  hc: typeof Highcharts
): UIKit.Module.Chart => {
  const init = (): void => {
    $('.chart-table').each((_, chart): void => {
      const wrapper = $(chart);
      const table = wrapper.find('table');
      const tableID = table.attr('id');
      const chartID = wrapper.attr('data-assetid');

      if (chartID === '') return;

      /* add data atts */
      const title = wrapper.attr('data-title');
      const type = wrapper.attr('data-type');
      const axisXTitle = wrapper.attr('data-x-axis-title');
      const axisYTitle = wrapper.attr('data-y-axis-title');
      const legend = wrapper.attr('data-show-legend');

      // trim each td to stop Highcharts including spaces
      table.find('td').each((_, row): void => {
        $(row).text($.trim($(row).text()));
      });

      hc.chart(
        `chart-${chartID}`,
        {
          data: {
            table: tableID,
            dateFormat: 'dd/mm/YYYY',
            parsed(columns): boolean {
              // Fix values from the table that are strings because they have commas in them
              $.each(columns, function (index, column): void {
                // loop through each series
                column.forEach(function (value, index): void {
                  // skip the first column as it is the series label
                  if (index != 0) {
                    // if the value is a string (prob due to comma) convert to a number but ignore dates
                    if (typeof value == 'string' && value.indexOf('/') == -1) {
                      // remove the comma
                      value = value.replace(/,/g, '');
                      // convert to number
                      value = parseFloat(value);
                      // add back into array
                      column[index] = value;
                    }
                  }
                });
              });

              return true;
            },
          },
          colors: [
            '#d4374a', // cardiff red
            '#499f7f', // green
            '#006aaf', // blue
            '#73479c', // purple
            '#f78d1e', // orange
            '#d63694', // pink
            '#a05725', // brown
            '#8fc73e', // light green
            '#ffd100', // yellow
          ],
          chart: {
            type: type, // type
            style: {
              fontFamily: 'franklin_gothic_fs_bookRg, Arial, Helvetica, sans-serif',
            },
          },
          title: {
            text: title,
            style: {
              fontSize: '1.0625rem',
            },
          },
          legend: {
            enabled: legend === 'true',
            itemStyle: {
              fontWeight: 'normal',
            },
            margin: 20,
          },
          yAxis: {
            allowDecimals: false,
            title: {
              text: axisYTitle,
            },
            labels: {
              formatter(this): string {
                return hc.numberFormat(this.value, -1, undefined, ',');
              },
            },
          },
          xAxis: {
            type: 'datetime',
            title: {
              text: axisXTitle,
            },
            dateTimeLabelFormats: {
              day: '%e %b',
            },
          },
          navigation: {
            buttonOptions: {
              enabled: false,
            },
          },
        },
        (): void => {
          /* The table has finished processes */
          table.attr('data-loaded', 'true');
          const tableCompleted = new CustomEvent(`highchart-table-created-${chartID}`);

          tableCompleted.initCustomEvent(`highchart-table-created-${chartID}`, true, true, {});

          /* Notify table-collapse.ts that the table has finished processing */
          document.dispatchEvent(tableCompleted);
        }
      );
    });
  };

  return {
    init,
  };
});
