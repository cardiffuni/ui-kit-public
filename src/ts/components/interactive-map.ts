define(['leaflet'], (L: any): UIKit.Module.InteractiveMap => {
  let data: UIKit.MapData = {} as UIKit.MapData;
  let markers: L.GeoJSON = {} as L.GeoJSON;
  let activeFilter = 'all';
  let map: L.Map = {} as L.Map;
  let geoJson: any[] = [];

  const desktopViewport = 980;

  let hasPanel = false;

  /* flag for resize handler */
  let running = false;

  /**
   * Initialiser for the interactive map
   */
  const init = (): void => {
    const mapElm = document.querySelector('.map');

    if (mapElm !== null) {
      const mapDataSelector = mapElm.getAttribute('data-map-data');

      if (mapDataSelector === null) return;

      const mapData = (window as any)[mapDataSelector] as UIKit.MapData;

      /* don't render map if mapData is not available */
      if (mapData === undefined) return;
      data = mapData;

      hasPanel = document.querySelector('.map__panel') !== null;

      mapElm.classList.add('map--panel-closed');
      const mapControls = mapElm.querySelector('.map__panel__controls');

      if (mapControls !== null && mapControls !== undefined) {
        /* Detect when the user has toggled the panel */
        if (data.panel.enabled) {
          mapControls.addEventListener('click', (): void => {
            toggleMapPanel(mapElm as HTMLElement);
            $('#map-modal').remove();
          });
        }
      }

      const mapConfig = {
        tileLayer: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attribution: 'Map data &copy; <a href="http://www.osm.org">OpenStreetMap</a>',
      };

      map = L.map(`map`, {
        center: [data.items[0].coordinates.lat, data.items[0].coordinates.long],
        zoom: 16,
      });

      L.tileLayer(mapConfig.tileLayer, {
        attribution: mapConfig.attribution,
      }).addTo(map);

      /* Generate map with all markers */
      generateMarkers({
        id: 'map',
        element: mapElm as HTMLElement,
      });

      /* filter the map markers */
      $('.filter .filter__items .btn').on('click', (evt): void => {
        const filter = evt.target.dataset.target;

        if (filter !== undefined) {
          activeFilter = filter.replace('.', '');

          /* Close the view panel */
          (document.querySelector('.map__panel') as HTMLElement).classList.remove('map__panel--view"');

          generateMarkers({
            id: 'map',
            element: mapElm as HTMLElement,
          });
        }
      });

      /* Re-render map when the map is focused */
      $('#tab-link-map').on('shown.bs.tab', (): void => {
        mapElm.classList.add('map--panel-open');

        /* re-render the map */
        requestAnimationFrame((): void => {
          map.invalidateSize(true);

          recenterMap();
        });
      });

      /**
       * When a 'view location' or card-panel list button is clicked, re-render the map.
       * And Show the map item on the map
       * and open a modal/ panel based on viewport
       */
      $(document).on('click', 'a[data-map-marker-key]', (evt): void => {
        evt.preventDefault();

        $('#tab-link-map').tab('show');

        /* re-render the map */
        requestAnimationFrame((): void => {
          map.invalidateSize(true);

          recenterMap();
        });

        /* If there are filters with mode */
        if (document.querySelector('.filter.filter--with-mode') && !mapElm.classList.contains('d-grid')) {
          const mapFilter = document.querySelector<HTMLAnchorElement>('.filter.filter--with-mode a[data-target=".map"]');

          if (mapFilter !== null) {
            /* trigger a click event to focus the map mode filter */
            mapFilter.click();
          }
        }

        const key = (evt.currentTarget as HTMLElement).dataset.mapMarkerKey || '0';
        const index = parseInt(key);

        /* Show modal/ panel and make the marker active */
        setGeoItems();

        showMapItem(
          {
            element: mapElm as HTMLElement,
            id: '',
          },
          key,
          {
            lat: data.items[index].coordinates.lat,
            lng: data.items[index].coordinates.long,
          }
        );
      });

      /* When the window resize, check whether to show the modal or not */
      window.addEventListener('resize', modalResizeHandler);

      /** When user hover over a map-marker key, make it active */
      $(document).on('mouseover', 'a[data-map-marker-key]', (evt): void => {
        document.getElementById(`marker-${(evt.currentTarget as HTMLElement).dataset.mapMarkerKey}`)?.classList.add('active');
      });

      /** When user no longer hover over a map-marker key, make it un-active */
      $(document).on('mouseout', 'a[data-map-marker-key]', (evt): void => {
        const key = (evt.currentTarget as HTMLElement).dataset.mapMarkerKey;

        /* Prevent mouse out from removing class when focusing in on the map marker */
        if (document.getElementById(`view-panel-${key}`) === null) {
          /* The view panel is not actively showing this marker */
          document.getElementById(`marker-${(evt.currentTarget as HTMLElement).dataset.mapMarkerKey}`)?.classList.remove('active');
        } else {
          /* The view panel is actively showing this marker, so ensure the marker has the active class */
          $('.map-svg-marker').removeClass('active');
          document.getElementById(`marker-${(evt.currentTarget as HTMLElement).dataset.mapMarkerKey}`)?.classList.add('active');
        }
      });
    }
  };

  /**
   * Check whether to show the modal or panel based on viewport
   */
  const modalResizeHandler = (): void => {
    if (running) return;

    /* remove the event listener if map-modal no longer exists */
    if (document.getElementById('map-modal') === null) {
      window.removeEventListener('resize', modalResizeHandler);
    }

    window.requestAnimationFrame((): void => {
      const modal = document.getElementById('map-modal');

      /* remove the event listener if map-modal no longer exists */
      if (modal === null) {
        window.removeEventListener('resize', modalResizeHandler);
      } else {
        if (window.innerWidth >= desktopViewport) {
          if (modal.classList.contains('show')) {
            $('#map-modal').modal('hide');
          }
        } else {
          if (!modal.classList.contains('show')) {
            $('#map-modal').modal('show');
          }
        }
      }
      running = false;
    });

    running = true;
  };

  /**
   * Gets a Leaflet marker
   *
   * @param geoJsonPoint data to render with the marker
   * @param latlng coordinates
   */
  const getMarkerIcon = (geoJsonPoint: any, latlng: [number, number]): void =>
    L.marker(latlng, {
      title: geoJsonPoint.title,
      key: geoJsonPoint.key,
      icon: L.divIcon({
        html: `<div class="map-svg-marker" id="marker-${geoJsonPoint.key}">
            <svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.26 20.799"><defs><clipPath id="path_${geoJsonPoint.key}"><path d="M51.35 227.793c0 .268 2.863.486 6.394.486s6.393-.217 6.393-.486-2.863-.485-6.393-.485-6.394.217-6.394.485" transform="translate(-51.35 -227.308)" fill="none"/></clipPath></defs><g><g><g clip-path="url(#path_${geoJsonPoint.key})" transform="translate(.979 19.829)"><g data-name="Group 60"><g opacity=".25"><path  fill="#333" d="M-2.957-1.801h18.701v4.572H-2.957z"/></g></g></g></g><path d="M7.129.737a6.477 6.477 0 016.393 6.557c0 6.624-6.393 12.293-6.393 12.293S.736 13.918.736 7.294A6.477 6.477 0 017.129.737z"  stroke="#333" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.473"/></g></svg>
          </div>`,
        iconSize: [24, 35],
        iconAnchor: [12, 35],
      }),
    });

  /**
   *
   * All all markers to the map and attach event listeners for when a
   * marker is clicked or hovered.
   *
   * @param options map information
   */
  const addMarkersToMap = (options: { id: string; element: HTMLElement }): void => {
    if (typeof markers.remove === 'function') {
      markers.remove();
    }

    markers = L.geoJSON(geoJson, {
      pointToLayer: getMarkerIcon,
    });

    markers
      .addTo(map)
      .on('click', (evt: any): void => {
        /* Marker is clicked */
        showMapItem(options, evt.layer.options.key, evt.latlng);
      })
      .on('mouseover', (evt): void => {
        /* Cursor is hovering the marker */
        document.querySelector(`a[data-map-marker-key="${evt.propagatedFrom.options.key}"]`)?.classList.add('active');
      })
      .on('mouseout', (evt): void => {
        /* Cursor is not longer hovering the marker */
        document.querySelector(`a[data-map-marker-key="${evt.propagatedFrom.options.key}"]`)?.classList.remove('active');
      });

    /* fit the bounds if there are any markers available */
    recenterMap();
  };

  /**
   * Adds all markers to the map and renders the default teaser
   * @param options Map data
   */
  const generateMarkers = ({ element, id }: { element: HTMLElement; id: string }): void => {
    setGeoItems();

    addMarkersToMap({
      id,
      element: element,
    });

    setMapList();
  };

  /**
   * Gets the geo json based on the active filter
   * @returns list of geoJson items
   */
  const setGeoItems = (): void => {
    let markerItems: any[] = [];

    data.items.forEach((item, index): void => {
      let show = activeFilter === 'all';

      if (item.filters !== undefined) {
        item.filters.forEach((filter): void => {
          if (filter.key === activeFilter) {
            markerItems.push({
              title: item.name,
              type: 'Feature',
              key: index,
              geometry: {
                type: 'Point',
                coordinates: [item.coordinates.long, item.coordinates.lat],
              },
            });
            return;
          }
        });
      }

      if (show) {
        markerItems.push({
          title: item.name,
          type: 'Feature',
          key: index,
          geometry: {
            type: 'Point',
            coordinates: [item.coordinates.long, item.coordinates.lat],
          },
        });
      }
    });

    geoJson = markerItems;
  };

  /**
   * Generates the card content for the modal and the card
   * @param item Map item to show
   */
  const generateCardContent = (item: UIKit.MapDataItem): { card: string; modal: string } => {
    let linksHTML = '';
    let categoryHTML = '';
    let subtitleHTML = '';
    let imageHTML = {
      card: '',
      modal: '',
    };

    /* Generate the linksHTML, ignore the last item */
    item.links.forEach((link, index): void => {
      if (index === item.links.length - 1) return;
      linksHTML += `<a href="${link.url}" target="_blank" class="btn btn-outline mb-5" aria-label="${link.aria}" ${link.data}>${link.icon} ${link.text}</a>`;
    });

    if (item.image.exists) {
      imageHTML.card = `<img class="lazy"
        srcset="${item.image.cdn}${item.image.path}?w=375&ar=16:9&fit=crop&q=60&auto=format 375w,
          ${item.image.cdn}${item.image.path}?w=470&ar=16:9&fit=crop&q=60&auto=format 470w,
          ${item.image.cdn}${item.image.path}?w=510&ar=16:9&fit=crop&q=60&auto=format 510w,
          ${item.image.cdn}${item.image.path}?w=575&ar=16:9&fit=crop&q=60&auto=format 575w"
        src="${item.image.cdn}${item.image.path}?w=350&ar=16:9&fit=crop&q=60&auto=format"
        sizes="(max-width: 767px) 80vw, (max-width: 970px) 60vw, (min-width: 971px) 375px"
        alt="${item.image.alt}">
      `;
      imageHTML.modal = `<img class="lazy"
        srcset="${item.image.cdn}${item.image.path}?w=375&ar=16:9&fit=crop&q=60&auto=format 375w,
          ${item.image.cdn}${item.image.path}?w=470&ar=16:9&fit=crop&q=60&auto=format 470w,
          ${item.image.cdn}${item.image.path}?w=510&ar=16:9&fit=crop&q=60&auto=format 510w,
          ${item.image.cdn}${item.image.path}?w=575&ar=16:9&fit=crop&q=60&auto=format 575w,
          ${item.image.cdn}${item.image.path}?w=768&ar=16:9&fit=crop&q=60&auto=format 768w"
        src="${item.image.cdn}${item.image.path}?w=350&ar=16:9&fit=crop&q=60&auto=format"
        sizes="(max-width: 767px) 100vw"
        alt="${item.image.alt}">
      `;
    }

    if (item.category !== undefined && item.category.exists) {
      categoryHTML = `<span class="card__category">${item.category.text}</span>`;
    }
    if (item.subtitle.exists) {
      subtitleHTML = `<span class="card__subtitle">${item.subtitle.text}</span>`;
    }

    return {
      card: `<div class="card__header p-0">
        <a href="#" class="d-flex text-tuatara align-items-center py-15 px-20 f-fg-med" id="back-to-map-list">
            <svg class="icon mr-10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32">
            <title>Chevron left</title>
            <path d="M25.88,28.28L13.59,16,25.88,3.72,22.16,0l-16,16,16,16Z" transform="translate(-6.13 0)"></path>
          </svg>Back to ${data.panel.title.charAt(0).toLowerCase()}${data.panel.title.slice(1)}
        </a>
      </div>
      ${imageHTML.card}
      <div class="card__body">
        ${categoryHTML}
        <h3 class="card__title">${item.name}</h3>
        ${subtitleHTML}
        <p>${item.teaser}</p>
      </div>
      <div class="card__footer d-flex flex-column">
        ${linksHTML}
      </div>`,
      modal: `<div class="card card--flat">
        ${imageHTML.modal}
        <div class="card__body">
          ${categoryHTML}
          <h3 class="card__title">${item.name}</h3>
          ${subtitleHTML}
          <p>${item.teaser}</p>
        </div>
        <div class="card__footer  d-flex flex-column">
          ${linksHTML}
        </div>
      </div>`,
    };
  };

  /**
   * Set the map list view based on the current active filter.
   */
  const setMapList = (): void => {
    const panel = document.querySelector('.map__panel__contents__list');
    if (panel !== null) {
      let defaultList = `<div class="card__body"><ul class="list list--bordered">`;

      data.items.forEach((item, index): void => {
        let show = activeFilter === 'all';

        if (item.filters !== undefined) {
          item.filters.forEach((filter): void => {
            if (filter.key === activeFilter) {
              show = true;
              return;
            }
          });
        }
        if (show) {
          let imageHTML = '';

          if (item.image.exists) {
            imageHTML = `<img src="${item.image.cdn}${item.image.path}?w=70&ar=16:9&fit=crop&q=60&auto=format"
            alt="${item.image.alt}">`;
          }
          defaultList += `<li> 
            <a href="#" data-lat="${item.coordinates.lat}" data-long="${item.coordinates.long}" data-map-marker-key="${index}">
              ${imageHTML}<span>${item.name}</span>
            </a>
          </li>`;
        }
      });
      defaultList += '</ul></div>';

      panel.innerHTML = defaultList;
    }
  };

  /**
   * Opens the panel
   *
   * @param mapElm Map element to open
   */
  const openMapPanel = (mapElm: HTMLElement): void => {
    if (mapElm.classList.contains('map--panel-open')) return;
    mapElm.classList.add('map--panel-transitioning');
    mapElm.classList.add('map--panel-open');
    mapElm.classList.remove('map--panel-closed');
    (mapElm.querySelector('.map__panel') as HTMLElement).classList.add('map__panel--view');
  };

  /**
   * Toggle the map panel
   *
   * @param mapElm Map element to toggle
   */
  const toggleMapPanel = (mapElm: HTMLElement): void => {
    mapElm.classList.add('map--panel-transitioning');
    mapElm.classList.toggle('map--panel-open');
    mapElm.classList.toggle('map--panel-closed');

    if (!mapElm.classList.contains('map--panel-open')) {
      /* recenter map when user closes panel */

      recenterMap();
    }
  };

  /**
   * Opens the panel/ modal based on what the user has chosen to view
   *
   * @param options map data
   * @param key key of the marker
   * @param latlng coordinates
   */
  const showMapItem = (options: { id: string; element: HTMLElement }, key: string, latlng: { lat: number; lng: number }): void => {
    if (hasPanel) {
      openMapPanel(options.element);
      const panel = document.querySelector('.map__panel__contents__view');
      (document.querySelector('.map__panel') as HTMLElement).classList.add('map__panel--view');

      if (panel !== null) {
        const content = generateCardContent(data.items[parseInt(key)]);
        panel.innerHTML = content.card;
        panel.setAttribute('id', `view-panel-${key}`);

        /* create a modal */
        const modalHTML = `<div id="map-modal" class="modal fade" tabindex="-1" aria-modal="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <span class="flex-grow-1" aria-hidden="true"></span>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" title="Close">
                  <svg class="icon" focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
                    <title>Close</title>
                    <path d="M29.75,24.5l0.06,0.06a0.69,0.69,0,0,1,.13.16,0.12,0.12,0,0,0,0,.09,0.69,0.69,0,0,1,0,.25v0.22a1,1,0,0,1-.25.47l-4,4a0.56,0.56,0,0,1-.22.16,0.84,0.84,0,0,1-.47.09,0.69,0.69,0,0,1-.25,0l-0.06-.06a0.36,0.36,0,0,1-.19-0.09L16,21.25,7.44,29.81a0.27,0.27,0,0,1-.16.09L7.19,30a0.69,0.69,0,0,1-.25,0,0.47,0.47,0,0,1-.22,0,0.73,0.73,0,0,1-.47-0.22l-4-4a0.56,0.56,0,0,1-.16-0.22A0.84,0.84,0,0,1,2,25.06a0.69,0.69,0,0,1,0-.25l0.06-.09a0.26,0.26,0,0,1,.09-0.16L10.75,16,2.19,7.44s0-.06-0.06-0.09a0.55,0.55,0,0,1-.06-0.16A0.74,0.74,0,0,1,2,6.94a0.69,0.69,0,0,1,0-.25,0.73,0.73,0,0,1,.22-0.44l4-4a0.56,0.56,0,0,1,.22-0.16A0.84,0.84,0,0,1,6.93,2a0.72,0.72,0,0,1,.25.06H7.28a0.66,0.66,0,0,1,.16.13L16,10.75l8.56-8.56a1,1,0,0,1,.19-0.12,0.06,0.06,0,0,0,.06,0,0.69,0.69,0,0,1,.25,0h0.25a1,1,0,0,1,.44.25l4,4a0.56,0.56,0,0,1,.16.22,0.84,0.84,0,0,1,.09.47,0.69,0.69,0,0,1,0,.25,0.06,0.06,0,0,0,0,.06l-0.12.19L21.25,16Z" transform="translate(-2 -2)"></path>
                  </svg>
                </button>
              </div>
              <div class="modal-body p-0">${content.modal}</div>
            </div>
          </div>
        </div>`;

        /* remove the current modal */
        $('#map-modal').remove();
        $('.content').append(modalHTML);

        if (window.innerWidth < desktopViewport) {
          $('#map-modal').modal('show');
        }

        $('#map-modal .close').on('click', (evt): void => {
          evt.preventDefault();

          $('#map-modal').modal('hide');
          $('#map-modal').on('hidden.bs.modal', (): void => {
            $('#map-modal').remove();
          });
        });

        requestAnimationFrame((): void => {
          const allMarkers = document.querySelectorAll(`.map-svg-marker:not(#marker-${key}`);

          if (allMarkers !== null) {
            allMarkers.forEach((item): void => item.classList.remove('active'));
            (document.getElementById(`marker-${key}`) as HTMLElement).classList.add('active');
          }
        });

        (document.getElementById('back-to-map-list') as HTMLAnchorElement).addEventListener(
          'click',
          (evt): void => {
            evt.preventDefault();

            (document.querySelector('.map__panel') as HTMLElement).classList.remove('map__panel--view');
            $('.map-svg-marker').removeClass('active');
            $('.map__panel__contents__view').removeAttr('id');

            recenterMap();
          },
          {
            once: true,
          }
        );
      }
    }

    /** Center the map on desktop */
    if (window.innerWidth >= desktopViewport) {
      map.fitBounds([
        [latlng.lat, latlng.lng],
        [latlng.lat, latlng.lng],
      ]);
    }
  };

  /**
   * Recenter the map based on the current geo json.
   */
  const recenterMap = (): void => {
    if (geoJson.length > 0) {
      map.fitBounds(markers.getBounds());
    }
  };
  return {
    init,
  };
});
