define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.PostgraduateSearch => {
  /**
   * Add an change event on the radio fields
   * @watch .searchform input[name="collection"]
   * @since 2.0.1
   * @author Christopher Atwood
   */
  const init = (): void => {
    $('#postgraduate-search-form input[name="collection"]').on('change', (evt: JQuery.ChangeEvent): void => {
      const form: JQuery<HTMLFormElement> = $(evt.target).closest('.form');
      const collection: HTMLInputElement = evt.target;
      const collectionName = collection.value as 'pg-taught-courses' | 'pgr-courses';

      if (form !== null) {
        form.get(0).action = i18n.postgraduate[collectionName];
      }
    });
  };
  return {
    init,
  };
});
