define(['i18n!locale/globals.min'], (i18n: UIKit.I18n): UIKit.Module.ContentUnavailable => {
  const widths = {
    full: 570,
    md: 250,
    sm: 180,
  };

  const getDisabledYoutubeHTML = ({
    serviceProvider,
    youtubeID,
    index,
    preferencesToShow,
    alt,
    url,
    width,
  }: {
    serviceProvider: ServiceProvider | string;
    youtubeID?: string;
    index: number;
    alt: string;
    url: string;
    width: number;
    preferencesToShow: string[];
  }): string => {
    const unavailableIcon = `<svg class="icon {{iconClasses}}" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.9619 45.9619"><title>Video disabled icon</title><path d="M4.015,39.1188l33.6292-33.63c-.0548-.0007-.108-.0078-.1631-.0078h-29a8,8,0,0,0-8,8v19A7.9915,7.9915,0,0,0,4.015,39.1188Z" transform="translate(0 0)" style="fill:#aaa"/><path d="M41.9476,6.8434,8.3175,40.4728c.055.0011.1084.0082.1636.0082h29a8,8,0,0,0,8-8v-19A7.9915,7.9915,0,0,0,41.9476,6.8434Z" transform="translate(0 0)" style="fill:#aaa"/><polygon points="17.481 15.481 17.481 31.481 30.481 23.481 17.481 15.481" style="fill:#fff"/><rect x="-8.519" y="21.981" width="63" height="2" transform="translate(-9.5191 22.981) rotate(-45)" style="fill:#aaa"/></svg>`;

    let videoTemplate = `<div class="image image--overlay-content content-unavailable content-unavailable--{{type}} mb-0 h-100" role="alert" role="alertdialog" aria-labelledby="content-unavailable-${index}-title" aria-describedby="content-unavailable-${index}-content">
      <div class="image__content text-center px-0 py-0">{{minimalContent}}{{fullContent}}</div>
      <div class="image__wrapper">
        <img class="w-100" src="https://img.youtube.com/vi/${youtubeID}/mqdefault.jpg" alt="alt text">
      </div>
    </div>`;

    const fullContent = `<h3 id="content-unavailable-${index}-title" class="lead"><strong>${i18n.contentUnavailable.title}</strong></h3>
      <p id="content-unavailable-${index}-content">${i18n.contentUnavailable.contentMessage}</p>
      <div class="d-flex pl-20"><ul class="list text-left mx-auto">${preferencesToShow.join('')}</ul></div>
      <div class="d-flex justify-content-center align-items-center flex-wrap">
        <a href="#" class="btn btn-success cookie-update-preferences d-inline-block mb-20" data-cookie-preference-functionality="true" data-cookie-preference-advertising="true">
          ${i18n.cookieBanner.enable}
        </a>
        <a href="${url}" class="text-underline mb-20 px-20" aria-label="${alt}">
          ${i18n.contentUnavailable.alternativeLink.replace('%s', serviceProvider)}
        </a>
      </div>`;

    const mdContent = `<span id="content-unavailable-${index}-title" class="sr-only">${i18n.contentUnavailable.title}</span>
      <div id="content-unavailable-${index}-content">
        <a href="#" class="f-fg-med px-15 text-underline d-block mb-0">${i18n.contentUnavailable.howToWatch}</a>
      </div>`;
    const smContent = `<span id="content-unavailable-${index}-title" class="sr-only">${i18n.contentUnavailable.title}</span>
      <div id="content-unavailable-${index}-content">
        <a href="#" class="f-fg-med px-15 text-underline d-block mb-0 text-sm">${i18n.contentUnavailable.howToWatch}</a>
      </div>`;
    const xsContent = `<span id="content-unavailable-${index}-title" class="sr-only">${i18n.contentUnavailable.title}</span>
      <div id="content-unavailable-${index}-content">
        <a href="#" class="f-fg-med px-15 text-underline d-block mb-0 text-sm">${i18n.contentUnavailable.howToWatch}</a>
      </div>`;

    let html = videoTemplate;

    html = html.replace(
      '{{fullContent}}',
      `<div class="content-unavailable__content px-20 pt-20 pb-0">${unavailableIcon
        .replace(/\{\{iconType\}\}/g, 'content')
        .replace('{{iconClasses}}', 'icon-xl mb-15')}${fullContent}</div>`
    );

    /* Build the template */
    if (width >= widths.full) {
      html = html.replace('{{minimalContent}}', '');
      html = html.replace('{{type}}', 'full-content');
    } else {
      html = html.replace('{{type}}', 'minimal-content');

      if (width >= widths.md) {
        html = html.replace(
          '{{minimalContent}}',
          `<div class="content-unavailable__overview">${unavailableIcon
            .replace(/\{\{iconType\}\}/g, 'overview')
            .replace('{{iconClasses}}', 'icon-xl mb-15')}${mdContent}</div>`
        );
      } else if (width >= widths.sm) {
        html = html.replace(
          '{{minimalContent}}',
          `<div class="content-unavailable__overview">${unavailableIcon
            .replace(/\{\{iconType\}\}/g, 'overview')
            .replace('{{iconClasses}}', 'icon-lg mb-10')}${smContent}</div>`
        );
      } else {
        html = html.replace('{{minimalContent}}', `<div class="content-unavailable__overview">${xsContent}</div>`);
      }
    }

    return html;
  };
  const getDisabledEmbedHTML = ({
    serviceProvider,
    youtubeID,
    index,
    preferencesToShow,
    alt,
    url,
  }: {
    serviceProvider: ServiceProvider | string;
    youtubeID?: string;
    index: number;
    alt: string;
    url: string;
    preferencesToShow: string[];
  }): string => {
    const unavailableIcon = `<svg class="icon {{iconClasses}}" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.9619 45.9619"><title>Video disabled icon</title><path d="M4.015,39.1188l33.6292-33.63c-.0548-.0007-.108-.0078-.1631-.0078h-29a8,8,0,0,0-8,8v19A7.9915,7.9915,0,0,0,4.015,39.1188Z" transform="translate(0 0)" style="fill:#aaa"/><path d="M41.9476,6.8434,8.3175,40.4728c.055.0011.1084.0082.1636.0082h29a8,8,0,0,0,8-8v-19A7.9915,7.9915,0,0,0,41.9476,6.8434Z" transform="translate(0 0)" style="fill:#aaa"/><polygon points="17.481 15.481 17.481 31.481 30.481 23.481 17.481 15.481" style="fill:#fff"/><rect x="-8.519" y="21.981" width="63" height="2" transform="translate(-9.5191 22.981) rotate(-45)" style="fill:#aaa"/></svg>`;

    let videoTemplate = `<div class="image image--overlay-content mb-0 border border-grey-light" role="alert" role="alertdialog" aria-labelledby="content-unavailable-${index}-title" aria-describedby="content-unavailable-${index}-content">
      <div class="image__content text-center pb-0">
        {{icon}}
        {{content}}
      </div>
      <div class="image__wrapper">
        <img class="w-100" src="https://img.youtube.com/vi/${youtubeID}/mqdefault.jpg" alt="alt text">
      </div>
    </div>`;

    const generalTemplate = `<div class="well bg-white text-center pb-0" role="alert" role="alertdialog" aria-labelledby="content-unavailable-${index}-title" aria-describedby="content-unavailable-${index}-content">
      {{content}}
    </div>`;

    const content = `<h3 id="content-unavailable-${index}-title" class="lead mt-15"><strong>${i18n.contentUnavailable.title}</strong></h3>
      <p id="content-unavailable-${index}-content">${i18n.contentUnavailable.contentMessage}</p>
      <div class="d-flex"><ul class="list text-left mx-auto">${preferencesToShow.join('')}</ul></div>
      <div class="d-flex justify-content-center align-items-center flex-wrap">
        <a href="#" class="btn btn-success cookie-update-preferences d-inline-block mb-20" data-cookie-preference-functionality="true" data-cookie-preference-advertising="true">
          ${i18n.cookieBanner.enable}
        </a>
        <a href="${url}" class="text-underline mb-20 px-20" aria-label="${alt}">
          ${i18n.contentUnavailable.alternativeLink.replace('%s', serviceProvider)}
        </a>
      </div>`;

    if (serviceProvider === 'Youtube' && youtubeID !== undefined)
      return videoTemplate
        .replace('{{content}}', content)
        .replace('{{icon}}', unavailableIcon.replace('{{iconClasses}}', 'icon-xl').replace('{{iconType}}', 'full'));

    return generalTemplate.replace('{{content}}', content);
  };

  return {
    getDisabledYoutubeHTML,
    getDisabledEmbedHTML,
  };
});
