define(['jquery', 'observer'], ($: JQueryStatic, intersect: any): UIKit.Module.LazyLoader => {
  var IntersectionObserver = intersect;

  const init = (): void => {
    let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
    let lazyImageObserver = new IntersectionObserver(function (entries: any): void {
      entries.forEach(function (entry: any): void {
        if (entry.intersectionRatio > 0) {
          let lazyImage = $(entry.target) as JQuery<HTMLImageElement>;
          lazyImage.attr('src', lazyImage.attr('data-src') || '');
          lazyImage.attr('srcset', lazyImage.attr('data-srcset') || '');
          lazyImage.removeClass('lazy');
          lazyImageObserver.unobserve(lazyImage[0]);
        }
      });
    });

    let lazyIFrames = document.querySelectorAll<HTMLIFrameElement>('iframe.lazy');
    let lazyIFrameObserver = new IntersectionObserver(function (entries: any): void {
      entries.forEach(function (entry: any): void {
        if (entry.intersectionRatio > 0) {
          let lazyIFrame = $(entry.target) as JQuery<HTMLIFrameElement>;
          lazyIFrame.attr('src', lazyIFrame.attr('data-src') || '');
          lazyIFrame.removeClass('lazy');
          lazyIFrameObserver.unobserve(lazyIFrame[0]);
        }
      });
    });

    lazyImages.forEach(function (lazyImage): void {
      lazyImageObserver.observe(lazyImage);
    });

    if (lazyIFrames !== null) {
      lazyIFrames.forEach((lazyIFrame, index): void => {
        const modal = $(lazyIFrame).closest('.modal');

        if (modal !== null) {
          $(modal).on('shown.bs.modal', (): void => {
            /* check whether it requires lazy loading */
            if (lazyIFrame.classList.contains('lazy')) {
              /* Add a loader */
              $(modal)
                .find('.modal-body')
                .append(
                  `<span id="iframe-modal-${index}" class="loader d-block loader--active loader--silver loader--centered" aria-busy="true" aria-live="polite"></span>`
                );

              /* Attach the src from the data-src */
              lazyIFrame.setAttribute('src', lazyIFrame.dataset.src || '');
              lazyIFrame.classList.remove('lazy');

              /* When the iframe is loaded, remove the loader */
              lazyIFrame.addEventListener('load', (): void => {
                $(`#iframe-modal-${index}`).remove();
              });
            } else {
              $(modal).off('shown.bs.modal');
            }
          });
        } else {
          lazyIFrameObserver.observe(lazyIFrame);
        }
      });
    }
  };

  return {
    init,
  };
});
