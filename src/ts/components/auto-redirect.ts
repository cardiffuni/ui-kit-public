define(['jquery'], ($: JQueryStatic): UIKit.Module.AutoRedirect => {
  const init = (): void => {
    // Select , radiobuttons, checkboxes
    $(document).on('change', 'input[data-auto-redirect="true"], select[data-auto-redirect="true"]', checkInput);
  };

  // Checks the target form input and submits the form on either a change or blur event.
  const checkInput = (evt: JQuery.ChangeEvent | JQuery.BlurEvent): void => {
    const element: JQuery<HTMLInputElement> = $(evt.target);

    if (element[0].type === 'select-one' || element[0].type === 'radio' || element[0].type === 'checkbox') {
      window.location.href = element[0].value;
    }
  };

  return {
    init,
  };
});
