define(['jquery'], ($: JQueryStatic): UIKit.Module.SubmitForm => {
  const init = (): void => {
    let lang = $('html').attr('lang') || 'en';

    $('.form').submit(function (): void {
      if ($('.form input[type="submit"]').data('submitted') == '1') {
      } else {
        $('.form input[type="submit"]').attr('data-submitted', '1');
        $('.form input[type="submit"]').css('pointer-events', 'none');
        $('.form input[type="submit"]').css('background', '#d3d3d2');
        $('.form input[type="submit"]').blur();

        if (lang === 'cy') {
          $('.form input[type="submit"]').val('Cyflwyno');
        } else {
          $('.form input[type="submit"]').val('Submitting');
        }
      }
    });
  };
  return {
    init,
  };
});
