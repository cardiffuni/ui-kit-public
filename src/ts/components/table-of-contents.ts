define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.TableOfContents => {
  const init = (): void => {
    const tableOfContentsItems: string[] = [];

    /* Get all H2s on the page */
    $('.document-content h2').each((index: number, heading: HTMLElement): void => {
      const item = $(heading);
      const headingClone = $(item).clone();
      if (item.parent('.contact').length === 0) {
        headingClone.find('.document-section__number').remove();

        let id = item.attr('id');
        const sectionName = headingClone.text().trim();

        /* If the heading is missing a ID, add one */
        if (id === undefined || id === '') {
          const headingID = headingClone
            .text()
            .trim()
            .replace(/&nbsp;/g, '-')
            .replace(/[ \.!@£$%^&*(){}:"|'":;\[\]`~_]+/gim, '-');

          item.attr('id', headingID);
          id = headingID;
        }

        tableOfContentsItems.push(`<li><a href="#${id}" aria-label="${i18n.tableOfContents.item.aria} ${sectionName}">${sectionName}</a></li>`);
      }
    });

    /** Add the list to the OL. */
    $('#document-toc-content').html(`<ul class="list list-links text-normal mb-30">${tableOfContentsItems.join('')}</ul>`);
  };
  return {
    init,
  };
});
