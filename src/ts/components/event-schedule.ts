define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.EventSchedule => {
  const secondsToMils = 1000;
  const currentTime = new Date();
  const currentTimestamp = currentTime.valueOf();
  const events: UIKit.EventScheudle[] = [];

  const maxDelay = Math.pow(2, 31) - 1;

  const init = (): void => {
    const items = $('.event-schedule__item[data-timestamp-start]');

    if (items === null) return;

    items.each((_, item): void => {
      const event: UIKit.EventScheudle = {
        element: $(item).find('.badge'),
        start: {
          value: parseInt($(item).data('timestamp-start')),
          date: undefined,
        },
        end: {
          value: parseInt($(item).data('timestamp-end')),
          date: undefined,
        },
        handlers: {
          ended: undefined,
          live: undefined,
        },
        status: {
          current: 'upcoming',
          live: false,
          upcoming: false,
          ended: false,
          open: $(item).data('registration-open') === true,
        },
        crossover: parseInt($(item).data('delay') || '60') * secondsToMils,
      };
      event.start.date = new Date(event.start.value);
      event.end.date = new Date(event.end.value);

      events.push(event);
      checkEvent(event, currentTimestamp);
    });

    /* Detect when window comes back in foucs. Ensure the state is always correct.
     * Some browsers will stop timeout if the tab goes into background.
     **/
    document.addEventListener('visibilitychange', (): void => {
      if (document.visibilityState === 'visible') {
        for (let i = 0; i < events.length; i++) {
          const currentTimestamp = new Date().valueOf();
          checkEvent(events[i], currentTimestamp);
        }
      } else {
        /* Clear any timeouts to prevent rescourses beign used in be background */
        for (let i = 0; i < events.length; i++) {
          clearTimeout(events[i].handlers.live);
          clearTimeout(events[i].handlers.ended);
        }
      }
    });
  };

  /**
   * Sets the event status and sets the event listeners if they are required.
   * @param event Event to check
   */
  const checkEvent = (event: UIKit.EventScheudle, currentTimestamp: number): void => {
    event = updateStatus(event, currentTimestamp);
    updateVisuals(event);

    if (event === undefined || event.start.date === undefined || event.end.date === undefined) return;

    /* Add  'Live' event listeners */
    if (
      (!event.status.live && (event.status.upcoming || event.status.open)) ||
      ((event.status.upcoming || event.status.open) && event.start.date !== undefined)
    ) {
      const timeUntilStart = event.start.date.valueOf() - event.crossover - currentTimestamp;

      /* Prevent timeout exceeding max 32bit int */
      if (timeUntilStart > maxDelay) return;

      clearTimeout(event.handlers.live);
      event.handlers.live = setTimeout((): void => {
        const currentTimestamp = new Date().valueOf();
        checkEvent(event, currentTimestamp);
      }, timeUntilStart);
    }

    /* Add  'Ended' event listeners */
    if (!event.status.ended) {
      const timeUntilEnd = event.end.date.valueOf() + event.crossover - currentTimestamp;

      /* Prevent timeout exceeding max 32bit int */
      if (timeUntilEnd > maxDelay) return;

      clearTimeout(event.handlers.ended);
      event.handlers.ended = setTimeout((): void => {
        const currentTimestamp = new Date().valueOf();
        checkEvent(event, currentTimestamp);
      }, timeUntilEnd);
    }
  };

  /**
   * Updates the status of an event item
   * @param event Event to update
   */
  const updateStatus = (event: UIKit.EventScheudle, currentTime: number): UIKit.EventScheudle => {
    if (event.start.date === undefined || event.end.date === undefined) return event;

    if (currentTime >= event.start.date.valueOf() - event.crossover && event.end.date.valueOf() > currentTime - event.crossover) {
      /* If the current time is >= start time - 1 minute and the current datetime is before the event end datetime */
      event.status.live = true;
    } else if (event.end.date.valueOf() <= currentTime) {
      event.status.ended = true;
    } else if (event.status.open) {
    } else {
      event.status.upcoming = true;
    }

    if (event.status.live) {
      event.status.upcoming = false;
      event.status.open = false;
    }

    return event;
  };

  /**
   *
   * Update the visual appearence of the badge based on the event status
   * @param event Event to update the badge
   */
  const updateVisuals = (event: UIKit.EventScheudle): void => {
    if (event.status.ended) {
      event.status.current = 'ended';
      event.element.addClass('badge-inverse').removeClass('badge-info badge-important');
    } else if (event.status.live) {
      event.status.current = 'live';
      event.element.addClass('badge-important').removeClass('badge-info badge-inverse');
    } else if (event.status.open) {
      event.status.current = 'open';
      event.element.addClass('badge-info').removeClass('badge-important badge-inverse');
    } else {
      event.element.addClass('badge-info').removeClass('badge-important badge-inverse');
    }

    event.element.text(i18n.eventStates[event.status.current]);
  };

  return {
    init,
  };
});
