define(['jquery'], ($: JQueryStatic): UIKit.Module.ConditionalFields => {
  const config: any = cfuiConditionalFieldsConfig || {};

  // go
  const init = (): void => {
    // loop through conditional fields array
    $.each(config, (field: any): void => {
      // get each parent input
      const $parentFieldInput = $(".form-conditional input[name*='" + field.parentField + "']");
      // only fire if field is on screen
      if ($parentFieldInput.get(0)) {
        // get the parent block
        const $parentField = $parentFieldInput.closest('.form-conditional');

        // radio parent fields
        if (field.parentType == 'radio') {
          // look for a change in the value from user
          $parentField.find('input[type=radio]').change(function (): void {
            // get current value (after user clicks)
            const currentValue = $("[name*='" + field.parentField + "']:checked").val();
            // go through each child and reveal if necessary
            $.each(field.childFields, function (_, fieldName): void {
              // get each child input
              const $childFieldInputs = $(".form-conditional [name*='" + fieldName + "']");
              // get the child block
              const $childField = $childFieldInputs.closest('.form-conditional');
              changeConditionalField(currentValue, field, $childField);
            });
          });
        }
        // checkbox parent fields
        if (field.parentType === 'checkbox') {
          // look for a change in the value
          $parentField.find('input[type=checkbox]').change((): void => {
            // get the list of checked checkboxes
            const selectedCheckboxes = getCheckboxValues(field.parentField);
            // if there are any, process
            let displayChildren = false;
            // if not revert to default class
            if (selectedCheckboxes.length > 0) {
              // loop through boxes we have checked
              $.each(selectedCheckboxes, function (_, value): void {
                // if one of the checked boxes is one of the values we are looking for, display
                if ($.inArray(value, field.valueWhenChildShown) != -1) {
                  displayChildren = true;
                }
              });
            }

            // go through each child and reveal if necessary
            $.each(field.childFields, (fieldName: string): void => {
              // get each child input
              const $childFieldInputs = $(".form-conditional [name*='" + fieldName + "']");
              // get the child block
              const $childField = $childFieldInputs.closest('.form-conditional');
              // if we should therefore display the fields, do so
              // otherwise revert to default
              if (displayChildren === true) {
                $childField.removeClass(field.hiddenChildClass);
              } else {
                $childField.addClass(field.hiddenChildClass);
              }
            });
          });
        }
        // loop through all child fields
        $.each(field.childFields, (fieldName: string): void => {
          // get each child input
          const $childFieldInputs: JQuery<HTMLElement> = $(".form-conditional [name*='" + fieldName + "']");
          // get the child block
          const $childField: JQuery<HTMLElement> = $childFieldInputs.closest('.form-conditional');
          // apply the default class to the child
          $childField.addClass(field.hiddenChildClass);
          // radio parent field
          if (field.parentType == 'radio') {
            // show or hide depending on parent value when page loads
            changeConditionalField($("[name*='" + field.parentField + "']:checked").val(), field, $childField);
          }

          // checkbox parent field
          if (field.parentType == 'checkbox') {
            // show or hide depending on parent value when page loads
            changeConditionalField($("input[name*='" + field.parentField + "']:checked").val(), field, $childField);
          }
        });
      }
    });
  };

  // utility function to change fields after detection
  const changeConditionalField = (currentValue: undefined | string | number | string[], field: any, $childField: JQuery<HTMLElement>): void => {
    // if the value matches when we should show the child, remove the hidden class
    if ($.inArray(currentValue, field.valueWhenChildShown) != -1) {
      $childField.removeClass(field.hiddenChildClass);
    }
    // if the value matches when we should hide the child, add the hidden class and clear the value
    if ($.inArray(currentValue, field.valueWhenChildHidden) != -1) {
      $childField.addClass(field.hiddenChildClass);
      $childField.find('input').prop('checked', false);
    }
  };

  // utility function to get checkbox values
  const getCheckboxValues = (fieldName: string): (string | number)[] => {
    const input = $(`input[name*='${fieldName}]:checked`) as JQuery<HTMLInputElement>;
    let values = input
      .map((_, el: HTMLInputElement): string | string[] | null | undefined => {
        return el.value;
      })
      .get();

    return values;
  };
  return {
    init,
    changeConditionalField,
    getCheckboxValues,
  };
});
