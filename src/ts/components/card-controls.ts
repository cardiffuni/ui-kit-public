define(['jquery', 'helpers/accessibility'], ($: JQueryStatic, accessibility: UIKit.Helpers.Accessibility): UIKit.Module.Controls => {
  /* Throttle flags */
  let throttled = false;
  let throttledScroll = false;

  /**
   * Initialiser for the card controls module
   */
  const init = (): void => {
    checkForControls();

    /* Attach button clicks to the prev and next button. */
    $('.cards').each((_, item): void => {
      const prevBtn = item.querySelector<HTMLElement>('.controls-item-prev');
      const nextBtn = item.querySelector<HTMLElement>('.controls-item-next');

      if (prevBtn !== null) {
        /* Load the previous lot of cards */
        prevBtn.addEventListener('click', (evt): void => {
          evt.preventDefault();
          scrollCards(item.querySelector<HTMLElement>('.cards__items') as HTMLElement, false);
        });
      }

      if (nextBtn !== null) {
        /* Load the next lot of cards */
        nextBtn.addEventListener('click', (evt): void => {
          evt.preventDefault();
          scrollCards(item.querySelector<HTMLElement>('.cards__items') as HTMLElement, true);
        });
      }
    });

    /* Detect when cards scroll, check whether we can remove disable the prev/ next button */
    $('.cards__items').each((_, item): void => {
      item.addEventListener('scroll', (): void => {
        if (throttledScroll) return;

        window.requestAnimationFrame((): void => {
          checkNavigation(item as HTMLElement);
          throttledScroll = false;
        });

        throttledScroll = true;
      });
    });

    /* When the window resizes, check whether we can remove/disable the prev/ next button */
    window.addEventListener('resize', (): void => {
      // clear the timeout
      if (throttled) return;

      window.requestAnimationFrame((): void => {
        checkForControls();
        throttled = false;
      });

      throttled = true;
    });
  };

  /**
   * Check whether we can scroll more cards into view
   *
   * @param {HTMLElement} cardItems Card items div
   * @param {boolean} forward wether we want to load more or less
   */
  const scrollCards = (cardItems: HTMLElement, forward: boolean): void => {
    /* check if you can scroll any further*/

    if (forward) {
      /* Cards width */
      const { canScroll, parentWidth, maxWidth } = checkAtEnd(cardItems);

      if (canScroll) {
        const newOffset = cardItems.scrollLeft + parentWidth;

        cardItems.parentElement?.querySelector('.controls-item-prev')?.classList.remove('disabled');

        // Feature detection
        if (cardItems.scrollTo !== undefined) {
          cardItems.scrollTo({
            left: newOffset,
            behavior: accessibility.preferences.reduceMotion ? 'auto' : 'smooth',
          });
        } else {
          $(cardItems).animate({ scrollLeft: newOffset }, accessibility.preferences.reduceMotion ? 0 : 'fast');
        }

        if (maxWidth <= newOffset + parentWidth) {
          cardItems.parentElement?.querySelector('.controls-item-next')?.classList.add('disabled');
        }
      }
    } else {
      const { canScroll, parentWidth } = checkAtStart(cardItems);

      if (canScroll) {
        const newOffset = cardItems.scrollLeft - parentWidth;

        cardItems.parentElement?.querySelector('.controls-item-next')?.classList.remove('disabled');

        // Feature detection
        if (cardItems.scrollTo !== undefined) {
          cardItems.scrollTo({
            left: newOffset,
            behavior: accessibility.preferences.reduceMotion ? 'auto' : 'smooth',
          });
        } else {
          $(cardItems).animate({ scrollLeft: newOffset }, accessibility.preferences.reduceMotion ? 0 : 'fast');
        }

        if (cardItems.scrollLeft - parentWidth <= 0) {
          cardItems.parentElement?.querySelector('.controls-item-prev')?.classList.add('disabled');
        }
      }
    }
  };

  /**
   *
   * Enable/ disable next button when there are no more cards to load
   * Enable/ disable previous button when there are card items is at the start of the div
   *
   *
   * @param {HTMLElement} cardItems card items div
   */
  const checkNavigation = (cardItems: HTMLElement): void => {
    const { canScroll } = checkAtEnd(cardItems);

    if (!canScroll) {
      cardItems.parentElement?.querySelector('.controls-item-next')?.classList.add('disabled');
    } else {
      cardItems.parentElement?.querySelector('.controls-item-next')?.classList.remove('disabled');
    }

    const prevOptions = checkAtStart(cardItems);

    if (!prevOptions.canScroll) {
      cardItems.parentElement?.querySelector('.controls-item-prev')?.classList.add('disabled');
    } else {
      cardItems.parentElement?.querySelector('.controls-item-prev')?.classList.remove('disabled');
    }
  };

  /**
   * Checks whether the card items is at the beginning and whether it can scroll backwards
   *
   * @param {HTMLElement} cardItems card items div
   * @return {canScroll: boolean, parentWidth: number, maxWidth: number} properties to detect whether we can scroll more card into view
   */
  const checkAtEnd = (cardItems: HTMLElement): { canScroll: boolean; parentWidth: number; maxWidth: number } => {
    const cardContainerWidth = cardItems.clientWidth;
    const scrollWidth = cardItems.scrollWidth;
    const scrollLeft = cardItems.scrollLeft;

    if (cardContainerWidth !== undefined) {
      return {
        canScroll: scrollWidth - scrollLeft - cardContainerWidth !== 0,
        parentWidth: cardContainerWidth,
        maxWidth: scrollWidth,
      };
    }

    return { canScroll: true, parentWidth: 1170, maxWidth: scrollWidth };
  };

  /**
   * Checks whether the card items is at the beginning and whether it can scroll backwards
   *
   * @param {HTMLElement} cardItems card items div
   * @return {canScroll: boolean, parentWidth: number, maxWidth: number} properties to detect whether we can scroll more card into view
   */
  const checkAtStart = (cardItems: HTMLElement): { canScroll: boolean; parentWidth: number; maxWidth: number } => {
    const cardContainerWidth = cardItems.parentElement?.clientWidth;
    const maxWidth = cardItems.scrollWidth;

    if (cardContainerWidth !== undefined) {
      return {
        canScroll: cardItems.scrollLeft !== 0 && cardContainerWidth < maxWidth,
        parentWidth: cardContainerWidth,
        maxWidth,
      };
    }

    return { canScroll: true, parentWidth: 1170, maxWidth };
  };

  /**
   * Check whether we need to show the next and previous buttons
   */
  const checkForControls = (): void => {
    const cardsContainerWidth = (document.querySelector<HTMLElement>('.cards') as HTMLElement).clientWidth;

    const cardItems = document.querySelectorAll<HTMLElement>('.cards__items .card:not(.d-none)');

    let totalWidth = 0;

    for (var i = 0; i < cardItems.length; i++) {
      totalWidth += cardItems[i].offsetWidth;
    }

    let offset = 0;

    if (window.innerWidth >= 980 && window.innerWidth < 1400) {
      /* Tablet viewport mode */
      offset = 200;
    } else if (window.innerWidth >= 1400) {
      /* XL desktop + 200px for controls */
      offset = -200;
    }

    if (cardsContainerWidth !== undefined && totalWidth + offset > cardsContainerWidth) {
      (document.querySelector('.controls-item-prev a') as HTMLElement).classList.add('d-flex');
      (document.querySelector('.controls-item-next a') as HTMLElement).classList.add('d-flex');
    } else {
      (document.querySelector('.controls-item-prev a') as HTMLElement).classList.remove('d-flex');
      (document.querySelector('.controls-item-next a') as HTMLElement).classList.remove('d-flex');
    }
  };

  return {
    init,
    checkForControls,
  };
});
