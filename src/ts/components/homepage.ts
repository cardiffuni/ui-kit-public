define(['jquery'], ($: JQueryStatic): UIKit.Module.Homepage => {
  const config: UIKit.HomepageBoxOptions = {
    undergraduate: {
      en: {
        collection: 'ug-courses',
        formAction: 'https://www.cardiff.ac.uk/study/undergraduate/courses/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/study/undergraduate/explore-subjects',
          aria: 'Browse undergraduate courses by subject area',
          text: 'Browse by subject area',
        },
        school: {
          url: 'https://www.cardiff.ac.uk/study/undergraduate/courses/browse-by-school',
          aria: 'Browse undergraduate courses by Academic School',
          text: 'Browse by School',
        },
        az: {
          url: 'https://www.cardiff.ac.uk/study/undergraduate/courses/a-to-z',
          aria: 'Browse undergraduate courses by A-Z',
          text: 'Browse by A-Z',
        },
      },
      cy: {
        collection: 'ug-courses',
        formAction: 'https://www.cardiff.ac.uk/cy/study/undergraduate/courses/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/cy/study/undergraduate/explore-subjects',
          aria: 'Pori cyrsiau israddedig yn ôl maes pwnc',
          text: 'Pori yn ôl maes pwnc',
        },
        school: {
          url: 'https://www.cardiff.ac.uk/cy/study/undergraduate/courses/browse-by-school',
          aria: 'Pori cyrsiau israddedig yn ôl Ysgol Academaidd',
          text: 'Pori fesul Ysgol',
        },
        az: {
          url: 'https://www.cardiff.ac.uk/cy/study/undergraduate/courses/a-to-z',
          aria: 'Pori cyrsiau israddedig yn ôl A-Y',
          text: 'Pori fesul A-Y',
        },
      },
    },
    pgr: {
      en: {
        collection: 'pgr-courses',
        formAction: 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-subject',
          aria: 'Browse postgraduate research courses by subject',
          text: 'Browse by subject area',
        },
        school: {
          url: 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school',
          aria: 'Browse postgraduate research courses by Academic School',
          text: 'Browse by School',
        },
        az: {
          url: 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/a-to-z',
          aria: 'Browse postgraduate research courses by A-Z',
          text: 'Browse by A-Z',
        },
      },
      cy: {
        collection: 'pgr-courses',
        formAction: 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/browse-by-subject',
          aria: 'Pori cyrsiau ymchwil ôl-raddedig yn ôl pwnc',
          text: 'Pori yn ôl maes pwnc',
        },
        school: {
          url: 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/browse-by-school',
          aria: 'Pori cyrsiau ymchwil ôl-raddedig yn ôl Ysgol Academaidd',
          text: 'Pori fesul Ysgol',
        },
        az: {
          url: 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/a-to-z',
          aria: 'Pori cyrsiau ymchwil ôl-raddedig yn ôl A-Y',
          text: 'Pori fesul A-Y',
        },
      },
    },
    pgt: {
      en: {
        collection: 'pg-taught-courses',
        formAction: 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses',
        subject: null,
        school: null,
        az: {
          url: 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/',
          aria: 'Browse postgraduate taught courses by A-Z',
          text: 'Browse by A-Z',
        },
      },
      cy: {
        collection: 'pg-taught-courses',
        formAction: 'https://www.cardiff.ac.uk/cy/study/postgraduate/taught/courses',
        subject: null,
        school: null,
        az: {
          url: 'https://www.cardiff.ac.uk/cy/study/postgraduate/taught/courses/',
          aria: 'Pori cyrsiau ôl-raddedig a addysgir yn ôl A-Y',
          text: 'Pori fesul A-Y',
        },
      },
    },
    cpe: {
      en: {
        collection: 'cpe-course',
        formAction: 'https://www.cardiff.ac.uk/part-time-courses-for-adults/courses/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/part-time-courses-for-adults/courses#subjects',
          aria: 'Browse part-time courses for adults by subject',
          text: 'Browse by subject area',
        },
        school: null,
        az: null,
      },
      cy: {
        collection: 'cpe-course',
        formAction: 'https://www.cardiff.ac.uk/cy/part-time-courses-for-adults/courses/search',
        subject: {
          url: 'https://www.cardiff.ac.uk/cy/part-time-courses-for-adults/courses#subjects',
          aria: 'Pori cyrsiau rhan-amser i oedolion yn ôl pwnc',
          text: 'Pori yn ôl maes pwnc',
        },
        school: null,
        az: null,
      },
    },
    cpd: {
      en: {
        collection: 'cpd-courses',
        formAction: 'http://www.cardiff.ac.uk/professional-development/available-training/short-courses/search',
        subject: null,
        school: null,
        az: null,
      },
      cy: {
        collection: 'cpd-courses',
        formAction: 'http://www.cardiff.ac.uk/cy/professional-development/available-training/short-courses/search',
        subject: null,
        school: null,
        az: null,
      },
    },
  };

  const init = (): void => {
    const width = $(window).width() || 0;
    if ($('#hero-video').length && width > 980 && $('.hero').attr('data-video')) {
      addHeroVideo();
    }

    changeCourseFinderForm();
    $('.search-title-options select').on('change', changeCourseFinderForm);
  };

  const changeCourseFinderForm = (): void => {
    let pageLang: UIKit.HomepageBoxContexts = ($('html').attr('lang') as UIKit.HomepageBoxContexts) || 'en';

    /* Get current mode */
    let selectedMode = $('.search-title-options select').val() as UIKit.HomepageCourseMode;

    /* Update the action and collection */
    $('#coursefinder-search').attr('action', config[selectedMode][pageLang].formAction);
    $('#coursefinder-search input[name=collection]').val(config[selectedMode][pageLang].collection);

    /* Update the supplementary links */
    updateSupplementaryLinks(selectedMode);

    /* Toggle promotion section */
    updatePromotionArea(selectedMode);
  };

  /**
   * Re-renders the supplementary links on the homepage search box.
   *
   * @param {UIKit.HomepageCourseMode} mode Re
   */
  const updateSupplementaryLinks = (mode: UIKit.HomepageCourseMode): void => {
    let pageLang: UIKit.HomepageBoxContexts = ($('html').attr('lang') as UIKit.HomepageBoxContexts) || 'en';

    // config[selectedLevel][pageLang].school;
    // let tempBrowseBySubject = config[selectedLevel][pageLang].subject;
    // let tempBrowseByAz = config[selectedLevel][pageLang].az;
    switch (mode) {
      case 'undergraduate':
        renderSupplementaryLinks('Undergraduate', [
          config[mode][pageLang].subject as UIKit.HomepageBoxLink,
          config[mode][pageLang].az as UIKit.HomepageBoxLink,
          config[mode][pageLang].school as UIKit.HomepageBoxLink,
        ]);

        break;
      case 'pgt':
        renderSupplementaryLinks('Postgraduate taught', [config[mode][pageLang].az as UIKit.HomepageBoxLink]);
        break;
      case 'pgr':
        renderSupplementaryLinks('Postgraduate research', [
          config[mode][pageLang].az as UIKit.HomepageBoxLink,
          config[mode][pageLang].school as UIKit.HomepageBoxLink,
          config[mode][pageLang].subject as UIKit.HomepageBoxLink,
        ]);
        break;
      case 'cpe':
        renderSupplementaryLinks('Part-time courses for adults', [config[mode][pageLang].subject as UIKit.HomepageBoxLink]);
        break;
      case 'cpd':
        renderSupplementaryLinks('Professional development');
        break;
      default:
        break;
    }
  };

  /**
   * Re-renders the supplementary links on the homepage search box.
   *
   * @param {UIKit.HomepageCourseMode} mode Re
   */
  const updatePromotionArea = (mode: UIKit.HomepageCourseMode): void => {
    const greenBox = document.querySelector('.search-box .green-bg');

    /* Hide all CTAs */
    $('.search-box .call-to-action').each((_, item): void => {
      item.classList.add('d-none');
      item.classList.remove('d-block');
    });

    switch (mode) {
      case 'undergraduate':
        if (greenBox !== null) greenBox.classList.remove('d-none');

        checkForCTA('undergraduate-call-to-action');

        break;
      case 'pgt':
        if (greenBox !== null) greenBox.classList.remove('d-none');

        checkForCTA('postgraduate-taught-call-to-action');

        break;
      case 'pgr':
        if (greenBox !== null) greenBox.classList.remove('d-none');

        checkForCTA('postgraduate-research-call-to-action');

        break;
      default:
        if (greenBox !== null) greenBox.classList.add('d-none');

        break;
    }
  };

  /**
   * Re-renders the supplementary links based on the links provided.
   *
   * @param {string}  name Name is used for the aria label of the link
   * @param {UIKit.HomepageBoxLink[]} links Array of links to add the the links.
   */
  const renderSupplementaryLinks = (name: string, links?: UIKit.HomepageBoxLink[]): void => {
    if (links !== undefined && links.length > 0) {
      let linkHTML = '';

      links.forEach((item): void => {
        linkHTML += `<li>
          <a href="${item.url}" class="ga-event" aria-label="${item.aria}" data-action="click-link" data-category="Main homepage" data-label="${item.url} - ${name} - Search box">
            ${item.text}
          </a>
        </li>`;
      });

      $('.search-related').addClass('d-block');
      $('.search-related').removeClass('d-none');
      $('.search-related').html(linkHTML);
      $('.search-related').attr('aria-hidden', 'false');
    } else {
      /* Hide the links */
      $('.search-related').addClass('d-none');
      $('.search-related').removeClass('d-block');
      $('.search-related').attr('aria-hidden', 'true');
    }
  };

  const addHeroVideo = (): void => {
    let $hero = $('.hero');
    let sourceString = '<source src="' + $hero.data('video') + '" type="video/mp4">';

    $('#hero-video').append(sourceString);

    let mastheadVideo = document.getElementById('hero-video') as HTMLVideoElement;
    if (mastheadVideo !== null) {
      mastheadVideo.autoplay = true;
      mastheadVideo.load();
    }
  };

  /**
   * Checks whether the given course mode has a associated CTa section.
   *
   * If there is a CTA section, show it and the green background.
   * @param {string} id  ID of the CTA container
   */
  const checkForCTA = (id: string): void => {
    const CTA = document.getElementById(id);
    const greenBox = document.querySelector('.search-box .green-bg');

    if (CTA === null) {
      if (greenBox !== null) greenBox.classList.add('d-none');
    } else {
      CTA.classList.remove('d-none');
      CTA.classList.add('d-block');
    }
  };
  return {
    init,
    changeCourseFinderForm,
    addHeroVideo,
  };
});
