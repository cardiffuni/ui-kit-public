define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): any => {
  class Navigation {
    public pageAssetid: string;
    public storage: UIKit.Navigation.Store.Store;
    public storageName: string;
    public endpoint: string;
    public currentURL: string;
    public status: UIKit.Navigation.Status;
    public transitionDuration: number;

    public context: string;
    public constructor() {
      /* Set the page URL and session name */
      this.currentURL = window.location.href;
      this.storageName = 'cupn';
      this.transitionDuration = 350;
      this.pageAssetid = $('body').data('assetid') || undefined;
      this.context = $('html').attr('lang') || 'en';

      if (this.context === 'cy') {
        this.endpoint = '/cy/feeds/navigation?id=';
      } else {
        this.endpoint = '/feeds/navigation?id=';
      }

      /* Set menu status */
      this.status = {
        session: {
          refresh: this.currentURL.search(/\/_recache/) !== -1,
          disable: this.currentURL.search(/\/_nocache/) !== -1,
        },
        transition: {
          menu: false,
        },
        direction: {
          back: false,
        },
        state: {
          menuOpen: false,
          waitingResponse: false,
          finishedProcessing: false,
        },
      };

      /* Reset the session  if /_recache in url */
      if (this.status.session.refresh) {
        window.sessionStorage.removeItem(this.storageName);
        this.storage = {};
      } else {
        this.storage = JSON.parse(window.sessionStorage.getItem(this.storageName) || '{}');
      }

      /* Ensure there is always an empty context */
      if (this.storage[this.context] === undefined) {
        this.storage[this.context] = {};
      }

      /* Initalise all event handers */
      this.init();

      /* Fetch navigation items for current page */
      this.checkDeviceWidth();
    }

    /**
     * Initaliser for the navigation.
     * Add event handler for .nav-back, nav-link-expand, and the menu open button.
     *
     * @param {HTMLElement} param1 HTMLElement to modify
     * @return Returns a custom string
     */
    public init = (): void => {
      $(document).on('click', '.nav-global-mobile .nav-link-expand, .nav-back > .nav-link', (evt: JQuery.ClickEvent): void => {
        const item: JQuery<HTMLElement> = $(evt.target);
        const assetid: string | undefined = item.attr('data-assetid');

        /* Only move to next screen when a valid assetid is provided */
        if (assetid !== undefined && assetid.length > 0) {
          evt.preventDefault();

          /* Set transition direction */
          this.status.direction.back = item.hasClass('nav-link');

          this.get(assetid);
        }
      });

      /* Sets an event handler for when burger item is clicked */
      $(document).on('click', '#openNavigation', (evt: JQuery.ClickEvent): void => {
        evt.preventDefault();

        this.toggleMenu();
      });

      /* Sets an event handler for when burger item is clicked */
      $(document).on('click', '.nav-local .nav-link-expand', (evt: JQuery.ClickEvent): void => {
        const item: JQuery<HTMLElement> = $(evt.target);
        const assetid: string | undefined = item.attr('data-assetid');

        /* Only move to next screen when a valid assetid is provided */
        if (assetid !== undefined && assetid.length > 0) {
          evt.preventDefault();

          this.getLocal(item, assetid);
        }
      });

      $(document).on('click', (evt: JQuery.ClickEvent): void => {
        /**
         * When there is an click on the document, check if the click is within the nav-global-links.
         * If click is outside of the area, force the mega menu to close
         */
        if (!document.querySelector('.nav-global-links')?.contains(evt.target)) {
          this.closeDesktopNavigationOverlay();
        }
      });

      /**
       * When there is enter or numpad enter is typed, Close any active menus (fail safe) and open the mega menu
       * When there is a Esc is entered, close desktop dropdown
       */
      $(document).on('keydown', '.nav-global-links > .nav-item > .nav-link', (evt: JQuery.KeyDownEvent): boolean => {
        const navItem = $(evt.target);

        switch (evt.key) {
          case 'NumpadEnter':
          case 'Enter':
            if (!navItem.parent('.nav-item').hasClass('open')) {
              $('.nav-global-links > .nav-item').removeClass('open');
              navItem.parent('.nav-item').addClass('open');
              return false;
            }
            break;
          default:
            return true;
        }
        return true;
      });

      /**
       * When focus on a nav item OR a context link, close any the dropdown
       */
      $(document).on('focus', '.nav-global-links > .nav-item > .nav-link, .nav-global-context > a', (evt: JQuery.FocusEvent): void => {
        this.closeDesktopNavigationOverlay();
      });

      /* When esc is pressed, check if there is a dropdown open. If so, close the active dropdown */
      $(document).on('keydown', (evt: JQuery.KeyDownEvent): void => {
        if ($('.nav-global-links > .nav-item.open').length !== 0) {
          switch (evt.key) {
            case 'Esc':
            case 'Escape':
              $('.nav-global-links > .nav-item.open').find('> .nav-link').trigger('focus');
              this.closeDesktopNavigationOverlay();

              break;
          }
        }

        return;
      });
    };

    /** Closes the active dropdown */
    public closeDesktopNavigationOverlay = (): void => {
      $('.nav-global-links > .nav-item.open').removeClass('open');
    };

    /**
     * Calls the request when it is not within session.
     * Then calls the render function.
     *
     * @param {string} assetid The assetid of the page to request pages for.
     * @see request
     * @see renderNavigation
     * @returns void
     */
    public get = (assetid: string): void => {
      if (this.storage[this.context][assetid] === undefined || this.status.session.disable) {
        /* Reset processing flag */
        this.status.state.finishedProcessing = false;

        /* Begin transitioning */
        this.transition('').then((): void => {
          if (!this.status.state.finishedProcessing) {
            this.updateTemplate(
              `<svg class="icon loading" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><g transform="translate(1 1)" stroke-width="2"><circle class="loading-circle" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></g></svg>`,
              true
            );
          }
        });

        /* Create the API request */
        this.request(assetid).then((json: UIKit.Navigation.Store.Item): void => {
          this.storage[this.context][assetid] = json;

          /* Update the browser session storage*/
          this.updateSession();

          /* Render the new navigation items */
          this.renderNavigation(assetid);
        });
      } else {
        /* When item is in session */
        this.transition('');

        /* Render the new navigation items */
        this.renderNavigation(assetid);
      }
    };

    public getLocal = (element: JQuery<HTMLElement>, assetid: string): void => {
      if (this.storage[this.context][assetid] === undefined || this.status.session.disable) {
        /* Reset processing flag */
        this.status.state.finishedProcessing = false;

        /* Begin transitioning */
        this.updateLocalIcon(
          element,
          `<svg class="icon loading" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><g transform="translate(1 1)" stroke-width="2"><circle class="loading-circle" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></g></svg>`
        );

        /* Create the API request */
        this.request(assetid).then((json: UIKit.Navigation.Store.Item): void => {
          this.storage[this.context][assetid] = json;

          /* Update the browser session storage*/
          this.updateSession();

          /* Render the new navigation items */
          this.renderLocal(element, assetid);
        });
      } else {
        /* When item is in session */
        this.transition('');

        /* Render the new navigation items */
        this.renderLocal(element, assetid);
      }
    };

    /**
     * Updated the browser's session with the new latest storage.
     * Promise is used for async.
     * @see storage
     *
     * @returns Promise<void>
     */
    public updateSession = (): Promise<void> =>
      new Promise((): void => {
        window.sessionStorage.setItem(this.storageName, JSON.stringify(this.storage));
      });

    /**
     * Renders the navigation
     *
     * @param  {string} assetid
     * @returns void
     */
    public renderNavigation = (assetid: string): void => {
      const grandParent: UIKit.Navigation.Store.Page = this.storage[this.context][assetid].lineage[
        this.storage[this.context][assetid].lineage.length - 2
      ];

      /* Empty string builder. */
      let template = ``;

      /* Add 'back to' item */
      if (grandParent !== undefined) {
        template += `
          <li class="nav-item nav-back">
            <a href="${grandParent.url}" data-assetid="${grandParent.assetid}" class="nav-link">
              <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32">
                <title>Chevron left</title>
                <path d="M25.88,28.28L13.59,16,25.88,3.72,22.16,0l-16,16,16,16Z" transform="translate(-6.13 0)"></path>
              </svg>
              <span id="backToSectionText">${i18n.navigation.back} ${grandParent.name}</span>
            </a>
          </li>`;
      }

      /* Add parent and children items */
      if (this.storage[this.context][assetid].lineage[this.storage[this.context][assetid].lineage.length - 1] !== undefined) {
        const parent: UIKit.Navigation.Store.Page = this.storage[this.context][assetid].lineage[
          this.storage[this.context][assetid].lineage.length - 1
        ];

        const parentName = parent.showEditingBrackets ? `(( ${parent.name} ))` : parent.name;

        /* Add parent item */
        template += `
          <li class="nav-item nav-global-mobile-section">
            <a href="${parent.url}" data-assetid="${parent.assetid}" class="nav-link active">${parentName}</a>`;

        if (this.storage[this.context][assetid].children.length > 0) {
          template += `<nav class="nav-global-mobile-section">`;

          /* Loop over every child page */
          this.storage[this.context][assetid].children.forEach((child: UIKit.Navigation.Store.Page): void => {
            template += this.navigationPartial(child);
          });

          template += `</nav>`;
        }
        template += `</li>`;
      }

      /* Add sibling sections, only used for the homepage */
      if (this.storage[this.context][assetid].siblings.length !== 0 && this.storage[this.context][assetid].children.length === 0) {
        this.storage[this.context][assetid].siblings.forEach((sibling: UIKit.Navigation.Store.Page): void => {
          template += this.navigationPartial(sibling);
        });
      }

      this.status.state.finishedProcessing = true;
      this.updateTemplate(template, false);
    };

    public renderLocal = (element: JQuery<HTMLElement>, assetid: string): void => {
      const depth = this.storage[this.context][assetid].lineage.length <= 5 ? this.storage[this.context][assetid].lineage.length : 5;

      let template = `<nav style="display: none; padding-left: ${depth * 20}px">`;

      /* Add sibling sections, only used for the homepage */
      if (this.storage[this.context][assetid].siblings.length !== 0 && this.storage[this.context][assetid].children.length === 0) {
        this.storage[this.context][assetid].siblings.forEach((sibling: UIKit.Navigation.Store.Page): void => {
          template += this.navigationPartial(sibling);
        });
      }

      template += '</nav>';
      this.status.state.finishedProcessing = true;
      element.append(template);
      element.find('.icon').parent().collapse('show');
    };

    /**
     * Calls the endpoint with the requested assetid
     * @param  {string} assetid Current assetid of the page.
     * @see endpoint
     * @returns Promise<UIKit.Navigation.Store.Item>
     */
    public request = (assetid: string): Promise<UIKit.Navigation.Store.Item> => {
      return new Promise((resolve): void => {
        $.get(`${this.endpoint}${assetid}`).done((data: UIKit.Navigation.Store.Item): void => {
          resolve(data);
        });
      });
    };

    /**
     * Toggles the mobile menu when the menu is not currently transition.
     */
    public toggleMenu = (): void => {
      /* Only first event when menu is not still transitioning */
      if (!this.status.transition.menu) {
        if (this.status.state.menuOpen) {
          /* Close mobile menu */
          this.status.transition.menu = true;

          $('#openNavigationText').text(i18n.navigation.menu);

          $('#openNavigation').removeClass('menu-open');

          /* Transition menu into view */
          this.transitionTimeout('menu-closing').then((): void => {
            /* Remove transitioning states */
            $('body').removeClass('menu-open');
            $('body').removeClass('menu-closing');
            $('.nav-global-mobile').removeClass('visible');
            $('.nav-global-mobile').removeClass('d-block');
            $('body').css('top', '');

            this.status.transition.menu = false;
            this.status.state.menuOpen = false;

            /* Reset navigation to the page user is currently on and check that this current page JSON has been fetched */
            if (this.pageAssetid !== undefined && this.storage[this.context][this.pageAssetid] !== undefined) {
              this.renderNavigation(this.pageAssetid);
            }
          });
        } else {
          /* open mobile menu */

          /* get current page */
          this.fetchCurrentPage();

          /* Get the offset to the top */
          this.status.transition.menu = true;

          $('.nav-global-mobile').addClass('d-block');
          $('.nav-global-mobile').addClass('visible');
          $('body').css('top', `-${window.scrollY}px`);
          $('#openNavigation').addClass('menu-open');

          /* Allow element to be display block to be before transitioning */
          setTimeout((): void => {
            $('#openNavigationText').text(i18n.navigation.close);

            this.transitionTimeout('menu-open').then((): void => {
              this.status.transition.menu = false;
            });

            this.status.state.menuOpen = true;
          }, 10);
        }
      }
    };

    /**
     * Adds a class to the body and sets a delay.
     *
     * @param {string} class name tobe added
     * @returns Promise
     */
    public transitionTimeout = (className: string): Promise<boolean> => {
      return new Promise((resolve): void => {
        $('body').addClass(className);

        setTimeout((): void => {
          resolve(true);
        }, this.transitionDuration);
      });
    };

    public navigationPartial = (item: UIKit.Navigation.Store.Page): string => {
      const name = item.showEditingBrackets ? `(( ${item.name} ))` : item.name;

      if (item.has_children) {
        return `
          <li class="nav-item nav-item-expandable">
            <a href="${item.url}" class="nav-link">${name}</a>
            <a href="#" class="nav-link-expand" data-assetid="${item.assetid}">
              <svg class="icon" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title>Expand</title>
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <g transform="translate(-727.000000, -388.000000)" fill="#383735">
                    <path d="M736.75,388 L736.75,395.25 L744,395.25 L744,397.75 L736.75,397.75 L736.75,405 L734.25,405 L734.25,397.749 L727,397.75 L727,395.25 L734.25,395.249 L734.25,388 L736.75,388 Z"></path>
                  </g>
                </g>
              </svg>
            </a>
          </li>`;
      }
      return `
        <li class="nav-item">
          <a href="${item.url}" class="nav-link ${item.active ? 'active' : ''}">${name}</a>
        </li>`;
    };

    public transition = (template: string = ''): Promise<boolean> => {
      this.status.state.waitingResponse = true;

      return new Promise((resolve): void => {
        $('.nav-global-mobile').append(`
          <div class="nav-global-mobile-next ${this.status.direction.back ? 'back' : ''}">
            ${template}
          </div>`);
        $('#mobileNavigationItems').addClass('nav-global-mobile-previous');

        if (this.status.direction.back) {
          $('#mobileNavigationItems').addClass('back');
        }

        /* Allow for display block for transitioning */
        setTimeout((): void => {
          $('.nav-global-mobile-next, .nav-global-mobile-previous').addClass('transitioning');

          setTimeout((): void => {
            $('#mobileNavigationItems').remove();
            $('.nav-global-mobile-next').attr('id', 'mobileNavigationItems');
            $('#mobileNavigationItems').removeClass('nav-global-mobile-next');

            this.status.state.waitingResponse = false;
            $('#mobileNavigationItems').removeClass('transitioning');
            $('#mobileNavigationItems').removeClass('back');
            resolve(true);
          }, this.transitionDuration);
        }, 10);
      });
    };

    /**
     * Updates the mobile menu
     * @param {string} template HTML string of the template to update
     * @param {boolean} withLoading Adds a class when it's a loading template
     */
    public updateTemplate = (template: string, withLoading: boolean): void => {
      if (this.status.state.waitingResponse) {
        $('.nav-global-mobile-next').html(template);
      } else {
        $('#mobileNavigationItems').html(template);
      }

      if (withLoading) {
        $('#mobileNavigationItems').addClass('loading');
      } else {
        $('#mobileNavigationItems').removeClass('loading');
      }
    };

    /**
     *  Update local icon of local navigation
     * @param {JQuery<HTMLElement>} item Element to update the SVG ofnavigation
     * @param {string} icon SVG string of the icon to update with
     */
    public updateLocalIcon = (item: JQuery<HTMLElement>, icon: string): void => {
      item.find('icon').html(icon);
    };

    /**
     * Creates a background request to get the current page navigation and
     * update the session.
     */
    public fetchCurrentPage = (): void => {
      if (this.pageAssetid !== undefined && (this.storage[this.context][this.pageAssetid] === undefined || this.status.session.disable)) {
        this.request(this.pageAssetid).then((json: UIKit.Navigation.Store.Item): void => {
          this.storage[this.context][this.pageAssetid] = json;

          this.updateSession();
        });
      }
    };
    /**
     * Checks whether the menu is open on desktop
     *
     * Fixes issues on table when  changes.
     */
    public checkDeviceWidth = (): void => {
      $(window).on('resize', (): void => {
        if (this.status.state.menuOpen) {
          const width: number | undefined = $(window).width();

          /* Check if it's desktop view */
          if (width !== undefined && width >= 980) {
            this.toggleMenu();
          }
        }
      });
    };
  }

  return {
    Navigation,
  };
});
