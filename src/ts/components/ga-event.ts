define(['jquery'], ($: JQueryStatic): UIKit.Module.GAEvent => {
  const init = (): void => {
    $('.ga-event').each(function (): void {
      $(this).on('click', function (): void {
        let linkCategory = $(this).data('category');
        let linkAction = $(this).data('action');
        let linkLabel = $(this).data('label');

        if (!linkCategory) {
          linkCategory = 'Missing category';
        }
        if (!linkAction) {
          linkAction = 'click';
        }
        if (!linkLabel) {
          linkLabel = 'Missing label';
        }

        if (typeof ga === 'function') {
          ga('send', 'event', linkCategory, linkAction, linkLabel);
        }
      });
    });
  };
  return {
    init,
  };
});
