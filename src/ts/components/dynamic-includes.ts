define(['components/lazy-loader.min'], (lazyLoader: UIKit.Module.LazyLoader): UIKit.Module.DynamicIncludes => {
  /**
   * The initialiser for dynamic includes
   */
  const init = (): void => {
    document.querySelectorAll('.dynamic-include').forEach((item): void => {
      const onSelector = item.getAttribute('data-on');

      if (onSelector !== null) {
        const target = document.querySelector<HTMLElement>(onSelector);

        if (target !== null) {
          target.addEventListener('click', (): void => {
            /* States */
            const isLoading = item.getAttribute('data-loading');
            const isLoaded = item.getAttribute('data-loaded');

            if ((isLoaded === 'false' || isLoaded === null) && (isLoading === 'false' || isLoading === null)) {
              item.setAttribute('aria-busy', 'true');
              item.setAttribute('data-loading', 'true');
              target.classList.add('btn--loading');

              loadContent(item as HTMLElement, target);
            }
          });
        }
      } else {
        /* Load content immediately */
        loadContent(item as HTMLElement);
      }
    });
  };

  /**
   * Replaces the inner HTML of item based on the request body
   * @param {HTMLElement} item HTMLElement to inject the HTML
   */
  const loadContent = (item: HTMLElement, target?: HTMLElement): void => {
    const url = item.getAttribute('data-url');

    if (url !== null) {
      const request = new XMLHttpRequest();

      request.onreadystatechange = function (): void {
        if (request.readyState === 4 && request.status === 200) {
          if (target !== undefined) {
            target.classList.remove('btn--loading');
          }

          /* Set attributes */
          item.setAttribute('data-loading', 'false');
          item.setAttribute('data-loaded', 'true');
          item.setAttribute('aria-busy', 'false');

          /* Set the HTML */
          item.innerHTML = request.responseText;

          /* Re-initialise lazy loader */
          lazyLoader.init();
        } else if (request.readyState === 4) {
          /* Request has finished, however has failed (Not a 200 status code) */
          const errorMessage: HTMLElement = document.createElement('p');

          errorMessage.innerHTML = item.dataset.errorMessage || '';
          errorMessage.classList.add('alert');
          item.outerHTML = errorMessage.outerHTML;
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }
  };

  return {
    init,
  };
});
