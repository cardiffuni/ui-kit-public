define(['jquery', 'observer'], ($: JQueryStatic, intersect: any): UIKit.Module.StickyActivator => {
  const init = (): void => {
    var IntersectionObserver = intersect;

    $('.sticky-top').before('<div class="sticky-top-before"></div>');
    const handlers = $('.sticky-top-before');

    if (handlers !== null) {
      handlers.each((_, handler): void => {
        observer.observe(handler);
      });
    }
  };

  const observer = new IntersectionObserver(
    (entries): void => {
      for (let i = 0; i < entries.length; i++) {
        if (entries[i].intersectionRatio === 0) {
          /* Add border-bottom */
          $(entries[i].target).next('.sticky-top').addClass('stuck');
        } else if (entries[i].intersectionRatio === 1) {
          $(entries[i].target).next('.sticky-top').removeClass('stuck');
        }
      }
    },
    { threshold: [0, 1] }
  );

  return {
    init,
  };
});
