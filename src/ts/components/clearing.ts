define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.Clearing => {
  /**
   * Returns the clearing template
   * @param title The title to display
   * @param bodyText the body html to display
   * @param linkURL The URL to display
   * @param linkText The link text to display
   */
  const template = (title: string, bodyText: string, linkURL: URL, linkText: string): string => {
    return `<div class="alert">
      <h4 class="alert-title">${title}</h4>
      <div class="alert-body">${bodyText} <a href="${linkURL.toString()}">${linkText}</a></div> 
    </div>`;
  };

  /**
   * The initialiser for clearing
   */
  const init = (): void => {
    const referrer: string = document.referrer;
    const isFromClearing: boolean = referrer.indexOf('/clearing') !== -1;
    const isClearingPage: boolean = window.location.href.indexOf('/clearing') !== -1;

    if (isFromClearing && !isClearingPage) {
      const tmpl: string = template(i18n.clearing.title, i18n.clearing.bodyText, new URL(referrer), i18n.clearing.linkText);
      $('.radio-selection').next().after(tmpl);
    }
  };

  return {
    init,
    template,
  };
});
