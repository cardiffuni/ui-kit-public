define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.TableCollapse => {
  const init = (): void => {
    const tables = $('.table-collapse');

    tables.each((_, item): void => {
      const table = $(item);
      const chartWrapper = table.closest('.chart-table');

      if (chartWrapper.length === 0 || (chartWrapper.length > 0 && table.attr('data-loaded') === 'true')) {
        processTable(table);
      } else {
        const chartID = chartWrapper.attr('data-assetid');

        /* Wait until chart.ts has finished processing the table */
        document.addEventListener(`highchart-table-created-${chartID}`, ((evt: CustomEvent): void => {
          processTable(table);
        }) as EventListener);
      }
    });
  };

  /**
   * Process a table to include the button and the event listeners
   * @param table JQuery table to attach the button to.
   */
  const processTable = (table: JQuery<HTMLElement>): void => {
    const showLast = table.hasClass('hide-top-rows');

    /* you can set data-limit in the html else it will be 10 */
    const limit = table.attr('data-limit') || 10;

    const rows = table.find('tbody tr');
    const totalRows = rows.length;

    const numOfColumns = table.find('tbody tr:first-child td').length;

    /* This is where it hides the amount of rows decided in limit */
    rows.each((index, item): void => {
      if (showLast) {
        // Shows last rows
        if (totalRows - index <= limit) return;
        $(item).addClass('d-none');
      } else {
        // Shows first rows
        if (index < limit) return;
        $(item).addClass('d-none');
      }
    });

    /* Creates the button that shows the hidden rows */
    if (totalRows > limit) {
      if (showLast) {
        /* Button at the start of the table */
        table
          .find('tbody')
          .prepend(
            `<tr data-skip-row="true"><td colspan="${numOfColumns}"><button class="btn btn-outline btn-block mb-0 show-all-rows">${i18n.collapseTables.previous}</button></td></tr>`
          );
      } else {
        /* Button at the bottom of the table */
        table
          .find('tbody')
          .append(
            `<tr data-skip-row="true"><td colspan="${numOfColumns}"><button class="btn btn-outline btn-block mb-0 show-all-rows">${i18n.collapseTables.all}</button></td></tr>`
          );
      }
    }

    $('.show-all-rows').on('click', (evt): void => {
      evt.preventDefault();
      $(evt.target).closest('tbody').find('.d-none').removeClass('d-none');
      $(evt.target).parent().parent().remove();
    });
  };
  return {
    init,
  };
});
