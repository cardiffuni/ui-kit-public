define([], (): UIKit.Module.Dropdown => {
  const init = (): void => {
    dropdownToggle();
    dropdownNavSection();
  };

  const dropdown = (el: HTMLElement): void => {
    // finds dropdowns
    if (el.querySelector('a') !== null) {
      const anchor = el.querySelector('a');
      if (anchor !== null) {
        anchor.addEventListener('click', function (this: HTMLAnchorElement, evt: MouseEvent): boolean {
          // targets open class name
          if (this.parentElement !== null) {
            if (this.parentElement.className.indexOf('open') > -1) {
              // adds class && aria-expanded
              this.parentElement.classList.remove('open');
              this.setAttribute('aria-expanded', 'false');
            } else {
              // resets
              this.parentElement.classList.add('open');
              this.setAttribute('aria-expanded', 'true');
            }
          }

          // prevents going to link
          evt.preventDefault();
          return false;
        });
      }
    }
  };
  const dropdownButton = (el: HTMLElement): void => {
    let activatingA = el.querySelector('a');
    let btn = '<button class="btn-dropdown dropdown-toggle"></button>';

    if (activatingA !== null) {
      activatingA.insertAdjacentHTML('afterend', btn);
    }

    const elBtn = el.querySelector('button');

    if (elBtn !== null) {
      elBtn.addEventListener('click', function (evt: MouseEvent): void {
        if (this.parentElement !== null) {
          const anchor = this.parentElement.querySelector('a');
          const button = this.parentElement.querySelector('button');

          if (this.parentElement.className.indexOf('open') > -1) {
            this.parentElement.classList.remove('open');

            if (anchor !== null) {
              anchor.setAttribute('aria-expanded', 'false');
            }
            if (button !== null) {
              button.setAttribute('aria-expanded', 'false');
            }
          } else {
            this.parentElement.classList.add('open');

            if (anchor !== null) {
              anchor.setAttribute('aria-expanded', 'true');
              anchor.setAttribute('aria-haspopup', 'true');
            }
            if (button !== null) {
              button.setAttribute('aria-expanded', 'true');
            }
          }
        }
        evt.preventDefault();
      });
    }
  };

  const dropdownToggle = (): void => {
    let menuItems = document.querySelectorAll('li.dropdown-more');
    Array.prototype.forEach.call(menuItems, (el): void => {
      if (el.className.indexOf('dropdown-more') > -1) {
        // if dropdown-more exists  don't add button
        dropdown(el);
      }
    });
  };

  const dropdownNavSection = (): void => {
    let navSection = document.querySelectorAll('.nav-section li.dropdown');

    Array.prototype.forEach.call(navSection, (el): void => {
      let navChildren = el.childNodes;
      Array.prototype.forEach.call(navChildren, (el): void => {
        if (el.className === 'dropdown-menu d-none d-md-block') {
          dropdownButton(el.parentElement);
        }
      });
    });
  };
  return {
    init,
    dropdown,
    dropdownButton,
    dropdownToggle,
    dropdownNavSection,
  };
});
