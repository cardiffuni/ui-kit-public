define(['jquery'], ($: JQueryStatic): UIKit.Module.Facet => {
  const init = (): void => {
    checkDirty();
    $('.facet-popup').appendTo('body'); //Move the facet popups to the bottom of the body tag. Unable to render them at the bottom with HTML. Need to resovle this. DH

    $(document).on('keydown', '.facet .daterange input', function (e): void {
      dateRangeInputFieldFilter(e);
    });

    $(document).on('keyup', '.facet .daterange input', function (e): void {
      moveToNextInput(e, $(this));
    });

    $(document).on('click', '.facet-show-more', function (e): void {
      e.preventDefault();
      let facetID = $(this).data('facet');
      $(`#${facetID} .facet-items .d-none`).removeClass('d-none');
      $(this).remove();
    });

    $(document).on('click', '.facet-group-title', function (e): void {
      e.preventDefault();
      $('#facet-menu-popup').addClass('d-none');
      $('#' + $(this).attr('data-facet')).removeClass('d-none');
    });

    $(document).on('click', '#launch-facet-menu-popup', function (e): void {
      e.preventDefault();
      $('#facet-menu-popup').removeClass('d-none');
      $('body').children().not('script, noscript, .facet-popup, #facet-menu-popup').addClass('d-none');
    });

    $(document).on('click', '.facet-title .icon', function (e): void {
      e.preventDefault();
      $('#facet-menu-popup').addClass('d-none');
      $('body').children().not('script, noscript, .facet-popup, #facet-menu-popup').removeClass('d-none');
    });

    $(document).on('click', '.facet-action-cancel', function (e): void {
      e.preventDefault();
      $('.facet-popup').addClass('d-none');
      $('#facet-menu-popup').removeClass('d-none');
    });

    /* new */
    $(document).on('shown.bs.collapse', '.facet .collapse', function (this: HTMLElement): void {
      $(this)
        .siblings('.facet-heading')
        .find('.icon')
        .replaceWith(
          '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 19.75"><title>Chevron down</title><path d="M28.28,6.13L16,18.41,3.72,6.13,0,9.84l16,16,16-16Z" transform="translate(0 -6.13)"></path></svg>'
        );
    });

    $(document).on('hidden.bs.collapse', '.facet .collapse', function (this: HTMLElement): void {
      $(this)
        .siblings('.facet-heading')
        .find('.icon')
        .replaceWith(
          '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 19.75"><title>Chevron up</title><path d="M28.28,25.88L16,13.59,3.72,25.88,0,22.16l16-16,16,16Z" transform="translate(0 -6.13)" /></svg>'
        );
    });

    $(document).on('click', '#clear-facet-inputs', (): void => {
      clearFacets();
    });

    /* When an input is changed, check if the form is dirty */
    $(document).on('change', '#search-result-filter input, .facet input', (): void => {
      checkDirty();
    });
  };

  /**
   * Clears all facet inputs and modal facet inputs
   */
  const checkDirty = (): void => {
    let dirty = false;

    /* clear modal input and facets */
    $('#search-result-filter input, .facet input').each(function (this: HTMLElement): void {
      const element = this as HTMLInputElement;
      switch (element.type) {
        case 'checkbox':
          if (element.checked === true) {
            dirty = true;
            return;
          }
          break;
        case 'radio':
          if (element.checked === true) {
            dirty = true;
            return;
          }
          break;
        default:
          if (element.value.length > 0) {
            dirty = true;
            return;
          }
          break;
      }
    });

    if (dirty) {
      $('#clear-facet-inputs').removeClass('disabled');
      $('#clear-facet-inputs').removeAttr('disabled');
      $('#clear-facet-inputs').attr('aria-hidden', 'false');
    } else {
      disableClear();
    }
  };

  /**
   * Clears all facet inputs and modal facet inputs
   */
  const clearFacets = (): void => {
    /* clear modal input and facets */
    $('#search-result-filter input, .facet input').each(function (this: HTMLElement): void {
      const element = this as HTMLInputElement;
      switch (element.type) {
        case 'hidden':
          break;
        case 'checkbox':
          element.checked = false;
          break;
        case 'radio':
          element.checked = false;
          break;
        default:
          element.value = '';
          break;
      }
    });
    disableClear();
  };

  const moveToNextInput = (e: JQuery.KeyUpEvent, $this: JQuery<HTMLInputElement>): void => {
    // Check if the input box is empty and if the keypress was: delete or backspace.
    const value = $this.val() as string;
    if (value !== undefined) {
      if (value.length === 0 && (e.keyCode == 46 || e.keyCode == 8)) {
        //Jump to the previous input box
        $($this).prevAll('.date').eq(0).focus();
      } else if (
        //Check if the input box is full
        value.length === ($($this).attr('maxlength') || 150) &&
        //Ensure the input is a number
        ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))
      ) {
        //Do not use JS to swap input boxes if the keypress is a tab. Default behaviour will do this.
        if (e.keyCode != 9) {
          //Jump to the next input
          $($this).nextAll('.date').eq(0).focus();
        }
      }
    }
  };

  const dateRangeInputFieldFilter = (e: JQuery.KeyDownEvent): void => {
    // Function modified from: https://stackoverflow.com/questions/469357/html-text-input-allows-only-numeric-input
    // Allow: delete, backspace, escape, enter, and tab.
    if (
      $.inArray(e.keyCode, [46, 8, 27, 13, 9]) !== -1 ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  };

  const disableClear = (): void => {
    $('#clear-facet-inputs').addClass('disabled');
    $('#clear-facet-inputs').attr('disabled', 'disabled');
    $('#clear-facet-inputs').attr('aria-hidden', 'true');
  };

  return {
    init,
    moveToNextInput,
    dateRangeInputFieldFilter,
    checkDirty,
    disableClear,
    clearFacets,
  };
});
