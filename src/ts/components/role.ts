define(['jquery'], ($: JQueryStatic): UIKit.Module.Role => {
  const init = (): void => {
    // If we have any elements with the data-get-role attribute
    if ($('[data-get-role]').length) {
      // loop through all
      $('[data-get-role]').each(function (): void {
        // grab the role
        let role = $(this).data('get-role');
        // if not blank
        if (role !== '') {
          // set up a let for the thing we are going to move
          let $element = $(this);
          // if parent element is a bodycopy, move that
          // if current element is a bodycopy, move that
          // if neither, move current element
          if ($(this).parent('.squiz-bodycopy').length) {
            $element = $(this).parent('.squiz-bodycopy');
          } else if ($(this).hasClass('squiz-bodycopy')) {
            $element = $(this);
          } else {
            $element = $(this);
          }
          // Add the thing we want to move to the correct part of the page
          $element.appendTo('[role="' + role + '"]');
        }
      });
    }
  };

  return {
    init,
  };
});
