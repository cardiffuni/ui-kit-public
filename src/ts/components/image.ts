define(['jquery'], ($: JQueryStatic): UIKit.Module.Image => {
  /**
   * Stops the hero video on the page
   */
  const start = (image: JQuery<HTMLElement>): void => {
    let target: HTMLElement = image[0];

    $(target).addClass('video-mode');
    setTimeout((): void => {
      const video: HTMLIFrameElement | null = target.querySelector('.video');

      if (video !== null && video.contentWindow !== null) {
        video.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
      }
    }, 350);
  };

  /**
   * Stops the hero video on the page
   */
  const stop = (image: JQuery<HTMLElement>): void => {
    let target: HTMLElement = image[0];
    const video: HTMLIFrameElement | null = target.querySelector('.video');

    if (video !== null && video.contentWindow !== null) {
      video.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    }

    $(target).removeClass('video-mode');
  };

  /**
   * Initalises all accordions.
   *
   * @requires bootstrap/collapse
   */
  const init = (): void => {
    $(document).on('click', '.image.with-video:not(.video-mode) .video-overlay', (evt: JQuery.ClickEvent): void => {
      evt.preventDefault();
      start($(evt.target).parent().parent());
    });

    $(document).on('click', '.image.with-video.video-mode a.video-overlay-close', (evt: JQuery.ClickEvent): void => {
      evt.preventDefault();
      stop($(evt.currentTarget).parent().parent().parent());
    });

    $(document).on('keyup', (evt: JQuery.KeyUpEvent): void => {
      if (evt.key === 'Escape' && $('.image.with-video.video-mode') !== null) {
        stop($('.image.with-video.video-mode'));
      }
    });
  };

  return {
    init,
    start,
    stop,
  };
});
