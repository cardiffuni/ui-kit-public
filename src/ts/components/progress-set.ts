define(['jquery'], ($: JQueryStatic): UIKit.Module.ProgressSet => {
  const init = (): void => {
    $(document).ready(function (): void {
      $('.progress-set').each(function (): void {
        let showDropdown = 0;
        $($(this).children('.progress-group')).each(function (indexJ): void {
          $($(this).parent().find('.progress-filter')).append(
            $('<option>', {
              value: indexJ + 1,
              text: $(this).children('h3').text(),
            })
          );
          if (indexJ > 0) {
            $(this).hide();
            if (showDropdown == 0) {
              showDropdown = 1;
            }
          }
        });
        if (showDropdown == 1) {
          $(this).children('.progress-group').children('h3').hide();
          $(this).children('select').show();
        }
      });

      $('.progress-filter').on('change', function (this: HTMLInputElement): void {
        $(this).parents('.progress-set').find('.progress-group').hide();
        $(this).parents('.progress-set').find(`.progress-group[data-year=${this.value}]`).show();
      });
    });
  };
  return {
    init,
  };
});
