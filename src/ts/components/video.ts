define(['jquery', 'components/cookie-policy', 'i18n!locale/globals.min', 'youtube', 'helpers/accessibility.min'], (
  $: JQueryStatic,
  cookiePolicy: UIKit.Module.CookiePolicy,
  i18n: UIKit.I18n,
  _: any,
  accessibility: UIKit.Helpers.Accessibility
): UIKit.Module.Video => {
  const videoReferences: UIKit.Video.VideoReferences = {};
  const transitionDuration = 350;
  const transitionContainerHeightDuration = 150;
  const videoBottomMargin = 20;

  /** 16:9 aspect ratio calcutation */
  const ratio16by9 = 0.5625;
  let totalContainers = 0;
  let cookiePreferences = cookiePolicy.getCookiePreferences();
  let contentUnavilable: UIKit.Module.ContentUnavailable;

  /** Whether the event is beng throttled */
  let throttled = false;
  const delay = 250;

  let initalised = false;

  const handleCookiePrefernceChange = (evt: CustomEvent): void => {
    cookiePreferences = evt.detail as UIKit.CookiePreferencesUpdate;
    if (
      !initalised &&
      (evt.detail as UIKit.CookiePreferencesUpdate).performance !== undefined &&
      (evt.detail as UIKit.CookiePreferencesUpdate).functionality &&
      (evt.detail as UIKit.CookiePreferencesUpdate).advertising
    ) {
      processVideos();
    }
  };
  const init = (): void => {
    if (!cookiePreferences.functionality || !cookiePreferences.advertising) {
      requirejs(['components/content-unavailable'], (contentModule: UIKit.Module.ContentUnavailable): void => {
        cookiePolicy.setUpdateListener();

        /* When cookie gets updated */
        document.addEventListener('cookie-preferences-updated', ((evt: CustomEvent): void => {
          handleCookiePrefernceChange(evt);
        }) as EventListener);

        contentUnavilable = contentModule;

        processVideos();

        /* add an eevent handler to look at when the cookie is changed on the video item and play it */
        $(document).on('click', '.cookie-update-preferences', (evt: JQuery.ClickEvent): void => {
          evt.preventDefault();

          const video = $(evt.target).closest('.video.video--lazy');
          setTimeout((): void => playVideo({ video }), transitionContainerHeightDuration);
        });
      });

      // window.resize event listener
      window.addEventListener('resize', (): void => {
        // clear the timeout
        if (!throttled) {
          processVideos();
          throttled = true;
          const activeVideo = $('.video.video--active');
          if (activeVideo.length > 0) {
            /* Dom parents */
            /** Active video parent */
            const parent = activeVideo.parent();
            /** Active video parent's parent */
            const container = parent.parent();

            const nearestContentToggle = container.closest('.content-toggle');
            const isWithinContentToggle = nearestContentToggle.length !== 0;

            /** Container height calcaluations. Used for animating the active/ non active states */
            const containerActiveHeight = (container.innerWidth() || 0) * ratio16by9;
            let containerActiveHeightWMargin = containerActiveHeight;

            const isAbsolutedPositioned = activeVideo.attr('data-original-bottom') !== undefined;

            if (!isAbsolutedPositioned) {
              /* Offsets */
              const containerOffset = container.offset() || { left: 0, top: 0 };

              const videoOffsetWithinContainer = isAbsolutedPositioned ? 0 : (activeVideo.offset() || { top: 0 }).top - containerOffset.top;
              containerActiveHeightWMargin += videoOffsetWithinContainer + videoBottomMargin;
            }

            /** whether the video should overlay content ancestor container. Default: false */
            const overlayContent = activeVideo.attr('data-overlay-content') === 'true';
            if (overlayContent) {
              parent.css({ position: 'static' });

              /** Calcuate X padding on both parent and container */
              const padding = getPaddingUnits({ parent, container });

              if (isWithinContentToggle) {
                nearestContentToggle.css({
                  'min-height': containerActiveHeightWMargin,
                });
              }
              container.css({ 'min-height': containerActiveHeightWMargin });

              const cssOpts: any = {
                height: containerActiveHeight,
                width: (container.outerWidth() || 0) - padding.total,
                left: padding.parent.left,
                right: padding.parent.right,
              };

              if (isAbsolutedPositioned) {
                cssOpts.bottom = 0;
              }
              activeVideo.css(cssOpts);
            }
          }

          setTimeout((): void => {
            throttled = false;
          }, delay);
        }
      });
    } else {
      processVideos();
    }

    /* Remove current handler and reset it */
    $(document).off('click', '.video.video--lazy:not(.video--active)');
    $(document).on('click', '.video.video--lazy:not(.video--active)', (evt: JQuery.ClickEvent): void => {
      evt.preventDefault();
      if (evt.target === '.video__wrapper__close') return;

      /** The actve video */
      const video = $(evt.target).closest('.video.video--lazy');
      video.attr('data-original-width', video.outerWidth() || 0);
      video.attr('data-original-height', video.outerHeight() || 0);

      /* Dom parents */
      /** Active video parent */
      const parent = video.parent();
      /** Active video parent's parent */
      const container = parent.parent();

      const nearestContentToggle = container.closest('.content-toggle');
      const isWithinContentToggle = nearestContentToggle.length !== 0;

      /** Container height calcaluations. Used for animating the active/ non active states */
      const containerActiveHeight = (container.innerWidth() || 0) * ratio16by9;
      let containerActiveHeightWMargin = containerActiveHeight;

      /* ignore offset within the container if it's position absolute */
      const isAbsolutedPositioned = video.css('position') === 'absolute';

      if (!isAbsolutedPositioned) {
        /* Offsets */
        const containerOffset = container.offset() || { left: 0, top: 0 };

        const videoOffsetWithinContainer = isAbsolutedPositioned ? 0 : (video.offset() || { top: 0 }).top - containerOffset.top;
        containerActiveHeightWMargin += videoOffsetWithinContainer + videoBottomMargin;
      } else {
        /* set the offset left for close */
        video.attr('data-original-left', video.position().left);
        video.attr('data-original-bottom', video.css('bottom'));
      }

      /** Whether the video should overlay content ancestor container. Default: false */
      const overlayContent = video.attr('data-overlay-content') === 'true';

      /* Group videos by it's container ID. This is used in conjection with overlayContent.*/
      const initalContainerID = container.attr('data-index');
      const containerID = initalContainerID || totalContainers;

      if (initalContainerID == null) {
        /* Add a new conainer group */
        videoReferences[containerID] = {};

        container.attr('data-index', totalContainers);
        totalContainers++;
      }

      const index = video.attr('data-index');

      if (overlayContent) {
        parent.css({ position: 'static' });

        /** Calcuate X padding on both parent and container */
        const padding = getPaddingUnits({ parent, container });

        /** whether the video is full width or it's parent */
        const videoIsFullWidth = video.outerWidth() === (container.innerWidth() || 0) - padding.total;

        container.attr('data-original-height', container.outerHeight() || 0);
        container.attr('data-original-width', container.innerWidth() || 0);

        if (!videoIsFullWidth) {
          video.find('.video__wrapper__overlay').removeClass('d-none');

          if (accessibility.preferences.reduceMotion) {
            /* When the container has a parent content toggle , it will reset the min-height */
            if (isWithinContentToggle) {
              nearestContentToggle.addClass('position-relative align-items-start').css({ 'min-height': containerActiveHeightWMargin });
              nearestContentToggle.attr('data-expanded', 'true');
            }

            container.addClass('position-relative align-items-start').css({ 'min-height': containerActiveHeightWMargin });
            const cssOpts: any = {
              height: containerActiveHeight,
              width: (container.outerWidth() || 0) - padding.total,
              left: padding.parent.left,
              right: padding.parent.right,
            };

            if (isAbsolutedPositioned) {
              cssOpts.bottom = 0;
            }

            video.addClass('position-absolute video--active').css(cssOpts);

            const isShowingOverview = video.find('.content-unavailable--minimal-content');
            video.find('.video__wrapper__close').css({ opacity: 1 });

            if (isShowingOverview.length !== 0) {
              isShowingOverview.find('.content-unavailable__content').removeClass('d-none').addClass('d-block').css({ opacity: 1 });
            }
          } else {
            /* When the container has a parent content toggle , it will reset the min-height */

            if (isWithinContentToggle) {
              nearestContentToggle
                .addClass('position-relative align-items-start')
                .animate({ 'min-height': containerActiveHeightWMargin }, transitionContainerHeightDuration);

              nearestContentToggle.attr('data-expanded', 'true');
            }

            container
              .addClass('position-relative align-items-start')
              .animate({ 'min-height': containerActiveHeightWMargin }, transitionContainerHeightDuration);

            video.addClass('position-absolute').css({
              width: video.attr('data-original-width') || 0,
              'min-height': parseInt(video.attr('data-original-height') || '0'),
              height: parseInt(video.attr('data-original-height') || '0'),
            });

            const animationOpts: UIKit.VideoAnimationOptions = {
              height: containerActiveHeight,
              width: (container.outerWidth() || 0) - padding.total,
              left: padding.parent.left,
              right: padding.parent.right,
            };
            if (isAbsolutedPositioned) {
              animationOpts.bottom = 0;
            }

            video
              .css({ left: video.position().left })
              .addClass('video--active')
              .animate(animationOpts, transitionDuration, (): void => {
                if (!cookiePreferences.advertising || !cookiePreferences.functionality) {
                  /* Transition full content unavailable message */
                  const isShowingOverview = video.find('.content-unavailable--minimal-content');
                  if (isShowingOverview.length === 0) return;

                  isShowingOverview.find('.content-unavailable__content').removeClass('d-none').addClass('d-block');
                  /* Fade close icon */
                  setTimeout((): void => {
                    isShowingOverview.find('.content-unavailable__content').css({ opacity: 1 });
                    video.find('.video__wrapper__close').css({ opacity: 1 });
                  }, 5);
                } else {
                  video.find('.video__wrapper__close').css({ opacity: 1 });
                }
              });
          }

          $(`#video-${index}-placeholder`).removeClass('d-none');
        } else {
          video.addClass('video--active');
          video.find('.video__wrapper__overlay').addClass('d-none');
          video.find('.loader').addClass('d-none').removeClass('loader--active');

          if (accessibility.preferences.reduceMotion) {
            const isShowingOverview = video.find('.content-unavailable--minimal-content');
            if (isShowingOverview.length === 0) return;

            /* Show content unavilable messages */
            isShowingOverview.find('.content-unavailable__content').removeClass('d-none pt-20').addClass('d-block p-10');
            isShowingOverview.find('.content-unavailable__overview').css({ opacity: 1 }).addClass('d-block');
            isShowingOverview.find('.content-unavailable__overview a').removeClass('d-block').addClass('d-none');
            isShowingOverview.find('.content-unavailable__overview .icon').addClass('mt-20');
            isShowingOverview.find('.content-unavailable__content').css({ opacity: 1 });
            isShowingOverview.find('.content-unavailable__content .icon').addClass('d-none');
            video.find('.video__wrapper__close').css({ opacity: 1 });
          } else {
            if (!cookiePreferences.advertising || !cookiePreferences.functionality) {
              /* Transition full content unavailable message */
              const isShowingOverview = video.find('.content-unavailable--minimal-content');

              /* Show content unavilable messages */
              if (isShowingOverview.length === 0) return;
              isShowingOverview.find('.content-unavailable__overview').css({ opacity: 1 }).addClass('d-block');
              isShowingOverview.find('.content-unavailable__content').removeClass('pt-20').addClass('pt-10');

              // video.addClass('video--active');
              isShowingOverview.find('.content-unavailable__overview a').css({ opacity: 0 });
              isShowingOverview.find('.content-unavailable__overview .icon').addClass('mt-20');

              /* Fade close icon */
              setTimeout((): void => {
                isShowingOverview.find('.content-unavailable__content').removeClass('d-none').addClass('d-block');
                isShowingOverview.find('.content-unavailable__overview a').removeClass('d-block').addClass('d-none');

                setTimeout((): void => {
                  isShowingOverview.find('.content-unavailable__content').css({ opacity: 1 });
                  isShowingOverview.find('.content-unavailable__content .icon').addClass('d-none');
                  video.find('.video__wrapper__close').css({ opacity: 1 });
                }, 100);
              }, transitionContainerHeightDuration);
            }
          }
        }
      } else {
        if (!cookiePreferences.advertising || !cookiePreferences.functionality) {
          const isShowingOverview = video.find('.content-unavailable--minimal-content');
          /* Transition full content unavailable message */
          if (isShowingOverview.length > 0) {
            video.addClass('video--active');
            isShowingOverview.find('.content-unavailable__content').removeClass('d-none pt-20').addClass('d-block p-10');
            isShowingOverview.find('.content-unavailable__overview').css({ opacity: 1 }).addClass('d-block');
            isShowingOverview.find('.content-unavailable__overview a').removeClass('d-block').addClass('d-none');
            isShowingOverview.find('.content-unavailable__overview .icon').addClass('mt-20');
            isShowingOverview.find('.content-unavailable__content').css({ opacity: 1 });
            isShowingOverview.find('.content-unavailable__content .icon').addClass('d-none');

            video.find('.video__wrapper__close').css({ opacity: 1 });
          }
        } else {
          video.find('> .icon').css({ opacity: 0 });
        }
      }
      playVideo({ video });
    });

    $(document).on('click', '.video.video--lazy.video--active', (evt: JQuery.ClickEvent): void => {
      if (evt.target === '.video.video--lazy.video--activ') {
        evt.preventDefault();
      }
    });

    $(document).off('click', '.video.video--active .video__wrapper__close');
    /* When close button is clicked */
    $(document).on('click', '.video.video--active .video__wrapper__close', (clickEvt: JQuery.ClickEvent): void => {
      clickEvt.preventDefault();

      const video = $(clickEvt.target).closest('.video.video--lazy');
      const index = video.attr('data-index');
      const uniqueIndex = `video-${index}`;
      const parent = video.parent();
      const container = parent.parent();
      const containerID = container.attr('data-index') || '0';

      /* Pause video */
      if (cookiePreferences.advertising && cookiePreferences.functionality) videoReferences[containerID][uniqueIndex].pauseVideo();

      /*  Transition to normal view and remove classes */
      pauseVideo({ video, disableContainerResize: false });
    });

    /* When user clicks "esc" */
    $(document).on('keyup', (keyEvt: JQuery.KeyUpEvent): void => {
      if (keyEvt.key !== 'Escape') return;
      if ($('.video.video--active').length === 0) return;

      const video = $(keyEvt.target).closest('.video.video--lazy');
      const index = video.attr('data-index');
      const uniqueIndex = `video-${index}`;
      const parent = video.parent();
      const container = parent.parent();
      const containerID = container.attr('data-index') || '0';

      /* Pause video and remove classes */
      if (cookiePreferences.advertising && cookiePreferences.functionality) videoReferences[containerID][uniqueIndex].pauseVideo();
      pauseVideo({ video, disableContainerResize: false });
    });
  };

  const processVideos = (): void => {
    const videos = $('.video.video--lazy:not(.video--placeholder)');
    const disabled = !cookiePreferences.advertising || !cookiePreferences.functionality;
    const preferencesToShow: string[] = [];

    if (!cookiePreferences.advertising) preferencesToShow.push(`<li>${i18n.cookiePreferences.preferences.advertising}</li>`);
    if (!cookiePreferences.functionality) preferencesToShow.push(`<li>${i18n.cookiePreferences.preferences.functionality}</li>`);

    videos.each((index, videoHTML): void => {
      const video = $(videoHTML);
      if (video.attr('data-index') === undefined) video.attr('data-index', index);

      addMarkup({ video, index, disabled, preferencesToShow });
    });
  };
  /**
   * Pause all videos within the videoReferences object that doesn't match
   * the active ID
   * @param activeID active DOM ID
   *
   */
  const pauseNonActiveVideos = (activeID: string): void => {
    for (const container in videoReferences) {
      for (const item in videoReferences[container]) {
        if (item !== activeID) {
          const video = $(`#${item}`).closest('.video.video--lazy');
          /* Pause video and remove classes */
          if (cookiePreferences.advertising && cookiePreferences.functionality) videoReferences[container][item].pauseVideo();
          pauseVideo({
            video,
            disableContainerResize: videoReferences[container][activeID] !== undefined,
          });
        }
      }
    }
  };

  /**
   * Remove classes
   * @param {video: JQuery<HTMLElement>, parent: JQuery<HTMLElement>} options Video dom element and the nearest parent
   */
  const pauseVideo = ({ video, disableContainerResize = false }: { video: JQuery<HTMLElement>; disableContainerResize: boolean }): void => {
    if (!video.hasClass('video--active')) return;

    const parent = video.parent();
    const container = parent.parent();

    const nearestContentToggle = container.closest('.content-toggle');
    const isWithinContentToggle = nearestContentToggle.length !== 0;

    const containerOffset = container.offset() || { left: 0 };
    const parentOffset = parent.offset() || { left: 0 };
    const overlayContent = video.attr('data-overlay-content') === 'true';
    const padding = getPaddingUnits({ parent, container });

    /** Ignore offset within the container if it's position absolute */
    const isAbsolutedPositioned = video.attr('data-original-bottom') !== undefined;

    const leftOffset = parentOffset.left - containerOffset.left + padding.parent.left;
    const isShowingOverview = video.find('.content-unavailable--minimal-content');

    if (!overlayContent && (!cookiePreferences.advertising || !cookiePreferences.functionality) && isShowingOverview.length > 0) {
      isShowingOverview.find('.content-unavailable__content').removeClass('d-block').addClass('d-none').css({ opacity: 0 });
      isShowingOverview.find('.content-unavailable__overview').removeClass('d-none').addClass('d-block').css({ opacity: 1 });
      isShowingOverview.find('.content-unavailable__overview a').css({ opacity: 1 }).removeClass('d-none');
      video.removeClass('video--active');
      return;
    } else if (!overlayContent) {
      video.removeClass('video--active');
      return;
    }

    if (parseInt(video.attr('data-original-width') || '0') !== parseInt(container.attr('data-original-width') || '0') - padding.total) {
      /* peformance, show the static image instead of the embed */
      video.find('.video__src').addClass('d-none');

      if (accessibility.preferences.reduceMotion) {
        const isShowingOverview = video.find('.content-unavailable--minimal-content');

        if (isShowingOverview.length > 0) {
          isShowingOverview.find('.content-unavailable__content').removeClass('d-block').addClass('d-none');
        }
        video.removeClass('position-absolute video--active').css({ height: '', width: '', left: '', right: '', bottom: '' });

        if (!disableContainerResize) {
          /* When the container has a parent content toggle , it will reset the min-height */
          if (isWithinContentToggle) {
            nearestContentToggle.css({ 'min-height': '' });
            nearestContentToggle.attr('data-expanded', 'false');
          }

          /* Update the container height if no other videos within the container is queued to play */
          container.css({ 'min-height': '' });
        }

        $(`#video-${video.attr('data-index')}-placeholder`).addClass('d-none');
      } else {
        // video.addClass('video--transitioning');
        const isShowingOverview = video.find('.content-unavailable--minimal-content');

        if (isShowingOverview === null) {
          closeActiveVideo({
            video,
            isAbsolutedPositioned,
            isShowingOverview,
            leftOffset,
            container,
            contentToggle: isWithinContentToggle ? nearestContentToggle : undefined,
            disableContainerResize,
          });

          $(`#video-${video.attr('data-index')}-placeholder`).addClass('d-none');
        } else {
          video.find('.loader').addClass('loader--active').removeClass('d-none');
          /* fade content-unavailable message */
          isShowingOverview.find('.content-unavailable__content').css({ opacity: 0 });

          /* Remove content-unavailable message */
          video.find('.video__wrapper__close').css({ opacity: 0 });

          setTimeout((): void => {
            closeActiveVideo({
              video,
              isAbsolutedPositioned,
              isShowingOverview,
              leftOffset,
              container,
              contentToggle: isWithinContentToggle ? nearestContentToggle : undefined,
              disableContainerResize,
            });
          }, 150);
        }
      }
    } else {
      if (accessibility.preferences.reduceMotion) {
        const isShowingOverview = video.find('.content-unavailable--minimal-content');
        if (isShowingOverview.length === 0) return;

        isShowingOverview.find('.content-unavailable__content').removeClass('d-block').addClass('d-none').css({ opacity: 0 });
        isShowingOverview.find('.content-unavailable__overview').removeClass('d-none').addClass('d-block').css({ opacity: 1 });
        isShowingOverview.find('.content-unavailable__overview a').css({ opacity: 1 }).removeClass('d-none');
        video.find('.video__wrapper__close').css({ opacity: 0 });
        video.removeClass('video--active');
      } else {
        if (!cookiePreferences.advertising || !cookiePreferences.functionality) {
          /* Transition full content unavailable message */

          if (isShowingOverview.length === 0) return;

          /* hide full content */
          isShowingOverview.find('.content-unavailable__content').css({ opacity: 0 });

          /* Fade close icon and show the overview content */
          setTimeout((): void => {
            isShowingOverview.find('.content-unavailable__overview a').removeClass('d-none');
            video.removeClass('video--active');
            isShowingOverview.find('.content-unavailable__content').addClass('d-none').removeClass('d-block');

            setTimeout((): void => {
              isShowingOverview.find('.content-unavailable__overview a').css({ opacity: 1 });
              video.find('.video__wrapper__close').css({ opacity: 0 });
            }, 100);
          }, transitionContainerHeightDuration);
        }
      }
    }

    return;
  };

  /**
   * Play the video and enable the loader
   */
  const playVideo = ({ video }: { video: JQuery<HTMLElement> }): void => {
    const index = video.attr('data-index');
    const uniqueIndex = `video-${index}`;
    const parent = video.parent();
    const container = parent.parent();
    const containerID = container.attr('data-index') || '0';

    /* Stop any other videos on the page */
    pauseNonActiveVideos(uniqueIndex);

    if (cookiePreferences.advertising && cookiePreferences.functionality) {
      const youtubeid = video.attr('data-youtubeid');
      const youtubeList = video.attr('data-youtubelist');

      /* Play video if we have already loaded the video in previously */
      if (
        videoReferences[containerID][uniqueIndex] !== undefined &&
        (videoReferences[containerID][uniqueIndex] as UIKit.Video.DisabledVideo).default === undefined
      ) {
        setTimeout((): void => {
          video.find('.video__src').removeClass('d-none');
          videoReferences[containerID][uniqueIndex].playVideo();
        }, transitionDuration);
        return;
      } else {
        video.find('.loader').addClass('loader--active').addClass('show');
      }

      let playerVars: YT.PlayerVars = {
        showinfo: 0,
        playsinline: 1,
        modestbranding: 1,
        list: youtubeList,
      };

      /* Load Youtube video into the DOM ID */
      videoReferences[containerID][uniqueIndex] = new YT.Player(uniqueIndex, {
        height: '390',
        width: '640',
        videoId: youtubeid,
        playerVars,
        events: {
          onReady: (evt): void => {
            /* Play video once it's ready */
            video.find('.video__src').removeClass('d-none');
            video.find('.loader').removeClass('show').removeClass('loader--active');
            evt.target.playVideo();
          },
        },
      });
    } else {
      videoReferences[containerID][uniqueIndex] = {
        default: true,
        pauseVideo: (): void => {},
        playVideo: (): void => {},
      };
    }
  };

  /**
   * Close video window
   */
  const closeActiveVideo = ({
    isShowingOverview,
    video,
    isAbsolutedPositioned,
    container,
    leftOffset,
    contentToggle,
    disableContainerResize,
  }: {
    isShowingOverview: JQuery<HTMLElement>;
    video: JQuery<HTMLElement>;
    isAbsolutedPositioned: boolean;
    container: JQuery<HTMLElement>;
    contentToggle?: JQuery<HTMLElement>;
    disableContainerResize: boolean;
    leftOffset: number;
  }): void => {
    /* Hide unavailable content */
    isShowingOverview.find('.content-unavailable__content').removeClass('d-block').addClass('d-none');

    const animationOpts: UIKit.VideoAnimationOptions = {
      height: parseInt(video.attr('data-original-height') || '0'),
      width: parseInt(video.attr('data-original-width') || '0'),
      left: leftOffset + (isAbsolutedPositioned ? parseInt(video.attr('data-original-left') || '0') : 0),
    };

    /* If the position is absolute before starting - used for animating to original position */
    if (isAbsolutedPositioned) {
      animationOpts.bottom = parseInt(video.attr('data-original-bottom') || '0');
    }

    video.css({ 'z-index': 101 }).animate(animationOpts, transitionDuration, (): void => {
      /* Hide placeholder */
      $(`#video-${video.attr('data-index')}-placeholder`).addClass('d-none');

      /* reset all inline modifiers */
      video
        .removeClass('position-absolute video--transitioning')
        .css({
          left: '',
          right: '',
          width: '',
          height: '',
          bottom: '',
          'min-width': '',
          'min-height': '',
          'z-index': '',
        })
        .removeClass('video--active');

      if (!disableContainerResize) {
        /* Update the container height if no other videos within the container is queued to play */

        container.animate({ 'min-height': container.attr('data-original-height') }, transitionContainerHeightDuration, (): void => {
          /* When the container has a parent content toggle , it will reset the min-height */
          if (contentToggle !== undefined) {
            contentToggle.css({ 'min-height': '' });
            contentToggle.attr('data-expanded', 'false');
          }

          container.css({ 'min-height': '' });
        });
      }
    });
  };

  /**
   * Adds the correct markup if any does not exists
   *
   * @param video DOM element of the video
   * @param index Number index of the video in the list
   */
  const addMarkup = ({
    video,
    index,
    disabled,
    preferencesToShow,
  }: {
    video: JQuery<HTMLElement>;
    index: number;
    disabled: boolean;
    preferencesToShow: string[];
  }): void => {
    /* If the play icon is not in the DOM */
    if (!disabled && video.find('> .icon').length === 0) {
      const image = video.find('> img');

      if (image !== null) {
        $(image).after(
          `<svg viewBox="0 0 40 32" xmlns="http://www.w3.org/2000/svg" class="icon"><title>Play video</title><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-300.000000, -119.000000)"><g transform="translate(135.000000, 30.000000)"><g transform="translate(165.000000, 89.000000)"><rect class="bg" fill="#D4374A" x="0.397191135" y="0" width="39.1836735" height="31.0204082" rx="8"></rect><polygon class="fg" fill="#FFFFFF" points="15.0910687 8.97959184 15.0910687 22.8571429 26.5196401 15.9183673"></polygon></g></g></g></g></svg>`
        );
      }
    }

    /* add unavailable message here */
    if (!disabled) {
      const disabledOverlay = video.find('.content-unavailable');
      if (disabledOverlay !== null) {
        /* Fade the image */
        disabledOverlay.css({ opacity: 0 });

        /* Once fade is competed, remove from DOM */
        setTimeout((): void => {
          disabledOverlay.remove();
          video.find('> img, > .icon').removeClass('d-none');
          video.removeClass('video--disabled');
          video.attr('href', video.attr('data-href') || '');
        }, transitionContainerHeightDuration);
      }
    } else if (!video.hasClass('video--active')) {
      video.attr('data-href', video.attr('href') || '');
      video.removeAttr('href');
      video.addClass('video--disabled');
      video.find('> img, > .icon').addClass('d-none');

      const unavailableContent = contentUnavilable.getDisabledYoutubeHTML({
        serviceProvider: 'Youtube',
        youtubeID: video.attr('data-youtubeid'),
        index,
        alt: '',
        width: parseInt(video.attr('data-original-width') || `${video.width()}`),
        preferencesToShow,
        url: video.attr('data-href') || '',
      });

      /* if image doesn't exists yet */
      if (video.find('.content-unavailable').length === 0) {
        video.append(unavailableContent);
      } else {
        /* Re-render the content (used for re-sizing) */

        video.find('.content-unavailable').replaceWith(unavailableContent);
      }
    }

    /* if the video does not already have the wrapper */
    if (video.find('.video__wrapper').length === 0) {
      video.append(`<div class="video__wrapper video__wrapper--dismissible">
        <div class="video__wrapper__overlay">
          <a class="video__wrapper__close" href="#" aria-label="Close video" title="Close video">
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><title>Close</title><path d="M29.75,24.5l0.06,0.06a0.69,0.69,0,0,1,.13.16,0.12,0.12,0,0,0,0,.09,0.69,0.69,0,0,1,0,.25v0.22a1,1,0,0,1-.25.47l-4,4a0.56,0.56,0,0,1-.22.16,0.84,0.84,0,0,1-.47.09,0.69,0.69,0,0,1-.25,0l-0.06-.06a0.36,0.36,0,0,1-.19-0.09L16,21.25,7.44,29.81a0.27,0.27,0,0,1-.16.09L7.19,30a0.69,0.69,0,0,1-.25,0,0.47,0.47,0,0,1-.22,0,0.73,0.73,0,0,1-.47-0.22l-4-4a0.56,0.56,0,0,1-.16-0.22A0.84,0.84,0,0,1,2,25.06a0.69,0.69,0,0,1,0-.25l0.06-.09a0.26,0.26,0,0,1,.09-0.16L10.75,16,2.19,7.44s0-.06-0.06-0.09a0.55,0.55,0,0,1-.06-0.16A0.74,0.74,0,0,1,2,6.94a0.69,0.69,0,0,1,0-.25,0.73,0.73,0,0,1,.22-0.44l4-4a0.56,0.56,0,0,1,.22-0.16A0.84,0.84,0,0,1,6.93,2a0.72,0.72,0,0,1,.25.06H7.28a0.66,0.66,0,0,1,.16.13L16,10.75l8.56-8.56a1,1,0,0,1,.19-0.12,0.06,0.06,0,0,0,.06,0,0.69,0.69,0,0,1,.25,0h0.25a1,1,0,0,1,.44.25l4,4a0.56,0.56,0,0,1,.16.22,0.84,0.84,0,0,1,.09.47,0.69,0.69,0,0,1,0,.25,0.06,0.06,0,0,0,0,.06l-0.12.19L21.25,16Z" transform="translate(-2 -2)"></path></svg>
          </a>
        </div>
        <div id="video-${index}" class="video__src d-none"></div>
        <div class="loader loader--centered fade"></div>
      </div>`);
    }

    /* If the position is absolute then don't bother adding the placeholder */
    if (video.css('position') === 'absolute') return;

    /* if the video does not already have the placeholder */
    if ($(`#video-${index}-placeholder`).length === 0) {
      $(video).after(
        `<div id="video-${index}-placeholder" class="d-none ${video[0].className} video--placeholder" style="height: ${$(
          video
        ).outerHeight()}px"></div>`
      );
    }
  };

  /**
   * Gets X padding for both parent and it's container
   */
  const getPaddingUnits = ({ parent, container }: { parent: JQuery<HTMLElement>; container: JQuery<HTMLElement> }): UIKit.Video.PaddingUnit => {
    const parentPadding = {
      left: parseInt(parent.css('padding-left').replace('px', '') || '0'),
      right: parseInt(parent.css('padding-right').replace('px', '') || '0'),
    };

    const containerPadding = {
      left: parseInt(container.css('padding-left').replace('px', '') || '0'),
      right: parseInt(container.css('padding-right').replace('px', '') || '0'),
    };

    return {
      parent: parentPadding,
      container: containerPadding,
      total: parentPadding.left + parentPadding.right + containerPadding.left + containerPadding.right,
    };
  };
  return {
    init,
    pauseNonActiveVideos,
    pauseVideo,
    addMarkup,
    getPaddingUnits,
  };
});
