define(['jquery'], ($: JQueryStatic): UIKit.Module.TabFocus => {
  const url = window.location.href;

  const init = (): void => {
    let anchor = url.substring(url.indexOf('#') + 1);

    /* Check if anchor exists and it is a tab */
    if (anchor !== undefined && anchor.length) {
      const elm = $(`a[href="#${anchor}"]`);

      if (elm.data('toggle') === 'tab') {
        elm.tab('show');
      }

      //scroll to tab anchoronst offset = $(href).offset();
      const offset = elm.offset();
      let top = 0;

      if (offset !== undefined) {
        top = offset.top;
      }

      elm[0].scrollIntoView(true);
      setTimeout((): void => {
        window.scrollBy(0, 100);
      }, 500);
    }
  };
  return {
    init,
  };
});
