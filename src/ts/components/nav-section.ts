define(['jquery', 'observer'], ($: JQueryStatic, intersect: any): UIKit.Module.NavSection => {
  const paddingOffset = 40;
  const init = (): void => {
    var IntersectionObserver = intersect;
    navStick();

    $('.nav-section.sticky-top').before('<div class="nav-section-top"></div>');
    const handler = document.querySelector('.nav-section-top');

    if (handler !== null) {
      observer.observe(handler);
    }
  };

  const observer = new IntersectionObserver(
    function (entries): void {
      for (let i = 0; i < entries.length; i++) {
        if (entries[i].intersectionRatio === 0) {
          $('.nav-section-title').addClass('d-block');
          $('.nav-section-title').removeClass('d-none');
          $('.nav-section-subtitle').addClass('d-md-block');
        } else if (entries[i].intersectionRatio === 1) {
          $('.nav-section-title').addClass('d-none');
          $('.nav-section-title').removeClass('d-block');
          $('.nav-section-subtitle').removeClass('d-md-block');
        }
      }
    },
    { threshold: [0, 1] }
  );

  const navStick = (): void => {
    let navItems = $('.nav-section.sticky-top .navbar .nav .nav-link');

    navItems.on('click', function (e: JQuery.ClickEvent): void {
      e.preventDefault();

      let href = $(this).attr('href');

      if (href !== '#' && href !== undefined && $(href) !== undefined) {
        const title = $('.nav-section-title');
        const subtitle = $('.nav-section-subtitle');
        const height = $(this).closest('.nav-section').height() || 0;

        let top = 0;

        if (href !== undefined && href !== '#') {
          const offset = $(href).offset();
          if (offset !== undefined) {
            top = offset.top;
          } else {
            top = 0;
          }
        } else {
          top = 0;
        }

        let offset = top - height - paddingOffset;

        /* If width is over MD breakpoint and if doesn't already have the class set */
        if (window.innerWidth >= 768 && subtitle !== null && !subtitle.hasClass('d-md-block')) {
          offset -= 30;
        }

        /* if the title is still hidden */
        if (title !== null && title.hasClass('d-none')) {
          offset -= 40;
        }
        $(window).scrollTop(offset);
      }
    });

    $(window).scroll(function (): void {
      const windowOffset = $(window).scrollTop() || 0;
      let navItems = $('.nav-section.sticky-top .navbar .nav .nav-link');

      navItems.each(function (): void {
        let href = $(this).attr('href');

        if (href !== undefined && href !== '#') {
          let targetDiv = $(href);

          if (targetDiv !== undefined && targetDiv.attr('#') !== '#') {
            let offset = targetDiv.offset();
            const top = offset !== undefined ? offset.top : 0;
            const topOffset = $('.nav-section-subtitle') !== undefined && $('.nav-section-subtitle').hasClass('d-md-block') ? 165 : 145;

            if (top - topOffset <= windowOffset) {
              let targetAnchor = $(`.nav-section.sticky-top a[href="${href}"]`);
              if (!targetAnchor.parent().hasClass('active')) {
                $('.nav-section.sticky-top .navbar .nav .nav-item.active').removeClass('active');
                targetAnchor.parent().addClass('active');

                $('.nav-section.sticky-top .navbar')[0].scrollLeft = targetAnchor[0].offsetLeft - 20;
              }
            }
          }
        }
      });
    });
  };

  return {
    init,
    navStick,
  };
});
