define(['jquery', 'i18n!locale/globals'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.Thumbnails => {
  const modal = ` <div id="thumbnail-modal" class="modal modal-image {{{image-portrait-class}}} fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close-modal" data-dismiss="modal">
            ${i18n.close} <svg class="icon" focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
            <title>Close</title>
            <path d="M29.75,24.5l0.06,0.06a0.69,0.69,0,0,1,.13.16,0.12,0.12,0,0,0,0,.09,0.69,0.69,0,0,1,0,.25v0.22a1,1,0,0,1-.25.47l-4,4a0.56,0.56,0,0,1-.22.16,0.84,0.84,0,0,1-.47.09,0.69,0.69,0,0,1-.25,0l-0.06-.06a0.36,0.36,0,0,1-.19-0.09L16,21.25,7.44,29.81a0.27,0.27,0,0,1-.16.09L7.19,30a0.69,0.69,0,0,1-.25,0,0.47,0.47,0,0,1-.22,0,0.73,0.73,0,0,1-.47-0.22l-4-4a0.56,0.56,0,0,1-.16-0.22A0.84,0.84,0,0,1,2,25.06a0.69,0.69,0,0,1,0-.25l0.06-.09a0.26,0.26,0,0,1,.09-0.16L10.75,16,2.19,7.44s0-.06-0.06-0.09a0.55,0.55,0,0,1-.06-0.16A0.74,0.74,0,0,1,2,6.94a0.69,0.69,0,0,1,0-.25,0.73,0.73,0,0,1,.22-0.44l4-4a0.56,0.56,0,0,1,.22-0.16A0.84,0.84,0,0,1,6.93,2a0.72,0.72,0,0,1,.25.06H7.28a0.66,0.66,0,0,1,.16.13L16,10.75l8.56-8.56a1,1,0,0,1,.19-0.12,0.06,0.06,0,0,0,.06,0,0.69,0.69,0,0,1,.25,0h0.25a1,1,0,0,1,.44.25l4,4a0.56,0.56,0,0,1,.16.22,0.84,0.84,0,0,1,.09.47,0.69,0.69,0,0,1,0,.25,0.06,0.06,0,0,0,0,.06l-0.12.19L21.25,16Z" transform="translate(-2 -2)" />
          </svg>
          </button>
        </div>
        <div class="modal-body">
          <figure class="image {{{downloadable}}}">
            {{{image}}}
            {{{caption}}}
          </figure>
        </div>
      </div>
    </div>
  </div>`;

  const landscapeImage = `<img  src="{{{image-url}}}?w=1190&ar=16:9&auto=format&fit=crop&q=60"  srcset="{{{image-url}}}?w=960&auto=format&fit=crop&q=60 960w, {{{image-url}}}?w=960&ar=16:9&auto=format&fit=crop&q=60 960w, {{{image-url}}}?w=500&auto=format&fit=crop&q=60 500w, {{{image-url}}}?w=400&auto=format&fit=crop&q=60 400w,  {{{image-url}}}?w=300&auto=format&fit=crop&q=60 300w," alt="{{{alt}}}" sizes="(max-width: 767px) 100vw, (min-width: 768px) 744px, (min-width: 980px) 960px"/>`;

  const portraitImage = `<img src="{{{image-url}}}?w=960&auto=format&fit=crop&q=60"  srcset="{{{image-url}}}?w=960&ar=16:9&auto=format&fit=crop&q=60 960w, {{{image-url}}}?w=744&auto=format&fit=crop&q=60 744w, {{{image-url}}}?w=500&auto=format&fit=crop&q=60 500w, {{{image-url}}}?w=400&auto=format&fit=crop&q=60 400w,  {{{image-url}}}?w=300&auto=format&fit=crop&q=60 300w," alt="{{{alt}}}" sizes="(max-width: 767px) 100vw, (min-width: 768px) 744px, (min-width: 980px) 960px, (min-width: 1200px) 1190px"/>`;

  /**.
   * Process all thumbnails and add event listeners to each one.
   */
  const init = (): void => {
    $('a.thumbnail.with-modal').on('click', (e): void => {
      e.preventDefault();
      const thumbnail = $(e.currentTarget);
      const caption = thumbnail.data('caption');
      const alt = thumbnail.data('alt') || '';
      const downloadLink = thumbnail.data('download-link');
      const imageLink = thumbnail.attr('href') || '';
      const imageLinkParts = imageLink.split('?');

      let captionHtml = '';

      if (caption !== undefined) {
        captionHtml = `<figcaption class="caption image__caption">
          <p class="image__caption__content">${caption}</p>`;

        if (downloadLink !== undefined) {
          captionHtml += `<a href="${downloadLink}" class="btn btn-primary image__download">${i18n.downloadImage}</a>`;
        }

        captionHtml += `</figcaption>`;
      } else if (downloadLink !== undefined) {
        captionHtml = `<figcaption class="caption image__caption">
          <p class="image__caption__content" aria-hidden="true"></p>
          <a href="${downloadLink}" class="btn btn-primary image__download">${i18n.downloadImage}</a>
        </figure>`;
      }

      /* Check if the image is portrait or landscape */
      const imageWidth = getParameterByName(`?${imageLinkParts[1]}` || '', 'w');
      const imageHeight = getParameterByName(`?${imageLinkParts[1]}` || '', 'h');
      const imageDataWidth = thumbnail.get(0).dataset.imageWidth || null;
      const imageDataHeight = thumbnail.get(0).dataset.imageHeight || null;
      let isLandscape = true;

      /* If the URL has a 'w' field AND 'h' is null or 'h' is more than 'w', set isLandscape to false */
      if (
        (imageWidth !== null && imageHeight === null) ||
        (imageWidth !== null && imageHeight !== null && parseInt(imageWidth) <= parseInt(imageHeight))
      ) {
        isLandscape = false;
      }

      /* check the data height and width */
      if (
        (imageDataWidth !== null && imageDataHeight === null) ||
        (imageDataWidth !== null && imageDataHeight !== null && parseInt(imageDataWidth) <= parseInt(imageDataHeight))
      ) {
        isLandscape = false;
      }

      let imageTemplate = isLandscape ? landscapeImage : portraitImage;

      let modalHtml = modal
        .replace('{{{downloadable}}}', downloadLink !== undefined ? 'image--downloadable' : '')
        .replace('{{{image}}}', imageTemplate.replace(/{{{image-url}}}/g, imageLinkParts[0]).replace('{{{alt}}}', alt))
        .replace('{{{image-portrait-class}}}', !isLandscape ? 'modal-image--portrait' : '')
        .replace('{{{caption}}}', captionHtml);

      if ($('#thumbnail-modal').length) {
        $('#thumbnail-modal').remove();
      }

      $('body').append(modalHtml);
      $('#thumbnail-modal').modal('show');
    });

    positionModalIcons();
  };

  /**
   * Get the value of a query parameter
   * @param queryString querystring part of the URL including the ?
   * @param name name of the attribute to search for
   */
  const getParameterByName = (queryString: string, name: string): null | string => {
    const match = RegExp('[?&]' + name + '=([^&]*)').exec(queryString);

    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  };

  const positionModalIcons = (): void => {
    $('.thumbnail.with-modal').each((e, element: HTMLElement): void => {
      const $thumb = $(element);

      $(
        `<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.03 32.03"><title>Zoom in</title><path d="M29.66,32a0.92,0.92,0,0,0,.69-0.28l1.41-1.41A1.7,1.7,0,0,0,32,29.66a1.27,1.27,0,0,0-.31-0.72l-9.62-9.66A0.08,0.08,0,0,1,22,19.22l-0.09-.06a0.8,0.8,0,0,1-.22-0.09A12.09,12.09,0,0,0,24,12a11.68,11.68,0,0,0-3.56-8.53A11.55,11.55,0,0,0,12,0,11.35,11.35,0,0,0,3.5,3.56,11.49,11.49,0,0,0,0,12a11.59,11.59,0,0,0,3.5,8.44A11.47,11.47,0,0,0,12,24a12.09,12.09,0,0,0,7.09-2.31l0.19,0.38L29,31.75A0.92,0.92,0,0,0,29.66,32ZM17.72,6.38A7.59,7.59,0,0,1,20,12a7.5,7.5,0,0,1-2.28,5.63A7.5,7.5,0,0,1,12.06,20a7.61,7.61,0,0,1-5.66-2.31,7.93,7.93,0,0,1,0-11.38A7.74,7.74,0,0,1,12.06,4a7.5,7.5,0,0,1,5.66,2.37h0ZM11,11H7v2h4v4h2V13h4V11H13V7H11v4Z" transform="translate(0 0)" /></svg>`
      ).appendTo($thumb);
    });
  };

  return {
    init,
    positionModalIcons,
  };
});
