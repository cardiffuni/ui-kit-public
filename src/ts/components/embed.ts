define(['jquery', 'components/cookie-policy', 'i18n!locale/globals.min'], (
  $: JQueryStatic,
  cookiePolicy: UIKit.Module.CookiePolicy,
  i18n: UIKit.I18n
): UIKit.Module.Embed => {
  const embedlykey = 'a785af81d09d43b68a2b466a9fd6c6db';
  let cookiePreferences = cookiePolicy.getCookiePreferences();
  let initalised = false;

  const init = (): void => {
    if (cookiePreferences.functionality && cookiePreferences.advertising) {
      processEmbeds();
    } else {
      requirejs(['components/content-unavailable'], (contentUnavilable: UIKit.Module.ContentUnavailable): void => {
        cookiePolicy.setUpdateListener();

        /* When cookie gets updated */
        document.addEventListener('cookie-preferences-updated', ((evt: CustomEvent): void => {
          cookiePreferences = evt.detail as UIKit.CookiePreferencesUpdate;
          if (
            !initalised &&
            (evt.detail as UIKit.CookiePreferencesUpdate).performance !== undefined &&
            (evt.detail as UIKit.CookiePreferencesUpdate).functionality &&
            (evt.detail as UIKit.CookiePreferencesUpdate).advertising
          ) {
            processEmbeds();
          }
        }) as EventListener);

        const preferencesToShow: string[] = [];

        if (!cookiePreferences.advertising) preferencesToShow.push(`<li>${i18n.cookiePreferences.preferences.advertising}</li>`);
        if (!cookiePreferences.functionality) preferencesToShow.push(`<li>${i18n.cookiePreferences.preferences.functionality}</li>`);

        /* Process all embeds and add content unavailable */
        ($('a.embed') as JQuery<HTMLAnchorElement>).each(function (this, index: number): void {
          const link = $(this);
          link.attr('data-href', link.attr('href') || '');

          const alt = link.text();
          const serviceProvider = serviceProviderName(link.attr('href') || '');
          const youtubeID = serviceProvider === 'Youtube' ? getYoutubeID(link.attr('href') || '') : '';

          link.html(
            contentUnavilable.getDisabledEmbedHTML({
              serviceProvider,
              youtubeID,
              index,
              alt,
              preferencesToShow,
              url: link.attr('href') || '',
            })
          );

          link.removeAttr('href');
          $(this).addClass('embed--disabled');
        });
      });
    }
  };

  /* Embed each link onto the page */
  const processEmbeds = (): void => {
    const wind: UIKit.EmbedWindow = window;

    /* Prevent mutliple re-renders and only  */
    initalised = true;
    let containsYotube = false;
    let requiresEmbeddly: undefined | boolean;

    ($('a.embed') as JQuery<HTMLAnchorElement>).each(function (): void {
      const anchor = $(this);
      if (anchor.attr('data-href') !== undefined) {
        anchor.attr('href', anchor.attr('data-href') || '');
      }

      // only links with class of wind.embedly should be used
      let embedURL = this.href;
      const serviceProvider = serviceProviderName(embedURL);

      if (serviceProvider === 'Youtube') {
        /* render using components/videos not embedly */
        containsYotube = true;
        renderYoutubeEmbed(anchor);
      } else if (embedURL.indexOf('hml.cardiff.ac.uk') !== -1) {
        /* render using cardiffPlayer not embedly */
        renderCardiffPlayerEmbed(anchor);
      } else if (embedURL.indexOf('twitter.com') !== -1) {
        /* load the twitter widget  */
        wind.twttr = (function (d, s, id): void {
          let js: HTMLScriptElement;
          let fjs = d.getElementsByTagName(s)[0];
          let t = wind.twttr || {};

          if (d.getElementById(id)) return;
          js = d.createElement(s) as HTMLScriptElement;
          js.id = id;
          js.src = 'https://platform.twitter.com/widgets.js';

          if (fjs.parentNode !== null) {
            fjs.parentNode.insertBefore(js, fjs);
          }

          t._e = [];
          t.ready = function (f: string): void {
            t._e.push(f);
          };
          return t;
        })(document, 'script', 'twitter-wjs');

        /* render using twitter not embedly */
        renderTwitterEmbed($(this));
      } else if (embedURL.indexOf('xerte.cardiff.ac.uk') !== -1) {
        /* render using xerte not embedly */
        renderXerteEmbed($(this));
      } else {
        requiresEmbeddly = true;
        anchor.attr('data-embedly-required', 'true');
      }
    });

    /* If any import requires embledly */
    if (requiresEmbeddly && requiresEmbeddly !== undefined) {
      requirejs(['embedly'], (): void => {
        wind.embedly('defaults', { cards: { key: embedlykey } });

        $('head').append(
          '<style class="embedly-css">#cards, .embedly-card, .embedly-card-hug{min-width: 100% !important;}.card .hdr {display: none;visibility: hidden;}.card .action{background: #d4374a;color: #fff;border: none;-webkit-border-radius: 2px; -moz-border-radius: 2px; border-radius: 2px;text-shadow: none; font-family: "franklin_gothic_fs_medregular",Arial,"Helvetica Neue",sans-serif; line-height: 19px; padding: 12px 12px 10px 12px; width: auto; display: inline-block;}.card .action:hover, .card .action:focus, .card .action:active{color: #fff; background-color: #e27683;}</style>'
        );

        /* Prevent share icons */
        $('.embed').attr('data-card-controls', '0');
        /* Process all links with .embed, and begin rendering */
        wind.embedly('card', '[data-embedly-required="true"]');

        /* When embedly has finished rendering a lin */
        wind.embedly('on', 'card.rendered', function (card: HTMLElement): void {
          let content = $(card).contents().find('body .card .bd .txt-bd');
          //check for panopto
          let url = content.find('.action').prop('href');

          if (url.indexOf('panopto.eu') != -1) {
            let hug = $(card).closest('.embedly-card-hug');
            let self = $(card).closest('.embedly-card');

            self.remove();
            hug.append(`<div class="well panopto-no-video text-center">
              <div class="row">
                <p>This video is not available because the author has restricted access to it.</p>
                <a href="${url}" target="_blank" class="btn btn-primary">View on Panopto</a>
              </div>
            </div>`);
          } else {
            content.find('.action').text(`Continue reading on ${content.find('.title').text()}`);
          }
        });
      });
    }

    /* Youtube vide */
    if (containsYotube) {
      /* Import videos for youtube playback and lazy loading for lazy images */
      requirejs(['components/video', 'components/lazy-loader'], (videos: UIKit.Module.Video, lazyLoader: UIKit.Module.LazyLoader): void => {
        videos.init();
        lazyLoader.init();
      });
    }
  };

  /**
   * Renders cardiff player embed
   */
  const renderCardiffPlayerEmbed = (embed: JQuery<HTMLElement>): void => {
    let embedURL = embed.prop('href');
    let embedURLSplit = embedURL.split('/');
    let embedVideoID = embedURLSplit[4];

    if (embedVideoID == undefined) {
      embed.wrap('<div class="alert"></div>');
      embed.prepend(`Error: Missing video ID. (URL: ${embedURL}, ID: ${embedVideoID})`);
    } else {
      embed.wrap(`<figure class="video video ${embedVideoID}>
        <div class="video-body"></figcaption>
      </figure>`);

      let videoHeight = $('.video' + embedVideoID).height();
      let videoWidth = $('.video' + embedVideoID).width();

      embed.wrap(
        `<iframe src="https://hml.cardiff.ac.uk/player?autostart=n&fullscreen=y&width=${videoWidth}&height=${videoHeight}&videoId=${embedVideoID}&quality=hi&captions=n&chapterId=0"></iframe>`
      );
    }
  };

  const renderYoutubeEmbed = (embed: JQuery<HTMLElement>): void => {
    let embedURL = embed.prop('href').replace('https://', '');
    let youtubeID = getYoutubeID(embedURL);
    let listID = '';

    /* Youtube list ID */
    if (embedURL.indexOf('list=') !== -1) {
      listID = embedURL.split('list=')[1];
      const ampersandPosition = listID.indexOf('&');

      if (ampersandPosition) {
        listID = listID.substring(0, ampersandPosition);
      }
    }

    if (youtubeID === undefined) {
      embed[0].outerHTML = `<div class="alert"><p>Error: Missing Youtube video reference.</p>
        <small>
          <strong>Information</strong><br>
          Request url: ${embedURL}<br>
          Youtube ID: Missing
        </small>
      </div>`;

      return;
    }

    embed[0].outerHTML = `<a href="${embed.prop('href')}" class="video video--lazy" data-youtubeid="${youtubeID}" data-youtubelist="${listID}">
      <img class="lazy" src="https://cardiff.imgix.net/__data/assets/image/0008/1279637/DefaultLogo.png?w=320&h=180&fit=crop&q=60&auto=format" data-src="https://img.youtube.com/vi/${youtubeID}/mqdefault.jpg" alt="alt text">
    </a>`;
  };

  const renderTwitterEmbed = (embed: JQuery<HTMLElement>): void => {
    const wind: UIKit.EmbedWindow = window;
    const embedURL = embed.prop('href');
    const tweetID = embedURL.match(/[\d]+$/);

    embed.wrap(`<div class="embedded embedded-twitter" data-tweetid="'${tweetID}"></div>`);
    $(`.embedded.embedded-twitter[data-tweetid="${tweetID}"]`)
      .addClass(embed.attr('class') || '')
      .removeClass('embed');

    embed.remove();

    wind.twttr.ready(function (twttr: any): void {
      twttr.widgets.createTweet(tweetID[0], $('.embedded.embedded-twitter[data-tweetid="' + tweetID + '"]').get(0));
    });
  };

  const renderXerteEmbed = (embed: JQuery<HTMLElement>): void => {
    let embedURL = embed.prop('href');
    let embedURLSplit = embedURL.split('/');
    let embedXerteID = embedURLSplit[3];

    embed.wrap(`<div class="embedded embedded-xerte" id="xerte-${embedXerteID}">`);

    let xerteWidth: number = $('#xerte-' + embedXerteID).width() || 0;
    let xerteHeight = (xerteWidth / 4) * 3;

    embed.wrap(`<iframe src="${embedURL}" width="${xerteWidth}" height="${xerteHeight}"></iframe>'`);
  };

  const serviceProviderName = (url: string): ServiceProvider | string => {
    /* Facebook */
    if (url.match(/((http|https):\/\/)(www\.facebook.com|fb.(me|com))/) !== null) return 'Facebook';
    if (url.match(/((http|https):\/\/)(([a-zA-Z-0-9]+.|)(youtube.|youtu.))/) !== null) return 'Youtube';
    if (url.match(/((http|https):\/\/)(www.|)(soundcloud.com|snd.sc)/) !== null) return 'Soundcloud';
    if (url.match(/((http|https):\/\/)([a-zA-Z-0-9]+.)(hosted|staging|cloud)\.(panopto.(com|eu))/) !== null) return 'Panopto';
    if (url.match(/twitter.com/) !== null) return 'Twitter';
    if (url.match(/((http|https):\/\/)(www.|)(instagram.com|instagr.am)/) !== null) return 'Instagram';
    if (url.match(/((http|https):\/\/)(www.|player.|)(vimeo.com)/) !== null) return 'Vimeo';
    if (url.match(/((http|https):\/\/)(www.|)((video214|animoto).com)/) !== null) return 'Animoto';
    if (url.match(/((http|https):\/\/)(www.|[a-zA-Z-0-9-]+.|)((slideshare.net|slidesha.re))/) !== null) return 'Slideshare';

    /* get host name */
    const anchor = document.createElement('a');
    anchor.href = url;
    return anchor.hostname;
  };
  const getYoutubeID = (url: string): string | undefined => {
    let youtubeID: string;

    if (url.indexOf('watch?v=') !== -1) {
      youtubeID = url.split('watch?v=')[1];
      const ampersandPosition = youtubeID.indexOf('&');

      if (ampersandPosition != -1) {
        youtubeID = youtubeID.substring(0, ampersandPosition);
      }
    } else {
      youtubeID = url.split('/')[1];
      const ampersandPosition = youtubeID.indexOf('&');

      if (ampersandPosition != -1) {
        youtubeID = youtubeID.substring(0, ampersandPosition);
      }
    }

    if (youtubeID.length === 0) return undefined;
    return youtubeID;
  };

  return {
    init,
  };
});
