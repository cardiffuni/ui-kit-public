define(['jquery', 'mapboxgl', 'turf', 'mapboxgl-supported'], ($: JQueryStatic, mapboxgl: any, turf: any): UIKit.Module.ContentMap => {
  let map: any = null;
  const config: UIKit.MapboxConfig = {
    accessToken: 'pk.eyJ1IjoiY2FyZGlmZnVuaSIsImEiOiJsSDQwWW9FIn0.3xwcUeOa2HzlF4Fl8EJ3Xg',
    mapStyle: 'mapbox://styles/cardiffuni/citkjwi6w008o2hmu0blha32l',
    $mapContainer: null,
    massMarkers: {
      type: 'FeatureCollection',
      features: [],
    },
    geoJsonLoadedcounter: 0,
    activeFilters: [],
    language: $('html').attr('lang') || 'en',
    popupActive: false,
    defaultCenter: [-103.59179687498357, 40.66995747013945],
    defaultZoom: 2,
    massClusterMaxZoom: 4,
    massClusterRadius: 50,
    browserError: 'This map cannot be displayed by your browser. Try upgrading your browser version or use a different browser.',
  };

  //init function to start everything off.
  //Adds a click event listener to .filter-map-layer which is the filter checkboxes.
  //Adds a click event listender to .collapse-box which collapses the box on mobile devices.
  //Disables map roation using right click + drag and touch rotation
  //calls loadMapData ocne the map has loaded.
  const init = (): void => {
    let $mapContainer = $('#content-map');
    mapboxgl.accessToken = config.accessToken;

    if (!mapboxgl.supported()) {
      $mapContainer.append(`<p class='alert'>${config.browserError}</p>`);
      $('#map-filters').remove();
    } else {
      map = new mapboxgl.Map({
        container: 'content-map',
        style: config.mapStyle,
        center: config.defaultCenter,
        zoom: config.defaultZoom,
        maxZoom: 8,
        maxBounds: [
          [-180, -85.05],
          [180, 85.05],
        ],
      });

      $(document).on('click', '.filter-map-layer', filterMapMarkers);
      $(document).on('click', '.collapse-box', collapseMenu);
      map.on('load', loadMapData);

      // disable map rotation using right click + drag
      // disable map rotation using touch rotation gesture
      map.dragRotate.disable();
      map.touchZoomRotate.disableRotation();
    }
  };

  //Map on load function.
  //Loads any geoJSON files from the feedConfig
  //Creates a hover and click popup.
  //Adds a mousemove listener to display properties.name on hover. This only fires on the mass-singles-layer. Also changes the mouse icon to a pointer when hovering over a highlight
  //Adds a on click listener to display properties.html when you click on a highlight layer.
  const loadMapData = (): void => {
    getSourceFeeds();

    let hoverPopup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });

    let clickPopup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });

    map.on('mousemove', function (e: mapboxgl.MapMouseEvent): void {
      if (map.getLayer('highlights-layer') !== undefined && map.getLayer('mass-singles-layer') !== undefined) {
        let hoverFeatures = map.queryRenderedFeatures(e.point, {
          layers: ['highlights-layer', 'mass-singles-layer'],
        });

        if (hoverFeatures.length) {
          if (hoverFeatures[0].layer.id === 'highlights-layer') {
            map.getCanvas().style.cursor = 'pointer';
          } else {
            map.getCanvas().style.cursor = '';
          }
        } else {
          hoverPopup.remove();
          map.getCanvas().style.cursor = '';
          return;
        }

        if (config.popupActive === false && hoverFeatures[0].properties.name !== undefined && hoverFeatures[0].layer.id !== 'highlights-layer') {
          hoverPopup.setLngLat(hoverFeatures[0].geometry.coordinates).setHTML(hoverFeatures[0].properties.name).addTo(map);
        }
      }
    });

    //Add an on click event that displays the popup when a marker on the highlights layer is clicked.
    map.on('click', function (e: mapboxgl.MapMouseEvent): void {
      if (map.getLayer('highlights-layer') !== undefined) {
        let features = map.queryRenderedFeatures(e.point, {
          layers: ['highlights-layer'],
        });

        if (!features.length) {
          clickPopup.remove();
          config.popupActive = false;
          return;
        }
        config.popupActive = true;
        hoverPopup.remove();

        clickPopup.setLngLat(features[0].geometry.coordinates).setHTML(features[0].properties.html).addTo(map);
      }
    });
  };

  //Function that is called for each geoJSON file in contentMapConfig.feed.
  //Grabs the file using $.getJSON and then adds the features object to config.massMarkers.features one at a time
  //Before adding the feature to the massMarkers object it checks if it needs to add the type property.
  //Once this function has been called for the final time (based on the length of contentMapConfig.feed and the value of config.geoJsonLoadedcounter) it calls addMassMarkersToMap()
  const addSourceFeed = (url: string, type: string): void => {
    $.getJSON(url).done(function (sourceData): void {
      for (let ii = 0; ii < sourceData.features.length; ii++) {
        const tempObj: mapboxgl.MapboxGeoJSONFeature = sourceData.features[ii];

        if (tempObj.properties === null) {
          tempObj.properties = { type: type };
        } else {
          tempObj.properties.type = type;
        }
        config.massMarkers.features.push(tempObj);
      }
      config.geoJsonLoadedcounter++;

      if (config.geoJsonLoadedcounter == contentMapConfig.feed.length) {
        addMassMarkersToMap();
      }
    });
  };

  //Function to add the mass-singles layer, mass-cluster-layer and mass-cluster-count-layer
  //Then calls addHighlightsToMap()
  const addMassMarkersToMap = (): void => {
    map.addSource('mass-feed', {
      type: 'geojson',
      data: config.massMarkers,
      cluster: true,
      clusterMaxZoom: config.massClusterMaxZoom,
      clusterRadius: config.massClusterRadius,
    });

    let massSinglesLayerStopsArray = [];
    for (let i = 0; i < contentMapConfig.categories.length; i++) {
      massSinglesLayerStopsArray.push([contentMapConfig.categories[i].category, contentMapConfig.categories[i].colour]);
    }

    map.addLayer({
      id: 'mass-singles-layer',
      source: 'mass-feed',
      type: 'circle',
      paint: {
        'circle-color': {
          property: 'type',
          type: 'categorical',
          stops: massSinglesLayerStopsArray,
        },
      },
    });

    map.addLayer({
      id: 'mass-cluster-layer',
      type: 'circle',
      source: 'mass-feed',
      filter: ['all', ['has', 'point_count']],
      paint: {
        'circle-color': '#aaaaaa',
        'circle-radius': 16,
      },
    });

    // Add a layer for the clusters' count labels
    map.addLayer({
      id: 'mass-cluster-count-layer',
      type: 'symbol',
      source: 'mass-feed',
      filter: ['all', ['has', 'point_count']],
      layout: {
        'text-field': '{point_count}',
        'text-font': ['Franklin Gothic FS Book', 'Arial Unicode MS Bold'],
        'text-size': 17,
        'text-offset': [0, 0.15],
      },
    });

    addHighlightsToMap();
  };

  //Adds the highlights to the map.
  //Creates the highlight bound box using turf.bbox
  //Fits the map to the bound box.
  const addHighlightsToMap = (): void => {
    let massSinglesLayerStopsArray = [];
    for (let i = 0; i < contentMapConfig.categories.length; i++) {
      massSinglesLayerStopsArray.push([contentMapConfig.categories[i].category, contentMapConfig.categories[i].colour]);
    }

    map.addSource('highlights-feed', {
      type: 'geojson',
      data: contentMapConfig.highlights,
    });

    //Add a layer for the highlights
    map.addLayer({
      id: 'highlights-layer',
      type: 'symbol',
      source: 'highlights-feed',
      layout: {
        'text-field': '{label}',
        'icon-image': '{marker}',
        'icon-size': 1.1,
        'icon-allow-overlap': true,
        'icon-ignore-placement': true,
        'text-size': 18,
        'text-font': ['Franklin Gothic FS Book', 'Arial Unicode MS Bold'],
        'text-offset': [0, 0.15],
      },
      paint: {
        'text-color': '#ffffff',
      },
    });

    let highlightsBoundBox = turf.bbox(contentMapConfig.highlights);
    map.fitBounds(highlightsBoundBox, { padding: 40 });
  };

  //Loads any geoJSON files from the feedConfig
  //Calls addSourceFeed() for each geoJSON file.
  //Calls buildFiltersMenu to build the filters box menu items.
  const getSourceFeeds = (): void => {
    if (contentMapConfig.feed.length === 0) {
      addHighlightsToMap();
    } else {
      for (let i = 0; i < contentMapConfig.feed.length; i++) {
        addSourceFeed(contentMapConfig.feed[i].url, contentMapConfig.feed[i].type);
      }
    }
    buildFiltersMenu();
  };

  //Builds the filters box menu items based on what is in feedConfig and uses the colour,name etc from contentMapConfig.categories
  const buildFiltersMenu = (): void => {
    let filterBox = $('#map-filters .box-body .nav');
    for (let i = 0; i < contentMapConfig.categories.length; i++) {
      const items = contentMapConfig.feed.filter((data: any): boolean => {
        return data.type === contentMapConfig.categories[i].category;
      });
      if (items.length || contentMapConfig.highlightsCount[contentMapConfig.categories[i].category] > 0) {
        filterBox.append(`
        <li class="nav-item"><label class="nav-link px-0">
          <input type="checkbox" value="" data-filter="${contentMapConfig.categories[i].category}" checked class="filter-map-layer">
          ${contentMapConfig.categories[i].label[config.language]}
          <span style="width: 15px;height: 15px;background-color: ${contentMapConfig.categories[i].colour}; float:right;"></span>
        </label></li>`);
      }
    }
  };

  //On click event for the filter checkboxes.
  //Resets the config.activeFilters to an empty array.
  //Loops through each of the filter checkboxes and adds any checked ones to the config.activeFilters array.
  //Filters the highlights and massMarkers geoJSON based on the config.activeFilers array and then calls setData() to replace the source and re-render the map.
  const filterMapMarkers = (): void => {
    config.activeFilters = [];

    const filterInputs: JQuery<HTMLInputElement> = $('.filter-map-layer');

    filterInputs.each(function (this): void {
      if (this.checked) {
        config.activeFilters.push($(this).data('filter'));
      }
    });

    if (config.activeFilters.length === 0) {
      map.scrollZoom.disable();
    } else {
      map.scrollZoom.enable();
    }

    if (map.getLayer('highlights-layer') !== undefined) {
      let filteredHighlighMarkers = contentMapConfig.highlights.features.filter(filterMarkers);
      let filteredHighlighMarkersGeoJSON = {
        type: 'FeatureCollection',
        features: [],
      };
      filteredHighlighMarkersGeoJSON.features = filteredHighlighMarkers;
      map.getSource('highlights-feed').setData(filteredHighlighMarkersGeoJSON);
    }

    if (map.getLayer('mass-singles-layer') !== undefined) {
      let filteredMaskMarkers = config.massMarkers.features.filter(filterMarkers);
      let filteredMarkersGeoJSON: UIKit.MapboxFeatures = { type: 'FeatureCollection', features: [] };
      filteredMarkersGeoJSON.features = filteredMaskMarkers;
      map.getSource('mass-feed').setData(filteredMarkersGeoJSON);
    }
  };

  // Used to filter the massMarkers geoJSON features based on the config.activeFilters array. This array is set by the filterMapMarkers function.
  const filterMarkers = (obj: mapboxgl.MapboxGeoJSONFeature): mapboxgl.MapboxGeoJSONFeature | void => {
    if (obj.properties !== null) {
      if ($.inArray(obj.properties.type, config.activeFilters) >= 0) {
        return obj;
      }
    }
  };

  const collapseMenu = (e: JQuery.ClickEvent): void => {
    e.preventDefault();
    $('#map-filters').toggleClass('collapsed');
    if ($('#map-filters').hasClass('collapsed')) {
      $('#map-filters .collapse-box [class*=icon-]').addClass('icon-expand').removeClass('icon-contract');
    } else {
      $('#map-filters .collapse-box [class*=icon-]').removeClass('icon-expand').addClass('icon-contract');
    }
  };
  return {
    init,
    loadMapData,
    addSourceFeed,
    addMassMarkersToMap,
    addHighlightsToMap,
    getSourceFeeds,
    buildFiltersMenu,
    filterMapMarkers,
    filterMarkers,
    collapseMenu,
  };
});
