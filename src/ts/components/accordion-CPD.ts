define(['jquery'], ($: JQueryStatic): UIKit.Module.AccordionCPD => {
  const init = (): void => {
    const numOfAccordionCPD: number = $('.accordion-cpd-course').children('.accordion-group').length;

    if (numOfAccordionCPD > 1) {
      $('.accordion-cpd-course')
        .children('.accordion-group')
        .each(function (index: number): void {
          if (index > 0) {
            $(this).children('.accordion-body').removeClass('in');
          }
        });
    }
  };

  return {
    init,
  };
});
