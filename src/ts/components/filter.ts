define(['components/card-controls.min'], (controls: UIKit.Module.Controls): UIKit.Module.Filter => {
  const filter = document.querySelector<HTMLElement>('.filter');
  const filterListSelector = filter?.dataset.list;

  let filterList: null | HTMLElement = null;

  if (filterListSelector !== undefined) {
    filterList = document.querySelector(filterListSelector);
  }

  /**
   * Open all filter panes with filterList
   */
  const openAll = (): void => {
    if (filterList !== null) {
      const panes = filterList.querySelectorAll('.filter-pane');

      panes.forEach((item): void => {
        item.classList.remove('d-none', 'card--first', 'card--last');
        item.classList.add('d-flex');
      });
    }
    controls.checkForControls();
  };

  /**
   * Open all filter panes when the target matches class list
   *
   * @param {string} element Target element
   */
  const open = (element: string): void => {
    if (filterList !== null) {
      const panes = filterList.querySelectorAll('.filter-pane');
      panes.forEach((item): void => {
        item.classList.remove('d-flex', 'card--first', 'card--last');
        item.classList.add('d-none');
      });
    }
    const currentSelection = document.querySelectorAll(element);

    if (currentSelection !== null) {
      currentSelection.forEach((item): void => {
        item.classList.remove('d-none');
        item.classList.add('d-flex');
      });
    }

    /* Check whether we still need to show the cards */
    controls.checkForControls();

    const totalCards = filterList?.querySelectorAll('.filter-pane.d-flex');

    if (totalCards === null || totalCards === undefined || totalCards.length === 0) return;

    /* Add first and last classes to fix padding offsets */
    totalCards[0].classList.add('card--first');
    totalCards[totalCards.length - 1].classList.add('card--last');
  };

  /**
   * Initialiser for filter
   */
  const init = (): void => {
    if (filter !== null) {
      const filterItems = document.querySelectorAll<HTMLElement>('.filter__items .btn');
      if (filterItems !== null) {
        filterItems.forEach((item): void => {
          item.addEventListener('click', (evt): void => {
            evt.preventDefault();

            /* Select active filter */
            selectFilter(item);

            const target = item.getAttribute('data-target');

            if (target === 'all') {
              openAll();
            } else if (target !== null) {
              open(target);
            }

            /* Scroll cards into view */
            let cardItemsDiv: HTMLElement | null;
            if (target === 'all') {
              cardItemsDiv = document.querySelector<HTMLElement>('.cards__items');
            } else {
              cardItemsDiv = document.querySelector(target as string)?.parentElement || null;
            }

            if (cardItemsDiv !== null) {
              cardItemsDiv.scrollTo({
                behavior: 'smooth',
                left: 0,
              });
            }
          });
        });
      }
    }
  };

  /**
   * Set aria-selected to true and active class
   * Set other filter with the same target to false.
   *
   * @param {HTMLElement} elm HTML element to apply the change too
   */
  const selectFilter = (elm: HTMLElement): void => {
    const filterGroup = elm.dataset.group;

    /* remove active on other filter items */
    if (filterGroup !== undefined) {
      const filterGroupItems = document.querySelectorAll(`${filterGroup} .btn`);

      if (filterGroupItems !== null) {
        filterGroupItems.forEach((item): void => {
          item.classList.remove('active');
          item.setAttribute('aria-selected', 'false');
        });
      }
    }

    elm.classList.add('active');
    elm.setAttribute('aria-selected', `true`);
  };

  return {
    init,
    selectFilter,
  };
});
