define(['jquery'], ($: JQueryStatic): UIKit.Module.SearchGlobal => {
  /**
   * Initalises all search global.
   *
   * @requires bootstrap/collapse
   */
  const init = (): void => {
    /**
     * Calcuate the offset of moble nav when the search box is open
     */
    $(document).on('hide.bs.collapse', '.search-global', function (this: HTMLElement): void {
      $('body').removeClass('search-open');
      const offset = ($('.nav-global').outerHeight(true) || 0);
      $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
    });

    /**
     * Calcuate the offset of moble nav when the search box is closed
     */
    $(document).on('show.bs.collapse', '.search-global', function (this: HTMLElement): void {
      $('body').addClass('search-open');
      const offset =
        ($('.nav-global').outerHeight(true) || 0) + ($('.search-global').outerHeight(true) || 0);
      $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
    });

    /**
     * Un-focus the input box when the accordion is closed.
     */
    $(document).on('hidden.bs.collapse', '.search-global', function (this: HTMLElement): void {
      $('#query').get(0).blur();
    });

    /**
     * Focus the input box when the accordion is opened.
     */
    $(document).on('shown.bs.collapse', '.search-global', function (this: HTMLElement): void {
      $('.search-global .search-form-query')[0].focus();
    });

    /**
     * Adds or removes a class when a search box is not empty
     */
    $(document).on('keyup input', '.search-form-query', function (this: HTMLInputElement): void {
      if (this.value.length > 0) {
        $(this).closest('form').addClass('has-input');
      } else {
        $(this).closest('form').removeClass('has-input');
      }
    });

    /**
     * Clears the input
     */
    $(document).on('click', '.search-form-clear', function (evt: JQuery.ClickEvent): void {
      evt.preventDefault();
      $(this).closest('form').find('.search-form-query').val('');
      $(this).closest('form').removeClass('has-input');
      $(this).closest('form').find('.search-form-query')[0].focus();
    });
  };

  return {
    init,
  };
});
