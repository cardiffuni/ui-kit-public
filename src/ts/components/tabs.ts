define(['jquery'], ($: JQueryStatic): UIKit.Module.Tab => {
  const init = (): void => {
    //if there are any tab panes in page
    if ($('.tab-pane.squiz-bodycopy').length) {
      //group tab panes that are next to each other § wrap
      $('.tab-pane.squiz-bodycopy')
        .filter(function (): boolean {
          return !$(this).prev().is('.tab-pane.squiz-bodycopy');
        })
        .map(function (this, index): JQuery<HTMLElement> {
          return $(this).nextUntil(':not(.tab-pane.squiz-bodycopy)').addBack();
        })
        .wrap('<div class="tab-content squiz-tabs" />');

      //loop through grouped tab panes
      $('.tab-content.squiz-tabs').each(function (tabIndex): void {
        //store tabs html
        let tabsHtml = '';

        //loop through tab panes in this group
        $(this)
          .find('.tab-pane.squiz-bodycopy')
          .each(function (index): void {
            $(this)
              .attr('role', 'tabpanel')
              .attr('aria-labelledby', `tab-${tabIndex}`)
              .attr('id', `tab-${$(this).attr('id')}`);

            if (index === 0) {
              $(this).addClass('active');
            }

            let descString = $(this).attr('desc') || '';
            tabsHtml += `
              <li class="nav-item">
                <a href="#${this.id}" class="nav-link${index === 0 ? ' active' : ''}" role="tab" data-toggle="tab" id="tab-${index}"
                  aria-controls="${this.id}" aria-selected="${index === 0 ? true : false}">
                  ${extractLanguageSubstring(descString)}
                </a>
              </li>`;
          });
        //build tabs navigation
        tabsHtml = `<ul class="nav nav-tabs">${tabsHtml}</ul>`;
        //put tabs before this group
        $(this).before(tabsHtml);
      });
    } // end check for .tab-pane
  };

  // this is to extract a value from a special string based on the current language
  // expected string format is en=yyy;cy=xxx;cn=zzz
  // also copes if semicolon is on the end
  // gets current language from the lang=en attribute on html tag
  const extractLanguageSubstring = (inputString: string): void => {
    // prepare a string to return
    let outputString;

    // set default language to English
    let languageCode = $('html').attr('lang') || '';

    //check whether this is a normal string or a semi-colon separated multilingual string
    let isMultilingual = false;
    if (inputString.indexOf('|') > -1) {
      isMultilingual = true;
    }

    // if special multilingual string, process that bad boy
    // otherwise return the boring normal one
    if (isMultilingual === true) {
      // remove trailing pipe characters
      let trailingSemicolon = /\|$/;
      inputString = inputString.replace(trailingSemicolon, '');

      // split languages
      let arrLanguages = inputString.split('|');

      // put the languages into an object
      let languageStrings: any = {};
      arrLanguages.forEach(function (languageItem): void {
        let arrLanguageItem = languageItem.split('=');
        languageStrings[arrLanguageItem[0]] = arrLanguageItem[1];
      });

      //console.dir(languageStrings);

      // return the appropriate string for the current language
      outputString = languageStrings[languageCode];
    } else {
      // return it, just normal
      outputString = inputString;
    }

    return outputString;
  };

  /**
   * Initialise all tabs with sliders. It will also listen for when new tabs
   * are activated or when to view port width changes to adjust the indicator width.
   */
  const initSliders = (): void => {
    let throttledResize = false;
    // TODO: Change name
    $('.tabs--with-slider').each((_, item): void => {
      $(item).append(`<div class="tabs__slider"><span class="tabs__slider__indicator" aria-hidden="true"></span></div>`);

      const slider = $(item).find('.tabs__slider__indicator');
      const activeTab = $(item).find('.active');

      /* Update with offset/ width */
      updateSliderWidth(activeTab, slider);

      /* When a new tab item is shown/ clicked */
      $(item)
        .find('.nav-link')
        .on('show.bs.tab', (evt): void => {
          updateSliderWidth($(evt.currentTarget), slider);
        });
    });

    /* When window is resized, update the slider width */
    window.addEventListener('resize', (): void => {
      if (throttledResize) return;

      window.requestAnimationFrame((): void => {
        $('.tabs--with-slider').each((_, item): void => {
          const slider = $(item).find('.tabs__slider__indicator');
          const activeTab = $(item).find('.active');

          /* Update with offset/ width */
          updateSliderWidth(activeTab, slider);
        });
        throttledResize = false;
      });

      throttledResize = true;
    });
  };

  /**
   * Sets the slider width and left offset based on the current active element.
   * @param {HTMLElement} element Current active tab element
   * @param {HTMLElement} slider the slider indicator HTMLElement
   */
  const updateSliderWidth = (element: JQuery<HTMLElement>, slider: JQuery<HTMLElement>): void => {
    if (element !== null) {
      $(slider).css({
        width: element.outerWidth() || 0,
        left: element[0].offsetLeft || 0,
      });
    }
  };

  return {
    init,
    extractLanguageSubstring,
    initSliders,
  };
});
