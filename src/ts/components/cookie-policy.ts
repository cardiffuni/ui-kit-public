define(['jquery', 'i18n!locale/globals.min'], ($: JQueryStatic, i18n: UIKit.I18n): UIKit.Module.CookiePolicy => {
  let cookiePreferences: UIKit.CookiePreferences = {
    preferences: {
      essential: false,
      performance: false,
      advertising: false,
      functionality: false,
    },
    get essential(): boolean {
      return this.preferences.essential;
    },
    set essential(val: boolean) {
      this.preferences.essential = val;

      if (val) {
        removeCookieBanner();
      }
    },
    get performance(): boolean {
      return this.preferences.performance;
    },
    set performance(val: boolean) {
      this.preferences.performance = val;
    },
    get advertising(): boolean {
      return this.preferences.advertising;
    },
    set advertising(val: boolean) {
      this.preferences.advertising = val;
    },
    get functionality(): boolean {
      return this.preferences.functionality;
    },
    set functionality(val: boolean) {
      this.preferences.functionality = val;
    },
  };

  // create a set of configuration letiables
  const config = {
    cookieName: 'cu-cookie-preferences',
    cookieDayDuration: 365,
  };

  const init = (cookieValue: 'true' | undefined): void => {
    /* Global variable set within pages where cookie banner should not be shown */
    const showBanner = typeof disableCookieBanner !== 'undefined' ? disableCookieBanner : false;

    if (!showBanner) {
      loadCookiePreferences(cookieValue);
      if (cookiePreferences.preferences.essential === false) {
        /* If no cookie call add the alert */
        renderCookieBanner();

        /* Calculate offset for navigation */
        computeOffsets();
      }

      $('#accept-cookie-message').on('click', function (e): void {
        e.preventDefault();
        setPreferences({
          essential: true,
          performance: true,
          advertising: true,
          functionality: true,
        });

        removeCookieBanner();
      });
    }

    setUpdateListener();
  };

  const setUpdateListener = (): void => {
    $(document).on('click', '.cookie-update-preferences', (evt): void => {
      evt.preventDefault();
      const performance = $(evt.target).attr('data-cookie-preference-performance');
      const advertising = $(evt.target).attr('data-cookie-preference-advertising');
      const functionality = $(evt.target).attr('data-cookie-preference-functionality');

      if (!cookiePreferences.essential) {
        cookiePreferences.essential = true;
      }

      if (performance === 'true' || performance === 'false') {
        cookiePreferences.performance = performance === 'true';
      }

      if (advertising === 'true' || advertising === 'false') {
        cookiePreferences.advertising = advertising === 'true';
      }

      if (functionality === 'true' || functionality === 'false') {
        cookiePreferences.functionality = functionality === 'true';
      }
      setPreferences(cookiePreferences.preferences);
    });
  };

  /**
   * Loads cookie from cookie object or from string
   *
   * @param cookieValue Optional if cookieValue has already been recevied, then this avoids the cookie call
   */
  const loadCookiePreferences = (cookieValue?: string): void => {
    if (cookieValue === undefined) {
      let matches = document.cookie.match(new RegExp('(?:^|; )' + config.cookieName.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));
      cookieValue = matches ? decodeURIComponent(matches[1]) : undefined;
    }

    if (cookieValue !== undefined) {
      const preferences = JSON.parse(cookieValue);

      cookiePreferences.preferences.essential = true;
      cookiePreferences.preferences.performance = preferences.performance;
      cookiePreferences.preferences.advertising = preferences.advertising;
      cookiePreferences.preferences.functionality = preferences.functionality;
    }
  };

  const getCookiePreferences = (): UIKit.CookiePreferencesUpdate => {
    loadCookiePreferences();

    return cookiePreferences;
  };

  /** Adds the template */
  const renderCookieBanner = (): void => {
    const template = `
    <div id="cu-cookie-alert" class="cookie-banner" role="alertdialog" aria-labelledby="cookie-banner-title" aria-describedby="cookie-banner-body">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-xl-6">
            <h2 class="sr-only text-white" id="cookie-banner-title">Cookie notice</h2>
            <p id="cookie-banner-body">${i18n.cookieBanner.conent}</p>
          </div>
          <div class="col-lg-7 col-xl-6 text-lg-right">
            <a href="#" class="btn btn-success" id="accept-cookie-message">${i18n.cookieBanner.acceptAll}</a>
            <a href="${i18n.cookieBanner.url}" class="btn btn-success">${i18n.cookieBanner.customise}</a>
          </div>
        </div>
      </div>
    </div>`;

    $('body').addClass('with-cookie-banner');
    $('.nav-secondary').before(template);
  };

  /**
   * Closes the menu and sets the cookie
   */
  const removeCookieBanner = (): void => {
    $('body').removeClass('with-cookie-banner');
    $('#cu-cookie-alert').remove();

    $('#search-global').css('top', 0);
    const offset = ($('.nav-global').outerHeight(true) || 0) + ($('.search-global.show').outerHeight(true) || 0);

    $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
  };

  const setPreferences = (preferences: UIKit.CookiePreferencesUpdate): void => {
    setCookie(config.cookieName, JSON.stringify(preferences), config.cookieDayDuration);

    /* Add an event for intal JS to listen too */
    const cookieChangeEvent = new CustomEvent('cookie-preferences-updated');

    cookieChangeEvent.initCustomEvent('cookie-preferences-updated', true, true, preferences);
    document.dispatchEvent(cookieChangeEvent);

    /* Sets an empty dataLayer if it's unset */
    (window as any).dataLayer = (window as any).dataLayer || [];

    /* Update cookie prefs object */
    cookiePreferences.preferences.essential = true;
    cookiePreferences.preferences.performance = preferences.performance;
    cookiePreferences.preferences.advertising = preferences.advertising;
    cookiePreferences.preferences.functionality = preferences.functionality;

    /* Push event to GTM */
    (window as any).dataLayer.push({
      event: 'set-cu-cookie-preferences', // name of event seems to be required
      'cu-cookie-performance': preferences.performance, // based on cookie value: true/false
      'cu-cookie-functionality': preferences.functionality, // based on cookie value: true/false
      'cu-cookie-advertising': preferences.advertising, // based on cookie value: true/false
    });
  };
  /**
   * Sets a cookie
   *
   * @param name Cookie name
   * @param value Cookie value
   * @param exdays Set th expiry for cookie
   */
  const setCookie = (name: string, value: string, exdays: number): void => {
    let d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = 'expires=' + d.toUTCString();
    document.cookie = name + '=' + value + '; ' + expires + ';path=/';
  };

  /**
   * Compted offsets for mobile navigation
   */
  const computeOffsets = (): void => {
    let offset =
      ($('.nav-global').outerHeight(true) || 0) + ($('.search-global.show').outerHeight(true) || 0);
    $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);

    $(window).on('resize', (): void => {
      if ($('#accept-cookie-message').length > 0) {
        offset =
          ($('.nav-global').outerHeight(true) || 0) +
          ($('.search-global.show').outerHeight(true) || 0);
        $('.nav-global-mobile').css('height', `calc(100% - ${offset}px)`);
      }
    });
  };

  const cookiePreferenceForm = (): void => {
    let hasErrors = {
      performance: false,
      advertising: false,
      functionality: false,
    };

    $('#update-cookie-preferences-form').on('submit', (e): void => {
      e.preventDefault();
      $('#form-updated-alert').addClass('d-none');

      /* Preferences */
      const rawPerformance = $('input[name="cookie-policy-performance"]:checked').val();
      const rawAdvertising = $('input[name="cookie-policy-advertising"]:checked').val();
      const rawfunctionality = $('input[name="cookie-policy-functionality"]:checked').val();

      /* check for errors */
      if (rawPerformance === undefined) {
        hasErrors.performance = true;
        $('#cookie-policy-performance-error').html(i18n.forms.required).removeClass('d-none');
      } else if (rawPerformance !== 'true' && rawPerformance !== 'false') {
        hasErrors.performance = true;
        $('#cookie-policy-performance-error').html(i18n.forms.invalidValue).removeClass('d-none');
      } else {
        hasErrors.performance = false;
        $('#cookie-policy-performance-error').addClass('d-none');
      }

      if (rawAdvertising === undefined) {
        hasErrors.advertising = true;
        $('#cookie-policy-advertising-error').html(i18n.forms.required).removeClass('d-none');
      } else if (rawAdvertising !== 'true' && rawAdvertising !== 'false') {
        hasErrors.advertising = true;
        $('#cookie-policy-advertising-error').html(i18n.forms.invalidValue).removeClass('d-none');
      } else {
        hasErrors.advertising = false;
        $('#cookie-policy-advertising-error').addClass('d-none');
      }

      if (rawfunctionality === undefined) {
        hasErrors.functionality = true;
        $('#cookie-policy-functionality-error').html(i18n.forms.required).removeClass('d-none');
      } else if (rawfunctionality !== 'true' && rawfunctionality !== 'false') {
        hasErrors.functionality = true;
        $('#cookie-policy-functionality-error').html(i18n.forms.invalidValue).removeClass('d-none');
      } else {
        hasErrors.functionality = false;
        $('#cookie-policy-functionality-error').addClass('d-none');
      }

      if (hasErrors.performance || hasErrors.advertising || hasErrors.functionality) {
        $('.question-error-msg:not(.d-none)').closest('fieldset')[0].scrollIntoView();
        $('#prefernces-updated-alert').remove();
      } else {
        setPreferences({
          essential: true,
          performance: rawPerformance === 'true',
          advertising: rawAdvertising === 'true',
          functionality: rawfunctionality === 'true',
        });

        $('#form-updated-alert').removeClass('d-none');
        $('#form-updated-alert')[0].scrollIntoView(true);
      }
    });
  };

  return {
    init,
    renderCookieBanner,
    removeCookieBanner,
    setCookie,
    cookiePreferenceForm,
    getCookiePreferences,
    setUpdateListener,
  };
});
