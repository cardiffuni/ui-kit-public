define(['jquery'], ($: JQueryStatic): UIKit.Module.TabAccordion => {
  const init = (): void => {
    accordionToTab();
    tabToAccordion();
  };

  const accordionToTab = (): void => {
    let accordionMobile = '.accordion-mobile .tab-content .tab-pane .accordion-group ';
    $(accordionMobile + '.accordion-heading [data-toggle="collapse"]').click(function (): void {
      if ($(accordionMobile + '.accordion-body').hasClass('in')) {
        $(accordionMobile + '.accordion-heading .accordion-toggle').addClass('collapsed');
      }
    });
    let toggle = $('.accordion-mobile .tab-content .tab-pane .accordion-group .accordion-toggle');
    toggle.click(function (): void {
      let activeTarget = $(this).parent().parent().parent().attr('id');
      let link = $("a[href='" + '#' + activeTarget + "']");
      let navList = $('.tabs-vertical .nav-list').children();
      navList.each(function (): void {
        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
        }
      });
      $(link).parent().addClass('active');
    });
  };
  const tabToAccordion = (): void => {
    let truthFlag = false;
    let body = $('.accordion-mobile .tab-content .tab-pane .accordion-group .accordion-body');
    truthFlag = false;
    $(window).resize(function (): void {
      let windowWidth = $(window).width() || 0;

      if (windowWidth < 768 && truthFlag == false) {
        let navActive = $('.tabs-vertical .nav-list li.active');
        let active = navActive.children().attr('href');
        truthFlag = true;
        let accordionToOpen = $(active + ' .accordion-group .accordion-body');
        let accordionToggleToOpen = $(active + ' .accordion-group .accordion-toggle');
        body.each(function (): void {
          if ($(this).hasClass('in')) {
            $(this).removeClass('in');
            $(this).css('height', '0');
            $(this).prev().children().addClass('collapsed');
          }
        });
        accordionToOpen.addClass('in');
        accordionToOpen.css('height', 'auto');
        accordionToggleToOpen.removeClass('collapsed');
      }
      if (windowWidth > 767 && truthFlag == true) {
        truthFlag = false;
      }
    });
  };
  return {
    init,
    accordionToTab,
    tabToAccordion,
  };
});
