define(['jquery'], ($: JQueryStatic): UIKit.Module.Postgraduate => {
  const config: any = {
    globalData: {},
    modeOptionsSet: [],
    devApiUrl: 'https://5be77502-cc7e-4c5a-a2e3-123cbfb2ab9e.mock.pstmn.io/study/feeds/pgr-application-endpoint?codes=',
    apiUrl: 'https://www.cardiff.ac.uk/study/feeds/pgr-application-endpoint?codes=',
    schools: {
      ARCHI: {
        name: 'Architecture',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/welsh-school-of-architecture',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/welsh-school-of-architecture',
      },
      BIOSI: {
        name: 'Biosciences',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-biosciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-biosciences',
      },
      CARBS: {
        name: 'Business',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/cardiff-business-school',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/cardiff-business-school',
      },
      CHEMY: {
        name: 'Chemistry',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-chemistry',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-chemistry',
      },
      COMSC: {
        name: 'Computer Science and Informatics',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-computer-science-and-informatics',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-computer-science-and-informatics',
      },
      DENTL: {
        name: 'Dentistry',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-dentistry',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-dentistry',
      },
      EARTH: {
        name: 'Earth and Ocean Sciences',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-earth-and-ocean-sciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-earth-and-ocean-sciences',
      },
      ENGIN: {
        name: 'Engineering',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-engineering',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-engineering',
      },
      ENCAP: {
        name: 'English Communication and Philosophy',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-english-communication-and-philosophy',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-english,-communication-and-philosophy',
      },
      CPLAN: {
        name: 'Geography and Planning',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-geography-and-planning',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-geography-and-planning',
      },
      HCARE: {
        name: 'Healthcare Sciences',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-healthcare-sciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-healthcare-sciences',
      },
      SHARE: {
        name: 'History Archaeology and Religion',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-history-archaeology-and-religion',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-history,-archaeology-and-religion',
      },
      JOMEC: {
        name: 'Journalism Media and Cultural Studies',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-journalism-media-and-cultural-studies',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-journalism,-media-and-cultural-studies',
      },
      LAWPL: {
        name: 'Law Politics and International Relations',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/cardiff-law-school',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-law-and-politics',
      },
      MATHS: {
        name: 'Mathematics',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-mathematics',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-mathematics',
      },
      MEDIC: {
        name: 'Medicine',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-medicine',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-medicine',
      },
      MLANG: {
        name: 'Modern Languages',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-modern-languages',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-modern-languages',
      },
      MUSIC: {
        name: 'Music',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-music',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-music',
      },
      OPTOM: {
        name: 'Optometry and Vision Sciences',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-optometry-and-vision-sciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-optometry-and-vision-sciences',
      },
      PHRMY: {
        name: 'Pharmacy and Pharmaceutical Sciences',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-pharmacy-and-pharmaceutical-sciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-pharmacy-and-pharmaceutical-sciences',
      },
      PHYSX: {
        name: 'Physics and Astronomy',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/cardiff-school-of-physics-and-astronomy',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-physics-and-astronomy',
      },
      PGMDE: {
        name: 'Postgraduate Medical and Dental Education',
        'url-research':
          'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-postgraduate-medical-and-dental-education',
        'url-taught':
          'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-postgraduate-medical-and-dental-education',
      },
      PSYCH: {
        name: 'Psychology',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-psychology',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-psychology',
      },
      SOCSI: {
        name: 'Social Sciences',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-social-sciences',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-social-sciences',
      },
      WELSH: {
        name: 'Welsh',
        'url-research': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/browse-by-school/school-of-welsh',
        'url-taught': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/browse-by-school/school-of-welsh',
      },
    },
  };

  const init = (): void => {
    createForm(); // set the apply box
    /*  Event hooks for whenever a drop-down box is selected for the apply
     * box. This will then call the update_apply_form function to change
     * the apply box information.
     */
    $('#drop-down-qualification, #drop-down-mode, #drop-down-start').change(function (): void {
      // get the data that is currently selected
      const qual = document.getElementById('drop-down-qualification') as HTMLInputElement;
      const mode = document.getElementById('drop-down-mode') as HTMLInputElement;
      const date = document.getElementById('drop-down-start') as HTMLInputElement;

      let info = {
        qual: qual.value,
        mode: mode.value,
        date: date.value,
      };
      // update the apply form
      updateApplyForm(info);
    });
  };

  const initRelatedCourses = (): void => {
    updateUrls();
  };

  const initRelatedSubjects = (): void => {
    let lang = $('html').attr('lang') || 'en';
    let defaultLink = $('#related-subject-default');
    // set up default link based on language
    defaultLink.attr(
      'href',
      lang == 'en'
        ? 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/search'
        : 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/search'
    );
    let subjectStr = defaultLink.attr('data-courses') || '';
    let subjectsArray = subjectStr.split(';');
    let container = defaultLink.parent().parent();
    let searchLink;
    // define the search link based on language
    if (lang == 'en') {
      searchLink =
        'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/search?collection=pgr-courses&form=search-en&gscope1=1&f.subjects%7Cs=';
    } else {
      searchLink =
        'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/search?collection=pgr-courses&form=search-cy&gscope1=2&f.subjects%7Cs=';
    }

    // if subject_array is not empty
    if (subjectsArray.length > 0 && subjectsArray[0] != '') {
      // loop through each subject and create a link
      for (let s in subjectsArray) {
        let subject = subjectsArray[s].trim();
        container.append(
          `<li><a href="${searchLink}${subject}">${capitaliseFirstLetter(
            subject
          )}<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron right</title><path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)" /></svg></a></li>`
        );
      }
      // remove default link
      defaultLink.remove();
      return;
    }
    return;
  };

  /* function modeCheck
   * @param1: key to add to array
   * @param2: Array that key will be added if unique
   * Return Array with added value
   * function provies a way to add keys to an array
   * and ensure that each one added is unique
   */
  const modeCheck = (key: string, array: any[]): any[] => {
    // check to see if key is in array
    // if -1 (not in array) then add to array
    if (array.indexOf(key) == -1) {
      array.push(key);
    }
    return array;
  };

  /* Fucntion createForm(Void)
   * return void
   * Function that sets up the page for the apply component.
   * Makes a call to the API and saves all data to be used
   * later on if user changes options in drop-down
   */
  const createForm = (): void => {
    // letants

    // THIS IS A TEST VALUE
    // let jsonTest = config.dummyData;
    // THIS IS END OF TEST VALUE
    let JSON_KEY_DATA = 'data';
    let JSON_KEY_QUAL = 'qualifications';
    const codes = $('#postgrad-apply-form').attr('data-codes') || '';
    let courseCodes = codes.replace(/\;\s/g, ';');

    /* Uncomment when testing is complete */
    // Ajax call for JSON data
    $.ajax({
      url: config.apiUrl + courseCodes,
      dataType: 'json',
      // data: data,
      success: (json: any): void => {
        //     connection was successful
        if (json['meta']['errorStatus']) {
          //       if the API goes down
          let lang = $('html').attr('lang') || 'en';
          $('#postgrad-apply-form > fieldset').hide(); // hide the application form
          let innerHTML = '<!--' + json['meta']['errorMessage'] + '-->'; // add error comment to page for debug
          innerHTML += lang == 'en' ? json['meta']['messageEn'] : json['meta']['messageCy']; // add correct language message
          $('#postgrad-apply-form').html(innerHTML); // add to page
        } else {
          // loop through the relevent JSON
          // This is adding the default options to the form - THIS IS REQUIRED
          let defaultData = config.globalData;

          defaultData['Default'] = {
            code: 'Default',
            qualName: 'Choose your qualification',
            'Default-Time': { nullDate: { startDate: 'Choose a start date', applyLink: null } },
          };
          config.modeOptionsSet = modeCheck('Default-Time', config.modeOptionsSet);
          // End adding default options to the form - THIS IS REQUIRED

          for (let j in json[JSON_KEY_DATA][JSON_KEY_QUAL]) {
            let jsonObj = json[JSON_KEY_DATA][JSON_KEY_QUAL][j]; // get one object to process

            // if the object is not known, then create and add it to global data
            if (!config.globalData.hasOwnProperty(jsonObj['code'])) {
              config.globalData[jsonObj['code']] = {};
              config.globalData[jsonObj['code']].code = jsonObj['code'];
              config.globalData[jsonObj['code']].qualName = jsonObj['name'];
            }
            // add mode to Set object.
            // Set used to ensure that all possible modes have been accounted for
            // which helps with indexing data inside the object.
            config.modeOptionsSet = modeCheck(jsonObj['mode'], config.modeOptionsSet);

            // process the date and link in getEntrypoints()
            if (!config.globalData[jsonObj['code']].hasOwnProperty(jsonObj['mode'])) {
              // if the mode does not exist, then create it
              config.globalData[jsonObj['code']][jsonObj['mode']] = getEntrypoints(jsonObj['entryPoints']);
            } else {
              // if it does the merge what already exists with the new data
              $.extend(config.globalData[jsonObj['code']][jsonObj['mode']], getEntrypoints(jsonObj['entryPoints']));
            }
          }

          // THIS IS REQUIRED
          let globalDataKeys = Object.keys(config.globalData);
          for (let i = 0; i < globalDataKeys.length; i++) {
            if (globalDataKeys[i] !== 'Default') {
              defaultData[globalDataKeys[i]]['Default-Time'] = { 'Default-Time': { startDate: 'Choose a start date', applyLink: null } };
            }
          }

          // END OF THIS IS REQUIRED

          // using the data that was saved.
          // build the apply box drop-downs
          setApplyForm(true, null, null);
        }
      },
    });
  };

  /* Function setApplyForm
   * @param1: boolOInit - (boolean) Used to inform the algorithm that this part of initialisation
   * @param2: selectedMode - (String or NULL) Gives currently selected mode from apply component. NULL during initialisation
   * @param3: selectedMode - (String or NULL) Gives currently selected date from apply component. NULL during initialisation
   * Return void
   * Function used to generate the correct information for the apply component's drop-down boxes.
   * If given selectedMode and selectedDate as strings, then automatically changes drop-downs to reflect correct information
   */
  const setApplyForm = (boolIni: boolean, selectedMode: string | null, selectedDate: string | null): void => {
    let keys;
    if (boolIni) {
      // if initialisation, then create all possible qualifications
      // into the drop-down boxes
      $.each(config.globalData, function (_, item): void {
        $('#drop-down-qualification').append(
          $('<option>', {
            value: item.code,
            text: item.qualName,
          })
        );
      });
    }
    // get the drop-down that is currnetly selected
    let selected = document.getElementById('drop-down-qualification') as HTMLInputElement;
    // get the keys from the set

    keys = config.modeOptionsSet;
    for (let i = 0; i < keys.length; ++i) {
      // for every key, check that the data object has the
      // property defined. If it is defined then generate
      // a drop-down option.
      if (config.globalData[selected.value].hasOwnProperty(keys[i])) {
        if (keys[i] === 'Default-Time') {
          $('#drop-down-mode').append(
            $('<option>', {
              value: 'Choose a mode of study',
              text: 'Choose a mode of study',
              id: 'Option-' + 'Default-Time',
            })
          );
        } else {
          $('#drop-down-mode').append(
            $('<option>', {
              value: keys[i],
              text: keys[i].charAt(0) + keys[i].slice(1).toLowerCase(),
              id: 'Option-' + keys[i].replace(/\s/g, '-'),
            })
          );
        }
      }
    }

    // if a selected mode string was given
    // then select the relevant mode in drop down
    if (selectedMode != null) {
      $('#Option-' + selectedMode.replace(/\s/g, '-')).attr('selected', 'selected');
    }

    let mode = document.getElementById('drop-down-mode') as HTMLInputElement;
    let modeStr = mode.value;
    // get the selected mode from drop down
    if (mode.value === 'Choose a mode of study') {
      modeStr = 'Default-Time';
    }
    // create drop down of dates for the relevant mode & course
    $.each(config.globalData[selected.value][modeStr], function (_, item): void {
      $('#drop-down-start').append(
        $('<option>', {
          value: item.startDate,
          text: item.startDate,
          id: item.startDate.replace(/\s/g, '-'),
        })
      );
    });

    // if given a selected date, then display it as selected
    if (selectedDate != null) {
      $(`#${selectedDate.replace(/\s/g, '-')}`).attr('selected', 'selected');
    }

    // change the href in the anchor to link to the corresponding page
    // based on previous data
    const dateInput = document.getElementById('drop-down-start') as HTMLInputElement;
    selectedDate = dateInput.value;
    if (selectedDate !== 'Choose a start date') {
      $('#apply-link').removeAttr('disabled');
      $('#apply-link').attr('href', config.globalData[selected.value][modeStr][selectedDate].applyLink);
    } else {
      $('#apply-link').attr('disabled', 'true');
    }

    return;
  };
  /* function getEntrypoints
   * @param1: obj - object snippet of global data
   * Return object with date and links as attributes
   * Function used to create meta objects that will store a string date and
   * also contain the relevant link for the next page
   */
  const getEntrypoints = (obj: any): any => {
    let theLanguage = $('html').attr('lang') !== undefined ? $('html').attr('lang') : 'en';
    let months;
    let dataObject: any = {};

    // if language is english, use english months
    if (theLanguage == 'en') {
      months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    } else {
      // else use the welsh months
      months = ['Ionawr', 'Chwefror', 'Mawrth', 'Ebrill', 'Mai', 'Mehefin', 'Gorffennaf', 'Awst', 'Medi', 'Hydref', 'Tachwedd', 'Rhagfyr'];
    }

    for (let d in obj) {
      // get the generate date and get the object
      let action = obj[d]['link'];
      let date = new Date(obj[d]['startDate']);
      if (date >= new Date()) {
        let strDate = date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
        // then create an object
        dataObject[strDate] = {
          startDate: strDate,
          applyLink: action,
        };
      }
    }
    return dataObject;
  };

  /* function updateApplyForm
   * @param1: info - object of data from the drop-down boxes
   * Return void
   * function is used each time to update the apply box
   * each time the user selects a drop-down
   */
  const updateApplyForm = (info: any): void => {
    // clear the Mode of Study drop down
    $('#drop-down-mode').empty();
    // clear the Start date drop down
    $('#drop-down-start').empty();
    // update the form
    setApplyForm(false, info.mode, info.date);
    return;
  };

  /*
   * Function updateUrls(void)
   * return type: void
   *
   * When called, function will take the selected school(s)
   * acronym from the metadata value then convert and generate relevant urls
   */
  const updateUrls = (): void => {
    let defaultLink = $('#browse-school');
    let theLanguage = $('html').attr('lang') !== undefined ? $('html').attr('lang') : 'en';
    let schoolKeys = (defaultLink.attr('data-schools') || '').split(';');
    let container = defaultLink.parent().parent(); // get the container for anchor tags
    // check that there values in array
    if (schoolKeys.length > 0 && schoolKeys[0] != '') {
      // Loop through number of acronyms given
      for (let i = 0; i < schoolKeys.length; ++i) {
        let temp = schoolKeys[i].trim();
        // generate anchor tags with acronym
        if (theLanguage == 'en') {
          container.append(
            `<li><a href="${setLanguageUrl(config.schools[temp]['url-taught'])}">Taught ${
              config.schools[temp]['name']
            } courses<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron right</title><path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)" /></svg></a></a></li>`
          );
          container.append(
            `<li><a href="${setLanguageUrl(config.schools[temp]['url-research'])}">Research ${
              config.schools[temp]['name']
            } programmes<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron right</title><path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)" /></svg></a></a></li>`
          );
        } else {
          // else use the welsh months
          container.append(
            `<li><a href="${setLanguageUrl(config.schools[temp]['url-taught'])}">Cyrsiau ${
              config.schools[temp]['name']
            } a addysgir<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron right</title><path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)" /></svg></a></a></li>`
          );
          container.append(
            `<li><a href="${setLanguageUrl(config.schools[temp]['url-research'])}">Rhaglenni Ymchwil ${
              config.schools[temp]['name']
            }<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.75 32"><title>Chevron right</title><path d="M6.13,28.28L18.41,16,6.13,3.72,9.84,0l16,16-16,16Z" transform="translate(-6.13 0)" /></svg></a></a></li>`
          );
        }
      }
      // removed the default link
      defaultLink.remove();
      return;
    }
    return;
  };

  /*
   * Function: capitaliseFirstLetter
   * @param1: string - string of word that needs to be capitalised
   * return string
   */
  const capitaliseFirstLetter = (string: string): string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  /*
   * Function setLanguageUrl
   * @param1: string - url to be changed based on language selected
   * return string
   * function used to change the url to welsh or english based on lang attribute
   */
  const setLanguageUrl = (str: string): string => {
    let lang =
      $('html').attr('lang') !== undefined && ($('html').attr('lang') == 'en' || $('html').attr('lang') == 'cy') ? $('html').attr('lang') : 'en';
    if (lang == 'cy') {
      return str.replace(/https:\/\/www\.cardiff\.ac\.uk\//, 'https://www.cardiff.ac.uk/cy/');
    }
    return str;
  };
  return {
    init,
    initRelatedCourses,
    initRelatedSubjects,
    modeCheck,
    createForm,
    getEntrypoints,
    updateApplyForm,
    updateUrls,
    capitaliseFirstLetter,
    setLanguageUrl,
  };
});
