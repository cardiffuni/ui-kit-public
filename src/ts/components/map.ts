define(['jquery', 'leaflet'], ($: JQueryStatic, L: any): UIKit.Module.Map => {
  const markerIcon = L.icon({
    iconUrl: 'https://cardiff.imgix.net/__data/assets/image/0005/1746248/leaflet-marker-icon.png?w=24',
    iconSize: [24, 35],
    iconAnchor: [12, 35],
  });

  const init = (): void => {
    $('.map-link:not(.map-two-marker)').each(function (index): void {
      const classes = $(this).prop('class');
      const query = $(this).prop('href');
      let caption = '';

      if (typeof $(this).data('caption') != 'undefined') {
        caption = $(this).data('caption');
      }

      const coords = getParameterByName('q', query).split(',');
      const lat = parseFloat(coords[0]);
      const lng = parseFloat(coords[1]);

      const zoom = parseInt(getParameterByName('z', query));

      let html = `
        <figure class="map ${classes}">
          <div class="map-body">
            <div id="map${index}" style="height:100%;"></div>
          </div>
      `;

      if (caption != '') {
        html += `<figcaption class="caption">${caption}</figcaption>`;
      }
      html += '</figure>';

      $(this).wrap(html);

      const mapConfig = {
        tileLayer: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attribution: 'Map data &copy; <a href="http://www.osm.org">OpenStreetMap</a>',
        markerLng: lng,
        markerLat: lat,
        zoomLevel: zoom,
      };

      let map = L.map(`map${index}`, {
        center: [mapConfig.markerLat, mapConfig.markerLng],
        zoom: mapConfig.zoomLevel,
      });

      L.tileLayer(mapConfig.tileLayer, {
        attribution: mapConfig.attribution,
      }).addTo(map);

      L.marker([mapConfig.markerLat, mapConfig.markerLng], { icon: markerIcon }).addTo(map);

      $(this).remove();
    });
  };
  const initMap = (): void => {
    $('.map-link.map-two-marker').each(function (index): void {
      // coordinates for map markers are embedded in anchor tags on page

      let mapLinkParent = $(this).parent();
      let mapLinkChildren = mapLinkParent.children('a');
      mapLinkChildren.html('');
      let coordinateArray: any[] = [];

      mapLinkChildren.each(function (link): void {
        const href = $(this).attr('href');
        const latLngStr = getParameterByName('q', href || '');
        const latLng = latLngStr.split(',');
        const lat = parseFloat(latLng[0]);
        const lng = parseFloat(latLng[1]);
        let coordinates = { lat: lat, lng: lng };
        coordinateArray.push(coordinates);

        if (link > 0) {
          $(this).addClass('d-none');
        }
      });

      let classes = $(this).prop('class');
      let query = $(this).prop('href');
      let caption = '';
      if (typeof $(this).data('caption') != 'undefined') {
        caption = $(this).data('caption');
      }

      let zoom = parseInt(getParameterByName('z', query));

      let html = `<figure class="map ${classes}"><div class="map-body"><div id="map-two-marker-${index}" style="height:100%;"></div></div>`;

      if (caption != '') {
        html += `<figcaption class="caption">${caption}</figcaption>`;
      }

      html += '</figure>';
      $(this).wrap(html);

      const mapConfig = {
        tileLayer: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attribution: 'Map data &copy; <a href="http://www.osm.org">OpenStreetMap</a>',
      };

      let map = L.map(`map-two-marker-${index}`, {
        zoom,
      });

      L.tileLayer(mapConfig.tileLayer, {
        attribution: mapConfig.attribution,
      }).addTo(map);

      let markersGroup = L.featureGroup();
      map.addLayer(markersGroup);

      for (let i = 0; i < coordinateArray.length; i++) {
        let lat = coordinateArray[i].lat;
        let lng = coordinateArray[i].lng;
        const divIcon = L.divIcon({
          html: `<svg class=“icon icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 363.34 490.91"><defs><style>.cls-1,.cls-2{fill:none;}.cls-1{clip-rule:evenodd;}.cls-3{clip-path:url(#clip-path);}.cls-4{clip-path:url(#clip-path-2);}.cls-5{fill:#d4374a;}</style><clipPath id="clip-path" transform="translate(-22.93 45.45)"><path class="cls-1" d="M204.6,0c75.25,0,136.25,62.3,136.25,139.13C340.85,279.7,204.6,400,204.6,400S68.35,279.7,68.35,139.13C68.35,62.3,129.35,0,204.6,0Z"/></clipPath><clipPath id="clip-path-2" transform="translate(-22.93 45.45)"><rect class="cls-2" x="-1559.95" y="-15063.64" width="3406.32" height="59036.36"/></clipPath></defs><title>map-marker</title><g class="cls-3"><g class="cls-4"><rect class="cls-5" width="363.34" height="490.91"/></g></g></svg><span class="leaflet-div-icon-text">${i}</span>`,
        });
        if (coordinateArray.length > 1) {
          L.marker([lat, lng], { icon: divIcon, title: i.toString() }).addTo(markersGroup);
        } else {
          L.marker([lat, lng], { icon: divIcon, title: i.toString() }).addTo(markersGroup);
        }
      }
      if (coordinateArray.length > 1) {
        map.fitBounds(markersGroup.getBounds().pad(0.5));
      } else {
        map.setView([coordinateArray[0].lat, coordinateArray[0].lng], 15);
      }
      map.scrollWheelZoom.disable();

      $(this).remove();
    });
  };

  const twoMarkerMap = (): void => {
    let $schoolAndLocationTab = $('a[href="#school-and-location"]');
    if ($schoolAndLocationTab.length) {
      $($schoolAndLocationTab).on('shown', function (): void {
        if (!$('.map').length) {
          initMap();
          $('.leaflet-control-zoom-in, .leaflet-control-zoom-out').on('click', function (): void {
            this.focus();
          });
        }
      });
    } else {
      initMap();
      $('.leaflet-control-zoom-in, .leaflet-control-zoom-out').on('click', function (): void {
        this.focus();
      });
    }
  };

  const getParameterByName = (name: string, querystring: string): string => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
      results = regex.exec(querystring);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  return {
    init,
    twoMarkerMap,
    getParameterByName,
  };
});
