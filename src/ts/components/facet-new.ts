/**
 * Translation function (*** included in Facet.ts, required here for compilation ***)
 * @param attribute Translation key
 * @param contextOverride Context to override with
 */
const i18n = (attribute: string, contextOverride: string | undefined = undefined): string => {
  const context: string = contextOverride || $('html').attr('lang') || 'en';
  const translations: UIKit.Translations = {
    en: {
      view: 'View results',
      clear: 'Clear all',
      filter: 'Filter results',
    },
    cy: {
      view: 'View results CY',
      clear: 'Clear all CY',
      filter: 'Hidlo canlyniadau',
    },
  };

  return translations[context][attribute] || '';
};

/** Build function to build mobile modal dynamically  */
const build = (): void => {
  let formElements = ``;
  const form = $('.facet:first-child()').parent();
  const formAction = form.attr('action');

  $('.facet').each((_, item): void => {
    const title = $(item).find('.facet-title');
    const contents = $(item)
      .find('.facet-body')
      .html()
      .replace(`aria-labelledby="${title.attr('id')}"`, `aria-labelledby="${title.attr('id')}-mobile"`)
      .replace(/id=\"([a-zA-Z-0-9]+)\"/g, `id="mobile-$1"`)
      .replace(/for=\"([a-zA-Z-0-9]+)\"/g, `for="mobile-$1"`);

    formElements += `
      <fieldset>
        <legend class="form-label" id="${title.attr('id')}-mobile">${title.text().replace(/Chevron (down|up)/, '')}</legend>
        ${contents}
      </fieldset>`;
  });

  let template = `
    <div id="search-result-filter" class="modal modal-facet fade" tabindex="-1" role="dialog" aria-labelledby="filter-results-model" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <p class="modal-title">Filter</p>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><title>Close</title><path d="M29.75,24.5l0.06,0.06a0.69,0.69,0,0,1,.13.16,0.12,0.12,0,0,0,0,.09,0.69,0.69,0,0,1,0,.25v0.22a1,1,0,0,1-.25.47l-4,4a0.56,0.56,0,0,1-.22.16,0.84,0.84,0,0,1-.47.09,0.69,0.69,0,0,1-.25,0l-0.06-.06a0.36,0.36,0,0,1-.19-0.09L16,21.25,7.44,29.81a0.27,0.27,0,0,1-.16.09L7.19,30a0.69,0.69,0,0,1-.25,0,0.47,0.47,0,0,1-.22,0,0.73,0.73,0,0,1-.47-0.22l-4-4a0.56,0.56,0,0,1-.16-0.22A0.84,0.84,0,0,1,2,25.06a0.69,0.69,0,0,1,0-.25l0.06-.09a0.26,0.26,0,0,1,.09-0.16L10.75,16,2.19,7.44s0-.06-0.06-0.09a0.55,0.55,0,0,1-.06-0.16A0.74,0.74,0,0,1,2,6.94a0.69,0.69,0,0,1,0-.25,0.73,0.73,0,0,1,.22-0.44l4-4a0.56,0.56,0,0,1,.22-0.16A0.84,0.84,0,0,1,6.93,2a0.72,0.72,0,0,1,.25.06H7.28a0.66,0.66,0,0,1,.16.13L16,10.75l8.56-8.56a1,1,0,0,1,.19-0.12,0.06,0.06,0,0,0,.06,0,0.69,0.69,0,0,1,.25,0h0.25a1,1,0,0,1,.44.25l4,4a0.56,0.56,0,0,1,.16.22,0.84,0.84,0,0,1,.09.47,0.69,0.69,0,0,1,0,.25,0.06,0.06,0,0,0,0,.06l-0.12.19L21.25,16Z" transform="translate(-2 -2)" /></svg>
            </button>
          </div>
          <div class="modal-body">
            <form action="${formAction}" method="get">
              ${formElements}
              <div class="form-buttons sticky sticky-bottom">
                <button type="submit" class="btn btn-lg btn-info">${i18n('view')}</button>
                <a href="#" id="clear-facet-inputs" class="btn btn-link">${i18n('clear')}</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>`;

  let button = `
    <a href="#search-result-filter" role="button" class="btn btn-lg btn-info btn-filter d-lg-none d-block" data-toggle="modal" id="filter-results-model"
    >
      ${i18n('filter')}
      <svg class="icon" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 319.89">
        <defs><style>.cls-1{fill:#fff;}</style></defs><title>filter-icon</title>
        <rect class="cls-1" width="400" height="40" rx="20"/>
        <rect class="cls-1" x="60" y="139.89" width="280" height="40" rx="20"/>
        <rect class="cls-1" x="120" y="279.89" width="160" height="40" rx="20"/>
      </svg>
    </a>
  `;

  $('.search-results').before(button);
  $('body').append(template);

  /* remove any submit buttons/ or buttons */
  $('#search-result-filter fieldset input[type="submit"], #search-result-filter fieldset .btn ').remove();
  /* Remove any hidden items */

  $(`#search-result-filter fieldset .hidden`).removeClass('hidden');
  $(`#search-result-filter .facet-show-more`).remove();
  form.find('#desktop-facet-submit').remove();
};
