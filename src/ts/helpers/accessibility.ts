define([], (): UIKit.Helpers.Accessibility => {
  let accessiblity: UIKit.AccessibilitySettings = {
    reduceMotion: false,
  };

  const setReduceMotion = (): void => {
    const preferReducedMotionListener = window.matchMedia('(prefers-reduced-motion: reduce)');

    /** Whether the user has opted for a reduced motions experience */
    accessiblity.reduceMotion = preferReducedMotionListener.matches;

    if (typeof preferReducedMotionListener.addEventListener !== 'undefined') {
      preferReducedMotionListener.addEventListener('change', (media): void => {
        accessiblity.reduceMotion = media.matches;
      });
    } else {
      /* IOS */
      preferReducedMotionListener.addListener((media): void => {
        accessiblity.reduceMotion = media.matches;
      });
    }
  };

  setReduceMotion();

  return {
    preferences: accessiblity,
  };
});
