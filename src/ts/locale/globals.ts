define([], (): { root: UIKit.I18n; cy: boolean } => {
  return {
    root: {
      collapseTables: {
        previous: 'View previous rows',
        all: 'View all rows',
      },
      close: 'Close',
      downloadImage: 'Download image',
      backtoTop: {
        title: 'Back to top',
        aria: 'Scroll back to top',
      },
      postgraduate: {
        'pgr-courses': 'https://www.cardiff.ac.uk/study/postgraduate/research/programmes/search',
        'pg-taught-courses': 'https://www.cardiff.ac.uk/study/postgraduate/taught/courses/search',
      },
      clearing: {
        title: 'Clearing and Adjustment',
        linkText: 'Go back to our Clearing pages',
        bodyText: "Call us on <span class='InfinityNumber'>+44 (0)33 3241 2800</span> to discuss potential vacancies.",
      },
      accordions: {
        openAll: 'Open all',
        closeAll: 'Close all',
      },
      cookieBanner: {
        conent: 'We use cookies to enable helpful features and collect information about how well our website and advertising are working.',
        acceptAll: 'Accept all cookies',
        customise: 'Manage your cookie preferences',
        enable: 'Enable cookies',
        url: 'https://www.cardiff.ac.uk/help/cookies/preferences',
      },
      cookiePreferences: {
        options: {
          accept: 'Use cookies that',
          deny: 'Do not use cookies that ',
        },
        preferences: {
          performance: 'measure our website’s performance',
          advertising: 'help with our advertising and marketing',
          functionality: 'enable useful features',
        },
      },
      forms: {
        required: 'This field is required.',
        invalidValue: 'The option you have selected is not valid.',
      },
      contentUnavailable: {
        title: 'Content unavailable',
        contentMessage:
          'This content is not available because your cookie preferences do not allow it. In order to view this content, you will need to allow cookies that:',
        alternativeLink: 'Or view the content on %s',
        howToWatch: 'How to watch this video',
      },
      continueReading: {
        more: 'Read more',
        less: 'Show less',
      },
      navigation: {
        menu: 'Menu',
        close: 'Close',
        back: 'Back to',
      },
      tableOfContents: {
        item: {
          aria: 'Skip to',
        },
      },
      eventStates: {
        ended: 'Event ended',
        open: 'Registration open',
        live: 'Live now',
        upcoming: 'Coming soon',
      },
    },

    cy: true,
  };
});
