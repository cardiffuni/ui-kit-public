define([], (): UIKit.I18n => {
  return {
    collapseTables: {
      previous: 'Gweld rhesi blaenorol',
      all: 'Gweld pob rhes',
    },
    close: 'Cau',
    downloadImage: 'CY Download image',
    backtoTop: {
      title: 'Cy Back to top',
      aria: 'Cy Scroll back to top',
    },
    postgraduate: {
      'pgr-courses': 'https://www.cardiff.ac.uk/cy/study/postgraduate/research/programmes/search',
      'pg-taught-courses': 'https://www.cardiff.ac.uk/cy/study/postgraduate/taught/courses/search',
    },
    clearing: {
      title: 'Clirio ac Addasu',
      linkText: "Yn ôl i'n tudalennau Clirio",
      bodyText: "Ffoniwch <span class='InfinityNumber'>+44 (0)33 3241 2800</span> i drafod lleoedd gwag posibl.",
    },
    accordions: {
      openAll: 'Agor pob un',
      closeAll: 'Cau pob un',
    },
    cookieBanner: {
      conent: "Rydym yn defnyddio briwsion i alluogi nodweddion defnyddiol ac i gasglu gwybodaeth am ba mor dda y mae ein gwefan a'n hysbysebion yn gweithio.",
      acceptAll: 'Caniatáu pob cwci',
      customise: 'Rheoli eich dewisiadau briwsion',
      enable: 'Galluogi cwcis',
      url: 'https://www.cardiff.ac.uk/cy/help/cookies/preferences',
    },
    cookiePreferences: {
      options: {
        accept: "Defnyddio cwcis sy'n",
        deny: "Peidio â defnyddio cwcis sy'n",
      },
      preferences: {
        performance: 'mesur perfformiad ein gwefan',
        advertising: 'helpu gyda hysbysebu a marchnata',
        functionality: 'galluogi nodweddion defnyddiol',
      },
    },
    forms: {
      required: "Mae'r angen llenwi'r maes hwn.",
      invalidValue: "Nid yw'r dewis hwn yn gymwys.",
    },
    contentUnavailable: {
      title: "Nid yw'r cynnwys ar gael",
      contentMessage:
        'Nid yw eich gosodiadau cwcis yn caniatáu inni ddangos y cynnwys hwn chi. Er mwyn gweld y cynnwys hwn, byddwch angen caniatáu cwcis sydd yn:',
      alternativeLink: 'Neu gallwch weld y cynnwys ar %s',
      howToWatch: "Sut i wylio'r fideo hwn",
    },
    continueReading: {
      more: 'Darllenwch ragor',
      less: 'Dangos llai',
    },
    navigation: {
      menu: 'Dewislen',
      close: 'Cau',
      back: 'Nôl i',
    },
    tableOfContents: {
      item: {
        aria: 'CY Skip to',
      },
    },
    eventStates: {
      ended: "Mae'r digwyddiad hwn ar ben",
      open: 'Cofrestru ar agor',
      live: 'Yn fyw nawr',
      upcoming: 'Ar gael cyn bo hir',
    },
  };
});
