define([], (): object => {
  requirejs.config({
    packages: [
      {
        name: 'highcharts',
        main: 'highcharts',
      },
    ],
    config: {
      i18n: {
        locale: (document.querySelector('html') as HTMLElement).getAttribute('lang') || 'en',
      },
    },
    paths: {
      i18n: '../lib/i18n.min',
      jquery: '../lib/jquery-3.4.1',
      prettier: '../lib/prettier',
      popper: '../lib/popper',
      bootstrap: '../lib/bootstrap/bootstrap',
      promise: '../lib/polyfill/promise',
      mapboxgl: '../lib/mapbox/mapbox-gl',
      'mapboxgl-supported': '../lib/mapbox/mapbox-gl-supported',
      leaflet: '../lib/leaflet/leaflet',
      turf: '../lib/mapbox/turf',
      observer: '../lib/polyfill/observer',
      hoverIntent: '../lib/hover-intent.min',
      tableSort: '../lib/table/table-sort',
      embedly: '../lib/embedly/embedly',
      sticky: '../lib/polyfill/sticky',
      validate: '../lib/validate/validate',
      mathJax: '../lib/mathJax',
      youtube: 'https://www.youtube.com/iframe_api?',
      accessplanit: 'https://coursebooking.cardiff.ac.uk/accessplan/NetLibrary/ClientScripts/webintegration',
      highcharts: 'https://code.highcharts.com',
    },
    shim: {
      tableSort: {
        deps: ['jquery'],
      },
      popper: {
        exports: 'popper.js',
      },
      bootstrap: {
        deps: ['popper', 'jquery'],
        exports: 'bootstrap',
      },
      validate: {
        deps: ['jquery'],
        exports: 'validate',
      },
      'components/map': {
        deps: ['leaflet'],
      },
    },
  });

  const version = '{{{UIKitVersion}}}';
  const cdn = 'https://d3q5dbq2rziek.cloudfront.net/';
  const local = 'http://localhost:8080/';
  const env: string = document.location.href.indexOf('localhost:8080') !== -1 ? 'dev' : 'prod';

  const loadCSS = function (url: string): void {
    let link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.href = env === 'prod' ? `${cdn}${version}/${url}` : `${local}dist/${url}`;
    document.getElementsByTagName('head')[0].appendChild(link);
  };

  /* Check for promises */
  if (typeof Promise === 'undefined') {
    requirejs(['promise'], (p: any): void => {
      var Promise = p;
    });
  }

  if (typeof IntersectionObserver === 'undefined') {
    requirejs(['observer'], (intersect: any): void => {
      var IntersectionObserver = intersect;
    });
  }

  const UAString = navigator.userAgent;

  const ie = {
    10: navigator.appVersion.indexOf('MSIE 10') !== -1,
    11: UAString.indexOf('Trident') !== -1 && UAString.indexOf('rv:11') !== -1,
  };

  if (ie['10']) {
    const html = document.querySelector('html');

    if (html !== null) {
      html.classList.add('ie-10');
    }
  }

  if (ie['11']) {
    const html = document.querySelector('html');

    if (html !== null) {
      html.classList.add('ie-11');
    }
  }

  if (window.NodeList && !NodeList.prototype.forEach) {
    (NodeList.prototype as any).forEach = Array.prototype.forEach;
  }

  const globals = (callback: Require): void => {
    callback(
      ['bootstrap', 'jquery', 'hoverIntent', 'components/navigation.min', 'components/search.min', 'components/back-to-top.min'],
      (bs: JQuery, $: JQueryStatic, intent: any, nav: any, search: UIKit.Module.SearchGlobal, backToTop: UIKit.Module.BackToTop): void => {
        ($('.nav-global-links > .nav-item') as UIKit.HoverIntent).hoverIntent({
          over(): void {
            if ($(this).hasClass('open')) return;
            $('.nav-global-links .open').removeClass('open');
            $(this).addClass('open');
          },
          out(): void {
            $(this).removeClass('open');
          },
          timeout: 400,
        });

        new nav.Navigation();
        search.init();
        backToTop.init();
        adjustScrollbarWidth();

        if (typeof cfuiReady !== 'undefined') {
          if (cfuiReady.length > 0) {
            for (const n in cfuiReady) {
              $(document).ready(cfuiReady[n]);
            }
          }
        }
      }
    );
  };

  const checkChart = (callback: Require): void => {
    if (document.querySelector('.chart-table') !== null) {
      callback(['jquery', 'components/chart'], ($: JQueryStatic, chart: UIKit.Module.Chart): void => {
        chart.init();
      });
    }
  };

  const checkClearing = (callback: Require): void => {
    if (document.referrer.indexOf('/clearing') !== -1 && window.location.href.indexOf('/clearing') === -1) {
      callback(['components/clearing.min'], (clearing: UIKit.Module.Clearing): void => clearing.init());
    }
  };

  const checkAlert = (callback: Require): void => {
    if (document.querySelector('.alert') !== null || document.querySelector('.alert-body.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/alert.min'], (bs: JQuery, alert: UIKit.Module.Alert): void => {
        alert.init();
      });
    }
  };

  const checkAccordions = (callback: Require): void => {
    if (document.querySelector('.accordion') !== null || document.querySelector('.accordion-body.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/accordion.min'], (bs: Function, accordion: UIKit.Module.Accordion): void => {
        accordion.init();
      });
    }
  };

  const checkPrettier = (callback: Require): void => {
    if (document.querySelector('pre') !== null) {
      callback(['prettier'], (): void => {
        const items = document.querySelectorAll('pre');

        for (let i = 0; i < items.length; i++) {
          items[i].classList.add('linenums');
          items[i].classList.add('prettyprint');
        }
      });
    }
  };

  const checkHomepage = (callback: Require): void => {
    if (document.querySelector('.l-home') !== null) {
      callback(['components/homepage.min'], (home: UIKit.Module.Homepage): void => {
        home.init();
      });
    }
  };

  const checkTooltip = (callback: Require): void => {
    if (document.querySelector('[data-toggle="tooltip"]') !== null) {
      callback(['bootstrap'], (): void => {
        $('[data-toggle="tooltip"]').tooltip();
      });
    }
  };
  const checkToasts = (callback: Require): void => {
    if (document.querySelector('.toast') !== null) {
      callback(['bootstrap'], (): void => {
        $('.toast').toast({
          autohide: true,
          animation: true,
          delay: 3200,
        });
      });
    }
  };

  const checkCarousel = (callback: Require): void => {
    if (document.querySelector('.carousel') !== null) {
      callback(['bootstrap', 'components/carousel.min'], (bs: Function, carousel: UIKit.Module.Carousel): void => {
        carousel.init();
      });
    }
  };

  const checkPills = (callback: Require): void => {
    if (document.querySelector('.nav-pills') !== null) {
      callback(['components/pill.min'], (pill: UIKit.Module.Pill): void => {
        pill.init();
      });
    }
  };

  const checkInteractiveMap = (callback: Require): void => {
    if (document.querySelector('.interactive-map') !== null) {
      callback(['components/interactive-map.min'], (map: UIKit.Module.InteractiveMap): void => {
        loadCSS('lib/leaflet/leaflet.css');
        map.init();
      });
    }
  };
  const checkFilters = (callback: Require): void => {
    if (document.querySelector('.filter') !== null) {
      callback(['components/filter.min'], (filter: UIKit.Module.Filter): void => {
        filter.init();
      });
    }
  };

  const checkAccordionCPD = (callback: Require): void => {
    if (document.querySelector('.accordion-cpd-course') !== null) {
      callback(['components/accordion-CPD.min'], (cpd: UIKit.Module.AccordionCPD): void => {
        cpd.init();
      });
    }
  };

  const checkSquizConditional = (callback: Require): void => {
    if (document.querySelector('.form-conditional') !== null) {
      callback(['components/conditional-fields.min'], (conditionals: UIKit.Module.ConditionalFields): void => {
        conditionals.init();
      });
    }
  };

  const checkContentMap = (callback: Require): void => {
    if (document.querySelector('#content-map') !== null) {
      callback(
        ['mapboxgl', 'turf', 'mapboxgl-supported', 'components/content-map.min'],
        (mapbox: any, turf: any, sup: any, map: UIKit.Module.ContentMap): void => {
          map.init();
        }
      );
    }
  };

  const checkContentToggle = (callback: Require): void => {
    if (document.querySelector('.content-toggle') !== null) {
      callback(['components/content-toggle.min'], (contentToggle: UIKit.Module.ContentToggle): void => {
        contentToggle.init();
      });
    }
  };

  const checkCookiePolicy = (callback: Require): void => {
    /* Check if cookie is set and is not a legacy value */
    let matches = document.cookie.match(
      new RegExp('(?:^|; )' + 'cu-cookie-preferences'.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)')
    );
    const cookieValue = matches ? decodeURIComponent(matches[1]) : undefined;

    if (cookieValue === undefined || cookieValue === 'true') {
      callback(['components/cookie-policy.min'], (cookie: UIKit.Module.CookiePolicy): void => {
        cookie.init(cookieValue);
      });
    }
  };

  const checkCookiePreferenceUpdateForm = (callback: Require): void => {
    /* Check if cookie is set and is not a legacy value */
    if (document.querySelector('#update-cookie-preferences-form')) {
      callback(['components/cookie-policy.min'], (cookie: UIKit.Module.CookiePolicy): void => {
        cookie.cookiePreferenceForm();
      });
    }
  };

  const checkDropdown = (callback: Require): void => {
    if (document.querySelector('.dropdown') !== null) {
      callback(['components/dropdown.min'], (dropdown: UIKit.Module.Dropdown): void => {
        dropdown.init();
      });
    }
  };

  const checkEmbed = (callback: Require): void => {
    if (document.querySelector('.embed') !== null) {
      callback(['components/embed.min'], (embed: UIKit.Module.Embed): void => {
        embed.init();
      });
    }
  };

  const checkFacet = (callback: Require): void => {
    if (document.querySelector('.facet') !== null) {
      callback(['components/facet.min'], (facet: UIKit.Module.Facet): void => {
        facet.init();
      });
    }
  };

  const checkGA = (callback: Require): void => {
    if (document.querySelector('.ga-event') !== null) {
      callback(['components/ga-event.min'], (ga: UIKit.Module.GAEvent): void => {
        ga.init();
      });
    }
  };

  const checkLazyLoad = (callback: Require): void => {
    if (document.querySelector('img.lazy,iframe.lazy') !== null) {
      callback(['components/lazy-loader.min'], (lazy: UIKit.Module.LazyLoader): void => {
        lazy.init();
      });
    }
  };

  const checkMap = (callback: Require): void => {
    if (document.querySelector('.map-link') !== null) {
      callback(['components/map.min'], (map: UIKit.Module.Map): void => {
        loadCSS('lib/leaflet/leaflet.css');
        map.init();

        if (document.querySelector('.map-link.map-two-marker')) {
          map.twoMarkerMap();
        }
      });
    }
  };

  const checkNavSection = (callback: Require): void => {
    if (document.querySelector('.nav-section.sticky-top') !== null) {
      callback(['components/nav-more.min', 'components/nav-section.min'], (more: UIKit.Module.NavMore, anchor: UIKit.Module.NavSection): void => {
        more.init().then((): void => {
          anchor.init();
        });
      });
    } else {
      if (document.querySelector('.nav-section') !== null) {
        callback(['components/nav-more.min'], (more: UIKit.Module.NavMore): void => {
          more.init();
        });
      }
    }
  };

  const checkPostgrad = (callback: Require): void => {
    if (document.querySelector('#postgrad-apply-form') !== null) {
      callback(['components/postgraduate.min'], (postgrad: UIKit.Module.Postgraduate): void => {
        postgrad.init();
      });
    }

    if (document.querySelector('#related-courses') !== null) {
      callback(['components/postgraduate.min'], (postgrad: UIKit.Module.Postgraduate): void => {
        postgrad.initRelatedCourses();
      });
    }
    if (document.querySelector('#post-grad-related-subjects') !== null) {
      callback(['components/postgraduate.min'], (postgrad: UIKit.Module.Postgraduate): void => {
        postgrad.initRelatedSubjects();
      });
    }
  };

  const checkProgressSet = (callback: Require): void => {
    if (document.querySelector('.progress-set') !== null) {
      callback(['components/progress-set.min'], (progress: UIKit.Module.ProgressSet): void => {
        progress.init();
      });
    }
  };

  const checkForm = (callback: Require): void => {
    if (document.querySelector('.form input[type="submit"]') !== null) {
      callback(['components/submit-form.min'], (form: UIKit.Module.SubmitForm): void => {
        form.init();
      });
    }
  };

  const checkTabFocus = (callback: Require): void => {
    if (window.location.href.indexOf('#') > -1) {
      callback(['bootstrap', 'components/tab-focus.min'], (_: any, tabs: UIKit.Module.TabFocus): void => {
        tabs.init();
      });
    }
  };
  const checkTabs = (callback: Require): void => {
    if (document.querySelector('.tab-pane.squiz-bodycopy') !== null) {
      callback(['bootstrap', 'components/tabs.min'], (_: any, tabs: UIKit.Module.Tab): void => {
        tabs.init();
      });
    }
  };
  const checkSliderTabs = (callback: Require): void => {
    if (document.querySelector('.tabs--with-slider') !== null) {
      callback(['bootstrap', 'components/tabs.min'], (_: any, tabs: UIKit.Module.Tab): void => {
        tabs.initSliders();
      });
    }
  };
  const checkTabAccordion = (callback: Require): void => {
    if (document.querySelector('.tabs-vertical') !== null) {
      callback(['components/tab-accordion.min'], (accordion: UIKit.Module.TabAccordion): void => {
        accordion.init();
      });
    }
  };

  const checkTables = (callback: Require): void => {
    if (document.querySelector('table') !== null) {
      if (document.querySelector('.table-sortable') !== null) {
        callback(['jquery', 'tableSort', 'components/table.min'], ($: any, _: any, table: UIKit.Module.Table): void => {
          table.init();
        });
      } else {
        callback(['jquery', 'components/table.min'], (_: any, table: UIKit.Module.Table): void => {
          table.init();
        });
      }
    }
  };

  const checkSticky = (callback: Require): void => {
    if (document.querySelector('.sticky-top, .sticky') !== null) {
      const el = document.createElement('a');
      const mStyle = el.style;
      let supported = true;
      mStyle.cssText = 'position:sticky;position:-webkit-sticky;position:-ms-sticky;';

      if (mStyle !== null && mStyle.position !== null) {
        supported = mStyle.position.indexOf('sticky') !== -1;
      } else {
        supported = false;
      }

      if (!supported) {
        callback(['sticky'], (stickybits: any): void => {
          stickybits('.sticky-top');
        });
      }

      callback(['components/sticky-activator.min'], (sticky: UIKit.Module.StickyActivator): void => {
        sticky.init();
      });
    }
  };

  const checkValidation = (callback: Require): void => {
    if (document.querySelector('.validate') !== null) {
      callback(['validate', 'components/validate.min'], (_: any, val: UIKit.Module.Validate): void => {
        val.init();
      });
    }
  };

  const checkMaths = (callback: Require): void => {
    if (document.querySelector('.math') !== null) {
      callback(['mathJax'], (mathJax: any): void => {
        const wind: UIKit.MathJaxWindow = window;

        wind.MathJax.startup.defaultPageReady();
      });
    }
  };

  const checkThumbnails = (callback: Require): void => {
    if (document.querySelector('.thumbnail.with-modal') !== null) {
      callback(['components/thumbnails.min'], (checkThumbnails: UIKit.Module.Thumbnails): void => {
        checkThumbnails.init();
      });
    }
  };

  const checkFormAutoSubmit = (callback: Require): void => {
    if (document.querySelector('[data-auto-redirect="true"]') !== null) {
      callback(['components/auto-redirect.min'], (redirect: UIKit.Module.AutoRedirect): void => {
        redirect.init();
      });
    }
  };

  const checkPostgraduateSearch = (callback: Require): void => {
    if (document.querySelector('#postgraduate-search-form') !== null) {
      callback(['components/postgraduate-search.min'], (postgradSearch: UIKit.Module.PostgraduateSearch): void => {
        postgradSearch.init();
      });
    }
  };

  const adjustScrollbarWidth = (): void => {
    let scrollBarWidth = window.innerWidth - ($(document).width() || 0);
    let root = document.documentElement;
    root.style.setProperty('--scrollbar-width', `${scrollBarWidth}px`);
  };

  const checkVideoWithPlaceholder = (callback: Require): void => {
    if (document.querySelector('.video.video--lazy') !== null) {
      callback(['components/video.min'], (videos: UIKit.Module.Video): void => {
        videos.init();
      });
    }
  };
  const checkTableOfContents = (callback: Require): void => {
    if (document.querySelector('.r-document') !== null) {
      callback(['components/table-of-contents.min'], (toc: UIKit.Module.TableOfContents): void => {
        toc.init();
      });
    }
  };

  const checkRequestAccessplanit = (callback: Require): void => {
    if (document.querySelector('#request-interest') !== null) {
      callback(['components/accessplanit.min'], (planit: UIKit.Module.Accessplanit): void => {
        planit.init();
      });
    }
  };

  const checkEventStatus = (callback: Require): void => {
    if (document.querySelector('.event-schedule__item[data-timestamp-start]') !== null) {
      callback(['components/event-schedule.min'], (eventSchedule: UIKit.Module.EventSchedule): void => {
        eventSchedule.init();
      });
    }
  };

  const checkTableCollapse = (callback: Require): void => {
    if (document.querySelector('.table-collapse') !== null) {
      callback(['components/table-collapse.min'], (tableCollapse: UIKit.Module.TableCollapse): void => {
        tableCollapse.init();
      });
    }
  };

  const checkCardsControls = (callback: Require): void => {
    if (document.querySelector('.cards') !== null) {
      callback(['components/card-controls.min'], (cardControls: UIKit.Module.Controls): void => {
        cardControls.init();
      });
    }
  };
  const checkDynamicIncludes = (callback: Require): void => {
    if (document.querySelector('.dynamic-include') !== null) {
      callback(['components/dynamic-includes.min'], (dynamicIncludes: UIKit.Module.DynamicIncludes): void => {
        dynamicIncludes.init();
      });
    }
  };

  const init = (): void => {
    globals(requirejs);
    checkChart(requirejs);
    checkGA(requirejs);
    checkEventStatus(requirejs);
    checkToasts(requirejs);
    checkSliderTabs(requirejs);
    checkInteractiveMap(requirejs);
    checkFilters(requirejs);
    checkTableOfContents(requirejs);
    checkPostgraduateSearch(requirejs);
    checkClearing(requirejs);
    checkPrettier(requirejs);
    checkTooltip(requirejs);
    checkAlert(requirejs);
    checkAccordions(requirejs);
    checkCarousel(requirejs);
    checkTabs(requirejs);
    checkPills(requirejs);
    checkNavSection(requirejs);
    checkAccordionCPD(requirejs);
    checkSquizConditional(requirejs);
    checkContentMap(requirejs);
    checkContentToggle(requirejs);
    checkCookiePolicy(requirejs);
    checkCookiePreferenceUpdateForm(requirejs);
    checkDropdown(requirejs);
    checkEmbed(requirejs);
    checkFacet(requirejs);
    checkLazyLoad(requirejs);
    checkMap(requirejs);
    checkProgressSet(requirejs);
    checkPostgrad(requirejs);
    checkForm(requirejs);
    checkForm(requirejs);
    checkTabFocus(requirejs);
    checkTabAccordion(requirejs);
    checkTables(requirejs);
    checkSticky(requirejs);
    checkValidation(requirejs);
    checkMaths(requirejs);
    checkHomepage(requirejs);
    checkFormAutoSubmit(requirejs);
    checkThumbnails(requirejs);
    checkVideoWithPlaceholder(requirejs);
    checkRequestAccessplanit(requirejs);
    checkTableCollapse(requirejs);
    checkCardsControls(requirejs);
    checkFilters(requirejs);
    checkDynamicIncludes(requirejs);
  };

  init();

  return {
    init,
    checkClearing,
    checkPrettier,
    checkAlert,
    checkAccordions,
    checkPills,
    checkTooltip,
  };
});
