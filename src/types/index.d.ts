declare var cfuiReady: any[];

declare namespace UIKit {
  namespace Module {
    interface Accessplanit {
      init(): void;
    }
    interface BackToTop {
      init(): void;
    }
    interface ContentGroup {
      init(): void;
    }
    interface Clearing {
      init(): void;
      template(context: string, bodyText: string, linkUrl: URL, linkText: string): string;
    }

    interface Filter {
      init(): void;
      selectFilter(elm: HTMLElement): void;
    }
    interface InteractiveMap {
      init(): void;
    }
    interface ContentUnavailable {
      getDisabledYoutubeHTML(options: {
        serviceProvider: ServiceProvider | string;
        youtubeID?: string;
        index: number;
        alt: string;
        url: string;
        width: number;
        preferencesToShow: string[];
      }): string;
      getDisabledEmbedHTML(options: {
        serviceProvider: ServiceProvider | string;
        youtubeID?: string;
        index: number;
        alt: string;
        url: string;
        preferencesToShow: string[];
      }): string;
    }

    interface Chart {
      init(): void;
    }
    interface Controls {
      init(): void;
      checkForControls(): void;
    }
    interface Cards {
      init(): void;
    }
    interface Alert {
      init(): void;
      create(): void;
      extractLanguageSubstring(input: string): string;
    }
    interface Accordion {
      init(): void;
      create(): void;
      openAll(target: JQuery<HTMLElement>, parentID: string): void;
      closeAll(target: JQuery<HTMLElement>, parentID: string): void;
      updateExpanded(button: JQuery<HTMLElement>, status: boolean): void;
      extractLanguageSubstring(input: string): string;
    }
    interface Pill {
      init(): void;
      openAll(): void;
      open(input: string): void;
    }
    interface Carousel {
      init(): void;
      checkNavigation(gallery: HTMLElement): void;
      computeOffset(gallery: HTMLElement): void;
    }
    interface Image {
      init(): void;
      stop(image: JQuery<HTMLElement>): void;
      start(image: JQuery<HTMLElement>): void;
    }
    interface AccordionCPD {
      init(): void;
    }
    interface ConditionalFields {
      init(): void;
      changeConditionalField(currentValue: undefined | string | number | string[], field: any, $childField: JQuery<HTMLElement>): void;
      getCheckboxValues(fieldName: string): (string | number)[];
    }
    interface ContentToggle {
      init(): void;
    }
    interface ContentMap {
      init(): void;
      loadMapData(): void;
      addSourceFeed(url: string, type: string): void;
      addMassMarkersToMap(): void;
      addHighlightsToMap(): void;
      getSourceFeeds(): void;
      buildFiltersMenu(): void;
      filterMapMarkers(): void;
      filterMarkers(obj: mapboxgl.MapboxGeoJSONFeature): mapboxgl.MapboxGeoJSONFeature | void;
      collapseMenu(e: JQuery.ClickEvent): void;
    }
    interface CookiePolicy {
      init(value: 'true' | undefined): void;
      renderCookieBanner(): void;
      removeCookieBanner(): void;
      setCookie(name: string, value: string, exdays: number): void;
      cookiePreferenceForm(): void;
      getCookiePreferences(): CookiePreferencesUpdate;
      setUpdateListener(): void;
    }
    interface Dropdown {
      init(): void;
      dropdown(el: HTMLElement): void;
      dropdownButton(el: HTMLElement): void;
      dropdownToggle(): void;
      dropdownNavSection(): void;
    }

    interface DynamicIncludes {
      init(): void;
    }
    interface Thumbnails {
      init(): void;
      positionModalIcons(): void;
    }

    interface Embed {
      init(): void;
    }
    interface Facet {
      init(): void;
      moveToNextInput(e: JQuery.KeyUpEvent, $this: JQuery<HTMLInputElement>): void;
      dateRangeInputFieldFilter(e: JQuery.KeyDownEvent): void;
      clearFacets(): void;
      checkDirty(): void;
      disableClear(): void;
    }

    interface AutoRedirect {
      init(): void;
    }
    interface GAEvent {
      init(): void;
    }

    interface EventSchedule {
      init(): void;
    }
    interface LazyLoader {
      init(): void;
    }
    interface Map {
      init(): void;
      twoMarkerMap(): void;
      getParameterByName(name: string, query: string): string;
    }
    interface NavSection {
      init(): void;
      navStick(): void;
    }
    interface SearchGlobal {
      init(): void;
    }
    interface NavGlobal {
      init(): void;
      toggleGlobalSearch(): void;
    }
    interface NavMore {
      init(): PromiseLike<boolean>;
    }
    interface Postgraduate {
      init(): void;
      initRelatedCourses(): void;
      initRelatedSubjects(): void;
      modeCheck(key: string, array: any): void;
      createForm(): void;
      getEntrypoints(obj: any): void;
      updateApplyForm(info: any): void;
      updateUrls(): void;
      capitaliseFirstLetter(str: string): void;
      setLanguageUrl(str: string): void;
    }

    interface PostgraduateSearch {
      init(): void;
    }
    interface ProgressSet {
      init(): void;
    }
    interface Squizbodycopy {
      init(): void;
      transformTabs(): void;
      transformAccordions(): void;
      transformAlerts(): void;
      extractLanguageSubstring(inputString: string): void;
      moveBodyCopiesWithRoles(): void;
    }
    interface Tab {
      init(): void;
      extractLanguageSubstring(inputString: string): void;
      initSliders(): void;
    }

    interface Role {
      init(): void;
    }
    interface SubmitForm {
      init(): void;
    }
    interface TabFocus {
      init(): void;
    }
    interface TabAccordion {
      init(): void;
      accordionToTab(): void;
      tabToAccordion(): void;
    }
    interface Table {
      init(): void;
    }

    interface TableCollapse {
      init(): void;
    }

    interface Validate {
      init(): void;
      highlight(element: HTMLElement): void;
      unhighlight(element: HTMLElement): void;
    }
    interface Thumbnails {
      init(): void;
      positionModalIcons(): void;
    }

    interface Homepage {
      init(): void;
      changeCourseFinderForm(): void;
      addHeroVideo(): void;
    }

    interface StickyActivator {
      init(): void;
    }
    interface Video {
      init(): void;
      pauseNonActiveVideos(videoID: string): void;
      pauseVideo(options: { video: JQuery<HTMLElement>; disableContainerResize: boolean }): void;
      addMarkup(opts: { video: JQuery<HTMLElement>; index: number; disabled: boolean; preferencesToShow: string[] }): void;
      getPaddingUnits(options: { parent: JQuery<HTMLElement>; container: JQuery<HTMLElement> }): UIKit.Video.PaddingUnit;
    }

    interface TableOfContents {
      init(): void;
    }
    interface ShowHide {
      showHide(): void;
    }
  }

  namespace Helpers {
    interface Accessibility {
      preferences: UIKit.AccessibilitySettings;
    }
  }

  interface AccessibilitySettings {
    /**
     * Whether the user has prefer-reduced-motion enabled
     */
    reduceMotion: boolean;
  }
  interface HoverIntent extends JQuery {
    hoverIntent(options: { over: Function; out?: Function; selector?: string; timeout?: number }): void;
  }

  interface I18n {
    collapseTables: {
      previous: string;
      all: string;
    };
    close: string;
    downloadImage: string;
    backtoTop: {
      title: string;
      aria: string;
    };
    postgraduate: {
      'pgr-courses': string;
      'pg-taught-courses': string;
    };
    clearing: {
      title: string;
      linkText: string;
      bodyText: string;
    };
    accordions: {
      openAll: string;
      closeAll: string;
    };
    cookieBanner: {
      conent: string;
      acceptAll: string;
      customise: string;
      enable: string;
      url: string;
    };
    cookiePreferences: {
      options: {
        accept: string;
        deny: string;
      };
      preferences: {
        performance: string;
        advertising: string;
        functionality: string;
      };
    };
    forms: {
      required: string;
      invalidValue: string;
    };
    contentUnavailable: {
      title: string;
      contentMessage: string;
      alternativeLink: string;
      howToWatch: string;
    };
    continueReading: {
      more: string;
      less: string;
    };
    navigation: {
      menu: string;
      close: string;
      back: string;
    };
    tableOfContents: {
      item: {
        aria: string;
      };
    };
    eventStates: {
      ended: string;
      open: string;
      live: string;
      upcoming: string;
    };
  }
  interface Indexable {
    [index: string]: string;
  }
  namespace Navigation {
    interface Status {
      session: {
        refresh: boolean;
        disable: boolean;
      };
      transition: {
        menu: boolean;
      };
      direction: {
        back: boolean;
      };
      state: {
        menuOpen: boolean;
        waitingResponse: boolean;
        finishedProcessing: boolean;
      };
    }
    namespace Store {
      interface Store {
        [index: string]: UIKit.Navigation.Store.StoreContext;
      }
      interface StoreContext {
        [index: string]: UIKit.Navigation.Store.Item;
      }
      interface Item {
        assetid: number;
        siblings: UIKit.Navigation.Store.Page[];
        children: UIKit.Navigation.Store.Page[];
        lineage: UIKit.Navigation.Store.Page[];
      }
      interface Page {
        assetid: string;
        name: string;
        url: string;
        has_children: boolean;
        active: boolean;
        status: {
          under_construction: boolean;
          live: boolean;
        };
        showEditingBrackets: boolean;
        status_code: number;
      }
    }
  }
  namespace Video {
    interface VideoReferences {
      [index: string]: {
        [index: string]: YT.Player | DisabledVideo;
      };
    }
    interface DisabledVideo {
      default: boolean;
      playVideo: Function;
      pauseVideo: Function;
    }

    interface PaddingUnit {
      parent: {
        left: number;
        right: number;
      };
      container: {
        left: number;
        right: number;
      };
      total: number;
    }
  }

  interface Translations {
    [index: string]: TranslationItem;

    en: TranslationItem;
    cy: TranslationItem;
  }

  interface TranslationItem {
    [index: string]: string;
  }

  interface SquizDescription {
    [index: string]: string;
    en: string;
    cy: string;
  }
  interface MapboxConfig {
    accessToken: string;
    mapStyle: string;
    $mapContainer: JQuery<HTMLElement> | null;
    massMarkers: MapboxFeatures;
    geoJsonLoadedcounter: number;
    activeFilters: any[];
    language: string;
    popupActive: boolean;
    defaultCenter: number[];
    defaultZoom: number;
    massClusterMaxZoom: number;
    massClusterRadius: number;
    browserError: string;
  }
  interface MapboxFeatures {
    type: string;
    features: any[];
  }

  interface EmbedWindow extends Window {
    twttr?: any;
    embedly?: any;
  }
  interface HTMLElementTable extends JQuery<HTMLElement> {
    tablesorter(): void;
  }

  interface JQueryValidate extends JQuery<HTMLElement> {
    validate(options: any): void;
  }

  interface MathJaxWindow extends Window {
    MathJax?: any;
  }

  interface CookiePreferences {
    preferences: {
      performance: boolean;
      advertising: boolean;
      functionality: boolean;
      essential: boolean;
    };
    performance: boolean;
    advertising: boolean;
    functionality: boolean;
    essential: boolean;
  }

  interface CookiePreferencesUpdate {
    performance: boolean;
    advertising: boolean;
    functionality: boolean;
    essential: boolean;
  }

  interface WindowGTM extends Window {
    dataLayer: any;
  }
  interface VideoAnimationOptions {
    height: number;
    width: number;
    left: number;
    right?: number;
    bottom?: number;
  }
  interface EventScheudle {
    element: JQuery<HTMLElement>;
    start: EventScheudleItem;
    end: EventScheudleItem;
    handlers: {
      live?: number;
      ended?: number;
    };
    status: {
      current: 'live' | 'ended' | 'upcoming' | 'open';
      open: boolean;
      live: boolean;
      ended: boolean;
      upcoming: boolean;
    };
    crossover: number;
  }
  interface EventScheudleItem {
    value: number;
    date?: Date;
  }
  type HomepageCourseMode = 'undergraduate' | 'pgt' | 'pgr' | 'cpe' | 'cpd';
  type HomepageBoxContexts = 'en' | 'cy';
  type HomepageBoxOptions = {
    [index in HomepageCourseMode]: HomepageBoxOptionItem;
  };
  type HomepageBoxOptionItem = {
    [index in HomepageBoxContexts]: HomepageBoxOptionContextItem;
  };
  interface HomepageBoxOptionContextItem {
    collection: string;
    formAction: string;
    subject: HomepageBoxLink | null;
    school: HomepageBoxLink | null;
    az: HomepageBoxLink | null;
  }
  interface HomepageBoxLink {
    url: string;
    aria: string;
    text: string;
  }
  interface MapData {
    panel: {
      enabled: boolean;
      title: string;
    };
    items: MapDataItem[];
  }
  interface MapDataItem {
    name: string;
    teaser: string;
    category?: {
      exists: boolean;
      text: string;
    };
    subtitle: {
      exists: boolean;
      text: string;
    };
    image: {
      exists: boolean;
      path: string;
      cdn: string;
      defaultImage: string;
      alt: string;
    };
    links: {
      url: string;
      data: string;
      text: string;
      icon: string;
      aria: string;
    }[];
    coordinates: {
      lat: number;
      long: number;
    };
    filters: { key: string; text: string }[];
    addEventListener(type: string, callback: Function): void;
  }
  namespace Filter {
    type Modes = 'list' | 'map';
  }
  interface ShowHideItem {
    id: string;
    showAt: number;
    hideAt: number;
    active: boolean;
    handlers: {
      live?: number;
      ended?: number;
    };
  }
}
declare var cfuiConditionalFieldsConfig: any;
declare var contentMapConfig: any;
declare var contentMapConfig: any;
declare var disableCookieBanner: any;
declare type ServiceProvider = 'Facebook' | 'Youtube' | 'Soundcloud' | 'Panopto' | 'Twitter' | 'Instagram' | 'Vimeo' | 'Animoto' | 'Slideshare';

declare var EnquiryManager: {
  createOpportunity(options: Accessplanit.Enquiry, success: (data: any) => void, error: (data: any) => void): void;
};

declare namespace Accessplanit {
  interface Enquiry {
    OwnerID: string;
    Title?: string;
    Forename: string;
    Surname: string;
    Email: string;
    Phone?: string;
    Mobile?: string;
    Address?: string;
    Town?: string;
    County?: string;
    Country?: string;
    Postcode?: string;
    CompanyName: string;
    CompanyEmail?: string;
    CompanyPhone?: string;
    CompanyAddress?: string;
    CompanyTown?: string;
    CompanyCounty?: string;
    CompanyCountry?: string;
    CompanyPostcode?: string;
    CompanyGroupID?: string;
    CompanyGroupName?: string;
    IndustryName?: string;
    CourseTemplateID?: string;
    AdditionalInformation?: string;
    MarketingOptIn?: boolean;
  }
}
